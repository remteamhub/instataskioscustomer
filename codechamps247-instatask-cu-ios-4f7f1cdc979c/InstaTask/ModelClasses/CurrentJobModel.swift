//
//  CurrentJobModel.swift
//  InstaTask
//
//  Created by Arqam Butt on 5/9/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrentJobModel: Mappable {
    
    static let shared = CurrentJobModel()
    
    var id : Int = 0
    var location_address : String = ""
    var service_name : String = ""
    var service_id : Int = 0
    var provider_id : Int = 0
    var customer_approval : Int = 0
    var after_work_img : String = ""
    var job_status : Int = 0
    var lat : Double = 0.0
    var current_situation_img : String = ""
    var customer_id : Int = 0
    var lng  : Double = 0.0
    var user = UserModel()
    var service_price : Int = 0
    var provider = ProviderModel()
    var completeData = [Any]()
    var edit_job = Int()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                          <- map["id"]
        location_address            <- map["location_address"]
        provider                    <- map["provider"]
        service_name                <- map["service_name"]
        service_id                  <- map["service_id"]
        provider_id                 <- map["provider_id"]
        customer_approval           <- map["customer_approval"]
        after_work_img              <- map["after_work_img"]
        job_status                  <- map["job_status"]
        lat                         <- map["lat"]
        current_situation_img       <- map["current_situation_img"]
        customer_id                 <- map["customer_id"]
        lng                         <- map["lng"]
        user                        <- map["user"]
        service_price               <- map["service_price"]
        provider                    <- map["provider"]
        edit_job                    <- map["edit_job"]
        
    }
    
}

class UserModel: Mappable {
    
    static let shared = UserModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0
    var rating : Float = 0
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]
        rating                          <- map["rating"]
        
    }
    
}


class ProviderModel: Mappable {
    
    static let shared = ProviderModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0
    var rating: Float = 0
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]
        rating                          <- map["rating"]
    }
    
}
