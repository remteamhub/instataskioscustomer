//
//  User.swift
//  QueFront
//
//  Created by Asfand Shabbir on 7/7/18.
//  Copyright © 2018 Code Champs. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

class User: Mappable {

    var id:Int = 0
    var username:String = ""
    var email:String = ""
    var email_verified_at:String = ""
    var created_at:String = ""
    var updated_at:Int = 0
    var phone:String = ""
    var avatar:String = ""
    var address:String = ""
    var fcm_token:String = ""
    var socket_token:String = ""
    var social_id:String = ""
    var lat:Double = 0.0
    var lng:Double = 0.0
    var status:Int = 0
    var first_name:String = ""
    var last_name:String = ""
    var type:Int = 0
    var rating: Float = 0
    var radius:Int = 0
    var isUserSkipLogin = false
//    var radius:String = ""
//    var approval_status:Int = 0
//    var user_id:Int = 0
//    var job_status:Int = 0
//    var withdraw_status:Int = 0
//    var total_earning:Int = 0
//    var weekly_earning:Int = 0
//    var overall_rating: Int = 0
//    var license_img:String = ""

    static let shared = User()

    init() {

    }

    required init?(map: Map) {
        self.mapping(map: map)
    }

    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }

    func deleteUser(handler: (()->()) ) {
        id = 0
        username = ""
        email = ""
        email_verified_at = ""
        created_at = ""
        updated_at = 0
        phone = ""
        avatar = ""
        address = ""
        fcm_token = ""
        socket_token = ""
        social_id = ""
        lat = 0.0
        lng = 0.0
        status = 0
        first_name = ""
        last_name = ""
        type = 0
        rating = 0
        radius = 0
        saveUser(user: self)
        handler()
    }

    func stringID() -> String{
        return "\(id)"
    }

    func loadUser() {
        let userDef = UserDefaults.standard
        if ((userDef.string(forKey: USER_DATA)) != nil) {
            let uString = UserDefaults.standard.value(forKey: USER_DATA) as! String
            let mapper = Mapper<User>()
            let userObj = mapper.map(JSONString: uString)
            let map = Map.init(mappingType: .fromJSON, JSON: (userObj?.toJSON())!)
            self.mapping(map:map)
        }
    }

    func saveUser(user:User) {
        UserDefaults.standard.set(user.toJSONString()!, forKey: USER_DATA)
        UserDefaults.standard.synchronize()
        loadUser()
    }

    func isLoggedIn() -> Bool {
        return id != 0 ? true : false
    }

    // Mappable
    func mapping(map: Map) {

        id                  <- map["id"]
        username            <- map["username"]
        email               <- map["email"]
        email_verified_at   <- map["email_verified_at"]
        created_at          <- map["created_at"]
        updated_at          <- map["updated_at"]
        phone               <- map["phone"]
        avatar              <- map["avatar"]
        address             <- map["address"]
        fcm_token           <- map["fcm_token"]
        socket_token        <- map["socket_token"]
        social_id           <- map["social_id"]
        lat                 <- map["lat"]
        lng                 <- map["lng"]
        status              <- map["status"]
        first_name          <- map["first_name"]
        last_name           <- map["last_name"]
        type                <- map["type"]
        rating              <- map["rating"]
        radius              <- map["radius"]
        
//        approval_status     <- map["approval_status"]
//        user_id             <- map["user_id"]
//        job_status          <- map["job_status"]
//        withdraw_status     <- map["withdraw_status"]
//        total_earning       <- map["total_earning"]
//        weekly_earning      <- map["weekly_earning"]
//        overall_rating      <- map["overall_rating"]
//        license_img         <- map["license_img"]

    }

}


class NearByProviderModel: Mappable {

    static let shared = NearByProviderModel()

    var usr_id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var address : String = ""
    var phone : String = ""
    var job_status : Int = 0
    var overall_rating : Int = 0
    var lat : Double = 0.0
    var lng : Double = 0.0
    var distance : Double = 0.0

    var serviceArray = [Any]()

    init() {

    }

    required init?(map: Map) {
        self.mapping(map: map)
    }

    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }

    func deleteNearBy(){
        usr_id = 0
        first_name = ""
        last_name = ""
        address = ""
        phone = ""
        job_status = 0
        overall_rating = 0
        lat = 0.0
        lng = 0.0
        distance = 0.0
        serviceArray = []
        saveArr(arr:NSArray.init())
    }
    
    func saveArr(arr:NSArray) {
        let data = NSKeyedArchiver.archivedData(withRootObject: arr)
        UserDefaults.standard.set(data, forKey: Near_DATA)//(arr, forKey: Near_DATA)
        UserDefaults.standard.synchronize()
    }
    
    func loadArr( handler: (()->())?) {
        let userDef = UserDefaults.standard
        if ((userDef.array(forKey: Near_DATA)) != nil) {
            let uString = UserDefaults.standard.value(forKey: Near_DATA) as! NSArray
            self.serviceArray.append(contentsOf: uString)
            handler?()
        }

        let usersData = UserDefaults.standard.object(forKey: Near_DATA) as? Data
        if let loggedInUsersData = usersData {
            let userData = NSKeyedUnarchiver.unarchiveObject(with: loggedInUsersData as Data)
                let arr = userData as! NSArray
                self.serviceArray.append(contentsOf: arr)
                handler?()
        }

    }
    
    func mapping(map: Map) {
        usr_id                      <- map["usr_id"]
        first_name                  <- map["first_name"]
        last_name                   <- map["last_name"]
        address                     <- map["address"]
        phone                       <- map["phone"]
        job_status                  <- map["job_status"]
        overall_rating              <- map["overall_rating"]
        lat                         <- map["lat"]
        lng                         <- map["lng"]
        distance                    <- map["distance"]
    }

}

//
//private static func archiveWidgetDataArray(widgetDataArray : NSArray) -> Data {
//    do {
//        let data = try NSKeyedArchiver.archivedData(withRootObject: widgetDataArray, requiringSecureCoding: false)
//
//        return data
//    } catch {
//        fatalError("Can't encode data: \(error)")
//    }
//
//}
//
//static func loadWidgetDataArray() -> NSArray {
//    guard
//        //isKeyPresentInUserDefaults(key: Near_DATA), //<- Do you really need this line?
//        let unarchivedObject = UserDefaults.standard.data(forKey: Near_DATA)
//        else {
//            return []
//    }
//    do {
//        guard let array = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(unarchivedObject) as? NSArray else {
//            fatalError("loadWidgetDataArray - Can't get Array")
//        }
//        return array
//    } catch {
//        fatalError("loadWidgetDataArray - Can't encode data: \(error)")
//    }
//}


//class Users: Mappable {
//
//    static let shared = Users()
//
//    var userDetails = UserDetails()
//    var nearbyProviders = [NearByProviderModel]()
//
//    init() {
//
//    }
//    required init?(map: Map) {
//        self.mapping(map: map)
//    }
//    convenience init(dic:[String:Any]) {
//        let map = Map.init(mappingType: .fromJSON, JSON: dic)
//        self.init(map:map)!
//    }
//    func mapping(map: Map) {
//        userDetails <- map["userDetails"]
//        nearbyProviders <- map["nearbyProviders"]
//    }
//
//}
//
//class UserDetails: Mappable {
//
//    var id:Int = 0
//    var username:String = ""
//    var email:String = ""
//    var email_verified_at:String = ""
//    var created_at:String = ""
//    var updated_at:Int = 0
//    var phone:String = ""
//    var avatar:String = ""
//    var address:String = ""
//    var fcm_token:String = ""
//    var socket_token:String = ""
//    var social_id:String = ""
//    var lat:Double = 0.0
//    var lng:Double = 0.0
//    var status:Int = 0
//    var first_name:String = ""
//    var last_name:String = ""
//    var type:Int = 0
//
//    //    var radius:String = ""
//    //    var approval_status:Int = 0
//    //    var user_id:Int = 0
//    //    var job_status:Int = 0
//    //    var withdraw_status:Int = 0
//    //    var total_earning:Int = 0
//    //    var weekly_earning:Int = 0
//    //    var overall_rating: Int = 0
//    //    var license_img:String = ""
//
//    static let shared = UserDetails()
//
//    init() {
//
//    }
//
//    required init?(map: Map) {
//        self.mapping(map: map)
//    }
//
//    convenience init(dic:[String:Any]) {
//        let map = Map.init(mappingType: .fromJSON, JSON: dic)
//        self.init(map:map)!
//    }
//
//    func deleteUser(handler: (()->()) ) {
//        id = 0
//        username = ""
//        email = ""
//        email_verified_at = ""
//        created_at = ""
//        updated_at = 0
//        phone = ""
//        avatar = ""
//        address = ""
//        fcm_token = ""
//        socket_token = ""
//        social_id = ""
//        lat = 0.0
//        lng = 0.0
//        status = 0
//        first_name = ""
//        last_name = ""
//        type = 0
//        //        radius = ""
//        //        approval_status = 0
//        //        user_id = 0
//        //        job_status = 0
//        //        withdraw_status = 0
//        //        total_earning = 0
//        //        weekly_earning = 0
//        //        overall_rating = 0
//        //        license_img = ""
//        saveUser(user: self)
//        handler()
//    }
//
//    func stringID() -> String{
//        return "\(id)"
//    }
//
//    func loadUser() {
//        let userDef = UserDefaults.standard
//        if ((userDef.string(forKey: USER_DATA)) != nil) {
//            let uString = UserDefaults.standard.value(forKey: USER_DATA) as! String
//            let mapper = Mapper<Users>()
//            let userObj = mapper.map(JSONString: uString)
//            let map = Map.init(mappingType: .fromJSON, JSON: (userObj?.toJSON())!)
//            self.mapping(map:map)
//        }
//    }
//
//    func saveUser(user:UserDetails) {
//        UserDefaults.standard.set(user.toJSONString()!, forKey: USER_DATA)
//        UserDefaults.standard.synchronize()
//        loadUser()
//    }
//
//    func isLoggedIn() -> Bool {
//        return id != 0 ? true : false
//    }
//
//    // Mappable
//    func mapping(map: Map) {
//
//        id                  <- map["id"]
//        username            <- map["username"]
//        email               <- map["email"]
//        email_verified_at   <- map["email_verified_at"]
//        created_at          <- map["created_at"]
//        updated_at          <- map["updated_at"]
//        phone               <- map["phone"]
//        avatar              <- map["avatar"]
//        address             <- map["address"]
//        fcm_token           <- map["fcm_token"]
//        socket_token        <- map["socket_token"]
//        social_id           <- map["social_id"]
//        lat                 <- map["lat"]
//        lng                 <- map["lng"]
//        status              <- map["status"]
//        first_name          <- map["first_name"]
//        last_name           <- map["last_name"]
//        type                <- map["type"]
//        //        radius              <- map["radius"]
//        //        approval_status     <- map["approval_status"]
//        //        user_id             <- map["user_id"]
//        //        job_status          <- map["job_status"]
//        //        withdraw_status     <- map["withdraw_status"]
//        //        total_earning       <- map["total_earning"]
//        //        weekly_earning      <- map["weekly_earning"]
//        //        overall_rating      <- map["overall_rating"]
//        //        license_img         <- map["license_img"]
//
//    }
//
//}
//
//
//class NearByProviderModel: Mappable {
//
//    static let shared = NearByProviderModel()
//
//    var usr_id : Int = 0
//    var first_name : String = ""
//    var last_name : String = ""
//    var address : String = ""
//    var phone : String = ""
//    var job_status : Int = 0
//    var overall_rating : Int = 0
//    var lat : Double = 0.0
//    var lng : Double = 0.0
//    var distance : Double = 0.0
//
//    var serviceArray = [Any]()
//
//    init() {
//
//    }
//
//    required init?(map: Map) {
//        self.mapping(map: map)
//    }
//
//    convenience init(dic:[String:Any]) {
//        let map = Map.init(mappingType: .fromJSON, JSON: dic)
//        self.init(map:map)!
//    }
//
//    func deleteNearByProvider(handler: (()->()) ) {
//        usr_id = 0
//        first_name = ""
//        last_name = ""
//        address = ""
//        phone = ""
//        job_status = 0
//        overall_rating = 0
//        lat = 0.0
//        lng = 0.0
//        distance = 0.0
//
//        saveNearByProvider(nearby: [self])
//        handler()
//    }
//
//
////    func loadNearByProvider() {
////        let userDef = UserDefaults.standard
////        if ((userDef.string(forKey: Near_DATA)) != nil) {
////            let uString = UserDefaults.standard.value(forKey: Near_DATA) as! String
////            let mapper = Mapper<NearByProviderModel>()
////            let userObj = mapper.map(JSONString: uString)
////            let map = Map.init(mappingType: .fromJSON, JSON: (userObj?.toJSON())!)
////            self.mapping(map:map)
////        }
////    }
////
////    func saveNearByProvider(nearby:[NearByProviderModel]) {
////        UserDefaults.standard.set(nearby.toJSONString()!, forKey: Near_DATA)
////        UserDefaults.standard.synchronize()
////        loadNearByProvider()
////    }
//    
//    func mapping(map: Map) {
//
//        usr_id                      <- map["usr_id"]
//        first_name                  <- map["first_name"]
//        last_name                   <- map["last_name"]
//        address                     <- map["address"]
//        phone                       <- map["phone"]
//        job_status                  <- map["job_status"]
//        overall_rating              <- map["overall_rating"]
//        lat                         <- map["lat"]
//        lng                         <- map["lng"]
//        distance                    <- map["distance"]
//    }
//
//}
