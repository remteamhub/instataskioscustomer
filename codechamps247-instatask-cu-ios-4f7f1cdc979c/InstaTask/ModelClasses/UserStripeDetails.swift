//
//  UserStripeDetails.swift
//  InstaTask
//
//  Created by Arqam Butt on 5/18/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import ObjectMapper

class UserStripeDetails: Mappable {
    
    static let shared = UserStripeDetails()
    
    var default_source : String = ""
    var tempDefaultSource : String = ""
    var sources = SourcesModel()
    var created : Int = 0
    var dataArr = [Any]()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        default_source       <- map["default_source"]
        sources              <- map["sources"]
        
    }
    
    func deleteStripeData(){
        UserDefaults.standard.removeObject(forKey: Stripe_DATA)
//        default_source = ""
//        tempDefaultSource = ""
//        sources = SourcesModel.init()
//        created = 0
//        dataArr = []
//        savaStripeData(arr:NSArray.init())
    }
    
    func savaStripeData(arr:NSArray){
        let uData = NSKeyedArchiver.archivedData(withRootObject: arr)
        UserDefaults.standard.set(uData, forKey: Stripe_DATA)
        UserDefaults.standard.synchronize()
    }
    
    func loadStripeArr( handler: (()->())?) {
        self.dataArr.removeAll()
        let usersData = UserDefaults.standard.object(forKey: Stripe_DATA) as? Data
        if let loggedInUsersData = usersData {
            let userData = NSKeyedUnarchiver.unarchiveObject(with: loggedInUsersData as Data)
            let arr = userData as! NSArray
            self.dataArr.append(contentsOf: arr)
            handler?()
        }
    }
}

class SourcesModel: Mappable {
    
    var object : String = ""
    var data = [DataModel]()
    var url : String = ""
    
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        object          <- map["object"]
        data            <- map["data"]
        url             <- map["url"]
        
    }
    
}

class DataModel: Mappable {
    
    static var shared = DataModel()
    
    var id : String = ""
    var exp_year : Int = 0
    var exp_month : Int = 0
    var last4 : String = ""
    var brand : String = ""
    var customer : String = ""
    
    init() {
        
    }
    
    init(data: DataModel) {
        let mapper = Mapper<DataModel>()
        let userObj = mapper.map(JSONString: data.toJSONString()!)
        let map = Map.init(mappingType: .fromJSON, JSON: (userObj?.toJSON())!)
        self.mapping(map:map)
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    
    func mapping(map: Map) {
        
        id                  <- map["id"]
        exp_year            <- map["exp_year"]
        exp_month           <- map["exp_month"]
        last4               <- map["last4"]
        brand               <- map["brand"]
        customer            <- map["customer"]
        
    }
    
}
