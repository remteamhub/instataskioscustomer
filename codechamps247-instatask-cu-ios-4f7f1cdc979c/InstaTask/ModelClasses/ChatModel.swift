//
//  ChatModel.swift
//  InstaTask
//
//  Created by Arqam Butt on 10/18/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatModel: Mappable {
    
    static let shared = ChatModel()
    
    var last_page_url : String = ""
    var prev_page_url : String = ""
    var from : Int = 0
    var total : Int = 0
    var path : String = ""
    var first_page_url : String = ""
    var last_page : Int = 0
    var next_page_url : String = ""
    var current_page : Int = 0
    var per_page : Int = 0
    var to : Int = 0
    var data = [Any]()
    
    var messageData = [Any]()
    var receiveMessageData = [Any]()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        last_page_url                   <- map["last_page_url"]
        prev_page_url                   <- map["prev_page_url"]
        from                            <- map["from"]
        total                           <- map["total"]
        path                            <- map["path"]
        first_page_url                  <- map["first_page_url"]
        last_page                       <- map["last_page"]
        next_page_url                   <- map["next_page_url"]
        current_page                    <- map["current_page"]
        per_page                        <- map["per_page"]
        to                              <- map["to"]
        data                            <- map["data"]
        
    }
    
}


class MessageModel: Mappable {
    
    static let shared = MessageModel()
    
    var id : Int = 0
    var from : Int = 0
    var created_at : String = ""
    var message : String = ""
    var updated_at : String = ""
    var to : Int = 0
    var read_status : Int = 0
    var from_user = FromModel()
    var to_user = ToModel()
    var msg_time : String = ""
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        from                    <- map["from"]
        message                 <- map["message"]
        to                      <- map["to"]
        read_status             <- map["read_status"]
        created_at              <- map["created_at"]
        updated_at              <- map["updated_at"]
        to_user                 <- map["to_user"]
        from_user               <- map["from_user"]
        msg_time                <- map["msg_time"]
    }
    
}


class FromModel: Mappable {
    
    static let shared = FromModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        first_name              <- map["first_name"]
        last_name               <- map["last_name"]
        
    }
    
}


class ToModel: Mappable {
    
    static let shared = ToModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        first_name              <- map["first_name"]
        last_name               <- map["last_name"]
        
    }
    
}
