//
//  ServicesModel.swift
//  InstaTask
//
//  Created by Arqam Butt on 5/3/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import ObjectMapper

class ServicesModel: Mappable {
    
    static let shared = ServicesModel()
    
    var id : Int = 0
    var title : String = ""
    var status : Int = 0
    var price : Int = 0
    var commision : Int = 0
    var created_at : String = ""
    var updated_at : String = ""

    var serviceArray = [Any]()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        title                   <- map["title"]
        status                  <- map["status"]
        price                   <- map["price"]
        commision               <- map["commision"]
        created_at              <- map["created_at"]
        updated_at              <- map["updated_at"]
        
    }
    
}
