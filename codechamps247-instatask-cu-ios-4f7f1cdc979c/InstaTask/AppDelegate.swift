//
//  AppDelegate.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/20/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import Stripe
import FBSDKCoreKit
import TwitterKit
//import SwiftLocation
import Firebase
import Crashlytics
import iOS_Slide_Menu
import UserNotifications
import FirebaseMessaging
import DropDown
import SwiftLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceTokenString:String = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //LocationManager.shared.showsBackgroundLocationIndicator = true
        GMSServices.provideAPIKey(MAPS_KEY)
        GMSPlacesClient.provideAPIKey(PLACES_API_KEY)
        
        STPPaymentConfiguration.shared().publishableKey = stripePublishableKey
        STPTheme.default().primaryForegroundColor = mainBlue
        IQKeyboardManager.shared.enable = true
        
        DropDown.startListeningToKeyboard()
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        
        
        LocationManager.shared.showsBackgroundLocationIndicator = false
        LocationManager.shared.backgroundLocationUpdates = false
        //TWITTER INIT
        TWTRTwitter.sharedInstance().start(withConsumerKey: TWITTER_CONSUMER_KEY, consumerSecret: TWITTER_CONSUMER_SECRET)
        //FACEBOOK INIT
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
//        
//        LocationManager.shared.requireUserAuthorization(.always)
//        LocationManager.shared.startUpdatingLocation()
//        if let _ = launchOptions?[UIApplication.LaunchOptionsKey.location] {
//            LocationManager.shared.locateFromGPS(.significant, accuracy: .any, timeout: Timeout.Mode.absolute(3000)) { (result) in
//                switch result {
//                case .success(let location):
//                    print("Location received AppDelegate: \(location.coordinate.latitude), \(location.coordinate.longitude)")
//                    LocationCoordinator.shared.lat = location.coordinate.latitude
//                    LocationCoordinator.shared.lng = location.coordinate.longitude
//                case .failure(let error):
//                    print("Received error: \(error)")
//                }
//            }
//        }
        

        registerNotification()
        FirebaseApp.configure()
        User.shared.loadUser()
        Messaging.messaging().delegate = self
        
        //application.registerForRemoteNotifications()
        
        SlideNavigationController.sharedInstance().enableShadow = true //Is it Pushed?????
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        SlideNavigationController.sharedInstance().leftMenu = secondViewController
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if TWTRTwitter.sharedInstance().application(app, open: url, options: options) {
            return true
        }
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        return handled
    }

    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("========> App Delegate Completion Handler Background Fetch <========")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        // LocationManager.shared.mapView?.clear()
        UserDefaults.standard.synchronize()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        NotificationCenter.default.post(.init(name: .refreshChat))
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        LocationManager.shared.startUpdatingLocation()
//        let position = CLLocationCoordinate2D(latitude:LocationManager.shared.location.coordinate.latitude, longitude: LocationManager.shared.location.coordinate.longitude)
//        LocationManager.shared.updateMarker(camPosition: position, map: LocationManager.shared.mapView, markerImage: "locationMarker")
        checkNotification()
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        if UserStripeDetails.shared.dataArr.count == 0 {
            UserStripeDetails.shared.deleteStripeData()
        }
        UserDefaults.standard.synchronize()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //print("didReceiveRemoteNotification", userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
        self.checkNotification(data: userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        Messaging.messaging().apnsToken = deviceToken
        print("Here is Token: \(deviceTokenString)")
    }
    
    
    
    func registerNotification() {
        
        if #available(iOS 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted, error) in
//                DispatchQueue.main.async {
                    if granted {
                        
                    }else{
//                        DispatchQueue.main.async {
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                        print("Settings opened: \(success)") // Prints true
                                    })
                                } else {
                                    UIApplication.shared.openURL(settingsUrl as URL)
                                }
                            }
//                        }
                    }
//                }
            }
            
        } else {
            let type: UIUserNotificationType = [.badge, .alert, .sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func checkNotification(data: [AnyHashable: Any] = [:]) {
        print(data)
//        print("Message ID: \(data["gcm.message_id"])")
//        print("Body: \(data["body"])")
//        print("APS: \(data["aps"])")
        
        if let api = data["body"] as? NSString{
            if api == "Job_Edit_Enable" {
                NotificationCenter.default.post(.init(name: .openJobEditEnable))
            }
            if api == "Job_Edit_Cancel"{
                NotificationCenter.default.post(.init(name: .openJobEditCancel))
            }
            if api == "You have new messages" {
                if Extra.shared.alreadyOnChat == false {
                    NotificationCenter.default.post(.init(name: .openNewMessage))
                }
            }
        }
        
        if let api = data["api"] as? String {
            //print(api)
            if api == "message" {
                NotificationCenter.default.post(.init(name: .openMessage))
                return
            }
            Extra.shared.checkNotification = api
            NotificationCenter.default.post(.init(name: .openRemoteNotification))
        }else{
//            print("Notification Without Data Hitting")
            Extra.shared.checkNotification = "checkCurrentJob"
            NotificationCenter.default.post(.init(name: .openRemoteNotification))
        }
        
        dispatchGroup.enter()
        NetworkManager.limitationsApi(delegate: HomeViewController(), showHUD: false)
        
        
        
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        
        //print("Notification Data:", response.notification.request.content.userInfo)
        
        let data = response.notification.request.content.userInfo
        Extra.shared.userClickedNotification = true
        //print(data)
        Messaging.messaging().appDidReceiveMessage(data)
        
        self.checkNotification(data: data)
        
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        //When user app is on.
        Extra.shared.userClickedNotification = false
        Messaging.messaging().appDidReceiveMessage(userInfo)
//        print("Message ID: \(userInfo["gcm.message_id"]!)")
//        print("Body: \(userInfo["body"]!)")
//        print("APS: \(userInfo["aps"]!)")
//        if let api = userInfo["api"] as? String {
//            print(api)
//            Extra.shared.checkNotification = api
//            NotificationCenter.default.post(.init(name: .openRemoteNotification))
//        }
        self.checkNotification(data: userInfo)
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        print("FCM token Two: \(fcmToken) ")
        deviceTokenString = fcmToken
    }
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
    public func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        print("--->messaging:\(messaging)")
        print("--->didReceive Remote Message:\(remoteMessage.appData)")
        guard let data =
            try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
            let prettyPrinted = String(data: data, encoding: .utf8) else { return }
        print("Received direct channel message:\n\(prettyPrinted)")
    }
}
