//
//  ChatInboxUser.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/30/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation

class ChatInboxUser
{
    var userId: String!
    var name: String!
    var avatarURL: String!
    
    init()
    {
        self.userId = ""
        self.name = ""
        self.avatarURL = ""
    }
    
    func fillData(dict: [String: AnyObject])
    {
        if let data = dict["userId"] as? String{
            self.userId = data
        }
        if let data = dict["name"] as? String{
            self.name = data
        }
        if let data = dict["avatarURL"] as? String{
            self.avatarURL = data
        }
    }
    
    func toDict() -> [String: AnyObject]
    {
        let dict = ["userId": userId, "name": name, "avatarURL": avatarURL]
        return dict as [String : AnyObject]
    }
}
