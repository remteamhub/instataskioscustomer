//
//  File.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/29/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation

class TrackingURL{
    var id: String!
    var url: String!
    var sentBy: String!
    var timeStamp: Double!
    
    init()
    {
        id = ""
        url = ""
        sentBy = ""
        timeStamp = 0
    }
    
    func fillData(dict: [String: AnyObject])
    {
        if let data = dict["id"] as? String
        {
            id = data
        }
        if let data = dict["url"] as? String
        {
            url = data
        }
        if let data = dict["sentBy"] as? String
        {
            sentBy = data
        }
        if let data = dict["timeStamp"] as? Double
        {
            timeStamp = data
        }
    }
}
