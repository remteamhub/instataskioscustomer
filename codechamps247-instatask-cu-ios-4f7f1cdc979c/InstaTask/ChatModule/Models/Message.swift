//
//  Message.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/22/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation
import CoreLocation
import MessageKit



private struct CoordinateItem: LocationItem {
    
    var location: CLLocation
    var size: CGSize
    
    init(location: CLLocation) {
        self.location = location
        self.size = CGSize(width: 240, height: 240)
    }
    
}

private struct ImageMediaItem: MediaItem {
    
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
    
    init(image: UIImage) {
        self.image = image
        self.size = CGSize(width: 240, height: 240)
        self.placeholderImage = UIImage()
    }
    
    init(url: String) {
        self.url = URL.init(string: url)
        self.size = CGSize(width: 240, height: 240)
        self.placeholderImage = UIImage()
    }
    
}

internal struct Message: MessageType {
    var sender: SenderType
    
    
    var messageId: String
    //var sender: Sender
    var sentDate: Date
    var kind: MessageKind
    var mediaURL: String
    var type: MessageTypeEnum
    
    private init(kind: MessageKind, sender: Sender, messageId: String, date: Date, mediaURL: String?) {
        self.kind = kind
        self.sender = sender
        self.messageId = messageId
        self.sentDate = date
        self.mediaURL = mediaURL ?? ""
        switch kind {
        case .attributedText( _):
            type = .text
        case .photo( _):
            type = .image
        case .custom( _):
            type = .system
        default:
            type = .text
            break
        }
    }
    
    private init(kind: MessageKind, sender: Sender, messageId: String, date: Date) {
        self.kind = kind
        self.sender = sender
        self.messageId = messageId
        self.sentDate = date
        self.mediaURL = ""
        switch kind {
        case .attributedText( _):
            type = .text
        case .photo( _):
            type = .image
        case .custom( _):
            type = .system
        default:
            type = .text
            break
        }
    }
    
    init(custom: Any?, sender: Sender, messageId: String, date: Date) {
        self.init(kind: .custom(custom), sender: sender, messageId: messageId, date: date)
    }
    
    init(text: String, sender: Sender, messageId: String, date: Date) {
        self.init(kind: .text(text), sender: sender, messageId: messageId, date: date)
    }
    
    init(attributedText: NSAttributedString, sender: Sender, messageId: String, date: Date) {
        self.init(kind: .attributedText(attributedText), sender: sender, messageId: messageId, date: date)
    }
    
    init(image: UIImage, sender: Sender, messageId: String, date: Date) {
        let mediaItem = ImageMediaItem(image: image)
        
        self.init(kind: .photo(mediaItem), sender: sender, messageId: messageId, date: date)
    }
    
    init(url: String, sender: Sender, messageId: String, date: Date) {
        let mediaItem = ImageMediaItem(url: url)
        self.init(kind: .photo(mediaItem), sender: sender, messageId: messageId, date: date, mediaURL: url)
    }
    
    init(thumbnail: UIImage, sender: Sender, messageId: String, date: Date) {
        let mediaItem = ImageMediaItem(image: thumbnail)
        self.init(kind: .video(mediaItem), sender: sender, messageId: messageId, date: date)
    }
    
    init(location: CLLocation, sender: Sender, messageId: String, date: Date) {
        let locationItem = CoordinateItem(location: location)
        self.init(kind: .location(locationItem), sender: sender, messageId: messageId, date: date)
    }
    
    init(emoji: String, sender: Sender, messageId: String, date: Date) {
        self.init(kind: .emoji(emoji), sender: sender, messageId: messageId, date: date)
    }
    
    func getCustomString() -> String
    {
        switch kind {
        case .custom(let data):
            return data as! String
        default:
            return ""
        }
    }
    
    func getAttributedString() -> String
    {
        switch kind {
        case .attributedText(let data):
            return data.string
        default:
            return ""
        }
    }
    
    func getMediaURL() -> String
    {
        switch kind {
        case .photo(let data):
            return data.url?.absoluteString ?? ""
        default:
            return ""
        }
    }
    
    //FOR FIREBASE CREATE A DICTIONARY
    func createDict() -> [String: AnyObject]
    {
        var text = ""
        var url = ""
        switch kind {
        case .attributedText(let data):
            text = data.string
        case .custom(let data):
            text = data as! String
        case .photo(let data):
            url = data.url?.absoluteString ?? ""
        default:
            
            break
        }
        let dict: [String: AnyObject] = ["type": self.type.rawValue as AnyObject, "senderId": sender.senderId as AnyObject, "msgId": messageId as AnyObject, "text": text as AnyObject, "url": url as AnyObject, "timeStamp":[".sv":"timestamp"] as AnyObject]
        return dict
    }
    
}
