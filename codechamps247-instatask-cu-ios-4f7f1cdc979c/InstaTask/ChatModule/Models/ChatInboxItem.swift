//
//  ChatInboxItem.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/30/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation

class ChatInboxItem
{
    var chatRoomId: String!
    var users: [ChatInboxUser]!
    var timeStamp: Double!
    var lastMsg: String!
    var read: Bool!
    var createdOn: Double!
    
    init()
    {
        chatRoomId = ""
        users = []
        timeStamp = 0
        lastMsg = ""
        read = false
        createdOn = 0
    }
    
    func fillData(dict: [String:AnyObject])
    {
        if let data = dict["chatRoomId"] as? String{
            self.chatRoomId = data
        }
        if let data = dict["users"] as? [String:AnyObject]{
            let usersArray = Array(data.values)
            self.users.removeAll()
            for singleUser in usersArray
            {
                let userDict = singleUser as! [String:AnyObject]
                let newUser = ChatInboxUser()
                newUser.fillData(dict: userDict)
//                if(newUser.userId == DataStore.shared.ownUserId)
//                {
//                    self.users.insert(newUser, at: 0)
//                }
//                else{
//                    self.users.append(newUser)
//                }
            }
        }
        if let data = dict["timeStamp"] as? Double{
            self.timeStamp = data
        }
        if let data = dict["lastMsg"] as? String{
            self.lastMsg = data
        }
        if let data = dict["read"] as? Bool{
            self.read = data
        }
        if let data = dict["createdOn"] as? Double{
            self.createdOn = data
        }
    }
    
    func toDict() -> [String: AnyObject]
    {
        var usersDict: [String:AnyObject] = [:]
        for user in users{
            usersDict[user.userId] = user.toDict() as AnyObject
        }
        
        let dict = ["chatRoomId": chatRoomId!, "users": usersDict, "timeStamp": timeStamp!, "read": read!, "lastMsg": lastMsg!] as [String : AnyObject]
        return dict
    }
    
    func toDictWithTimestamp() -> [String: AnyObject]
    {
        var usersDict: [String:AnyObject] = [:]
        for user in users{
            usersDict[user.userId] = user.toDict() as AnyObject
        }
        
        let dict = ["chatRoomId": chatRoomId!, "users": usersDict, "timeStamp": [".sv":"timestamp"], "read": read!, "lastMsg": lastMsg!] as [String : AnyObject]
        return dict
    }
    
}
