////
////  FirebaseMethods.swift
////  SnikPic
////
////  Created by Asfand Shabbir on 11/27/18.
////  Copyright © 2018 Aqsa Arshad. All rights reserved.
////
//
//import FirebaseDatabase
//
//class FirebaseMethods
//{
//    class func createNewKey(reference: DatabaseReference) -> String {
//        return reference.childByAutoId().key ?? ""
//    }
//    
//    class func writeData(reference: DatabaseReference, value: Any?) {
//        reference.setValue(value)
//    }
//    
//    class func readValueOnce(query: DatabaseQuery, completion: @escaping([String: AnyObject]) -> Void)
//    {
//        query.observeSingleEvent(of: .value) { (snapshot) in
//            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
//            completion(postDict)
//        }
//    }
//    
//    class func keepListeningValue(query: DatabaseQuery, completion: @escaping([String: AnyObject]) -> Void)
//    {
//        query.observe(.value) { (snapshot) in
//            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
//            
//            completion(postDict)
//        }
//    }
//    
//    class func keepListeningAddedChild(query: DatabaseQuery, completion: @escaping([String: AnyObject]) -> Void)
//    {
//        query.observe(.childAdded) { (snapshot) in
//            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
//            
//            completion(postDict)
//        }
//    }
//    
//    class func keepListeningRemovedChild(query: DatabaseQuery, completion: @escaping([String: AnyObject]) -> Void)
//    {
//        query.observe(.childRemoved) { (snapshot) in
//            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
//            
//            completion(postDict)
//        }
//    }
//    
//}
