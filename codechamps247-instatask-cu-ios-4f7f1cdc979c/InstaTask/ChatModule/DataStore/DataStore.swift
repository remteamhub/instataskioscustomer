
//  DataStore.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/22/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import UIKit
import MessageKit

class DataStore
{
    static let shared = DataStore()

    var ownSender: Sender!
    var senders: [Sender] = []

//    var otherUser: User!
//    var fromChat = false
//
//    //CHAT ROOM CONSTANTS
//    let chatRoomsBasicReference = Database.database().reference().child("chatInbox")
//    var systemSender = Sender.init(id: "00000", displayName: "")
//
//    var pageLimit = 10
//    var currentPaginationIndex = 10
//
//    //FIREBASE VARIALBES
//    var chatRoomId = ""
//    var ownUserId = ""
//
//    //FIREBASE LISTENERS FOR INBOX
//    var inboxReference: DatabaseReference!
//
//    var inboxNewReference: DatabaseReference!
//
//    var chatReference: DatabaseReference!
//    var urlsReference: DatabaseReference!
//    var chatQuery: DatabaseQuery!
//
//    //FIREBASE LISTENERS FOR CHAT
//    var chatOneTimeQuery: DatabaseQuery!
//    var chatListenerNewQuery: DatabaseQuery!
//    var chatListenerRemovedQuery: DatabaseQuery!
//
//    var messages: [Message] = []
//    var chatItems: [ChatInboxItem] = []
//    var bubbleCount = 0
//
//    private init() {}
//
//    //MARK:- CHAT INBOX LISTENERS AND WRITERS
//    func listenToChatInboxOnce(completion: @escaping([ChatInboxItem]) -> Void)
//    {
//        if(inboxReference != nil)
//        {
//            inboxReference.removeAllObservers()
//        }
//
//        inboxReference = Database.database().reference().child("chatInbox").child(ownUserId)
//        chatItems.removeAll()
//
//        FirebaseMethods.keepListeningValue(query: inboxReference) { (snapshotDict) in
//            self.chatItems.removeAll()
//
//            for msgs in snapshotDict
//            {
//                let msgDict = msgs.value as! [String: AnyObject]
//                let newMsg = self.getChatItemFromDict(dict: msgDict)
//                self.chatItems.append(newMsg!)
//            }
//            self.chatItems.sort(by: {$0.timeStamp > $1.timeStamp})
//            completion(self.chatItems)
//        }
//    }
//
//    //UPDATE THE MESSAGES COUNT BUBBLE
//    func updateBubble(tabBarController: UITabBarController)
//    {
//        //update bubble
//        let tabArray = tabBarController.tabBar.items
//        let tabItem = tabArray?[2] // as! UITabBarItem
//        if(self.bubbleCount > 0)
//        {
//            tabItem?.badgeValue = "\(self.bubbleCount)"
//            tabItem?.badgeColor = UIColor.red
//        }
//        else
//        {
//            tabItem?.badgeColor = UIColor.clear
//            tabItem?.badgeValue = ""
//        }
//    }
//
//    //UPDATE CHAT INBOX AFTER CURRENT MESSAGE
//    func updateChatItemAfterMessage(msg: Message, chatRoomId: String)
//    {
//        let ownInboxReference = chatRoomsBasicReference.child(ownUserId).child(chatRoomId)
//
//        FirebaseMethods.readValueOnce(query: ownInboxReference) { (snapshotDict) in
//            let chatRoom = self.getChatItemFromDict(dict: snapshotDict)
//
//            if(chatRoom!.chatRoomId == "")
//            {
//                //chat room doesn't exist in own Inbox
//            }
//            else
//            {
//                //does exist, just update
//                chatRoom!.read = true
//                if(msg.type == MessageTypeEnum.text)
//                {
//                    chatRoom!.lastMsg = msg.getAttributedString()
//                }
//                else if(msg.type == MessageTypeEnum.image){
//                    chatRoom!.lastMsg = "Image"
//                }
//
//                //overwrite toOwnChatRoom
//                FirebaseMethods.writeData(reference: ownInboxReference, value: chatRoom!.toDictWithTimestamp())
//
//                //overwrite toOtherChatRoom
//                chatRoom?.read = false
//                let otherInboxRef = self.chatRoomsBasicReference.child(self.senders.last!.id).child(chatRoom!.chatRoomId)
//                FirebaseMethods.writeData(reference: otherInboxRef, value: chatRoom!.toDictWithTimestamp())
//            }
//        }
//    }
//
//    //CREATE CHAT INBOX
//    func createInboxItem(secondUser: User)
//    {
//        let msg = Message.init(custom: "\(User.getLoggedInUser().userName) just approved you!", sender: DataStore.shared.systemSender, messageId: UUID().uuidString, date: Date())

        //create users
//        let ownUser = ChatInboxUser.init()
//        ownUser.name = User.getLoggedInUser().userName
//        ownUser.userId = User.getLoggedInUser().uuid
//        ownUser.avatarURL = User.getLoggedInUser().profilePictureUrl
//
//        let otherUser = ChatInboxUser.init()
//        otherUser.name = secondUser.userName
//        otherUser.userId = secondUser.uuid
//        otherUser.avatarURL = secondUser.profilePictureUrl
//
//        var newChatId = ownUser.userId+"_"+otherUser.userId
//        if(otherUser.userId > ownUser.userId)
//        {
//            newChatId = otherUser.userId+"_"+ownUser.userId
//        }
//
//        self.createNewChatItemWithMessage(newChatRoomId: newChatId, msg: msg, roomSenders: [ownUser, otherUser])
//    }
//
//    func createNewChatItemWithMessage(newChatRoomId: String, msg: Message, roomSenders: [ChatInboxUser])
//    {
//        chatReference = Database.database().reference().child("chats").child(newChatRoomId).child("messages")
//
//        self.writeNewMessage(newChatRoom: true, msg: msg, chatRoomId: newChatRoomId) { (msg) in
//            //does exist, just update
//            let chatItem = ChatInboxItem()
//            chatItem.chatRoomId = newChatRoomId
//            chatItem.users = roomSenders
//            chatItem.lastMsg = msg.getCustomString()
//            chatItem.read = true
//            if(msg.type == MessageTypeEnum.text)
//            {
//                chatItem.lastMsg = msg.getAttributedString()
//            }
//            else if(msg.type == MessageTypeEnum.image){
//                chatItem.lastMsg = "Image"
//            }
//
//            //write toOwnChatRoom
//            let ownInboxReference = self.chatRoomsBasicReference.child(roomSenders.first!.userId).child(chatItem.chatRoomId)
//            FirebaseMethods.writeData(reference: ownInboxReference, value: chatItem.toDictWithTimestamp())
//
//            //write toOtherChatRoom
//            chatItem.read = false
//            let otherInboxRef = self.chatRoomsBasicReference.child(roomSenders.last!.userId).child(chatItem.chatRoomId)
//            FirebaseMethods.writeData(reference: otherInboxRef, value: chatItem.toDictWithTimestamp())
//        }
//    }
//
//    //TODO: UPDATE AND CREATE CHAT ROOM
//    func writeChatRoom(chatItem: ChatInboxItem)
//    {
//        let chatRoomReference = Database.database().reference().child("chatInbox").child(ownUserId)
//        if(chatItem.chatRoomId == "")
//        {
//            chatItem.chatRoomId = chatRoomReference.childByAutoId().key
//        }
//        FirebaseMethods.writeData(reference: chatRoomReference.child(chatItem.chatRoomId), value: chatItem.toDict())
//    }
//
//    func markSeenChatRoom(chatRoomId: String!)
//    {
//        let chatRoomReference = Database.database().reference().child("chatInbox").child(ownUserId).child(chatRoomId).child("read")
//        chatRoomReference.setValue(true)
//    }
//
//    //TODO: CHAT INBOX LISTENERS FOR ADDED, CHANGED, REMOVED
//
//
//    //MARK:- CONVERSATION LISTENERS AND WRITERS BELOW
//    //LISTENS TO MESSAGES FOR PAGE LIMIT ONCE
//    func getMessagesOnce(completion: @escaping([Message]) -> Void)
//    {
//        if(chatOneTimeQuery != nil)
//        {
//            chatOneTimeQuery.removeAllObservers()
//        }
//
//        chatReference = Database.database().reference().child("chats").child(chatRoomId).child("messages")
//        urlsReference = Database.database().reference().child("urls").child(chatRoomId)
//        chatOneTimeQuery = chatReference.queryOrdered(byChild: "timeStamp").queryLimited(toLast: UInt(currentPaginationIndex))
//
//        messages.removeAll()
//
//        FirebaseMethods.readValueOnce(query: chatOneTimeQuery) { (snapshotDict) in
//            for msgs in snapshotDict
//            {
//                let msgDict = msgs.value as! [String: AnyObject]
//                let newMsg = self.getMessageFromDict(dict: msgDict)
//                self.messages.append(newMsg!)
//            }
//            self.messages.sort(by: {$0.sentDate.timeIntervalSince1970 < $1.sentDate.timeIntervalSince1970})
//            completion(self.messages)
//        }
//    }
//
//    //GIVEN A TIMESTAMP, LISTENS TO NEWER MESSAGES
//    func listenToNewMessages(lastTimeStamp: Double, completion: @escaping(Message) -> Void)
//    {
//        if(self.chatListenerNewQuery != nil)
//        {
//            chatListenerNewQuery.removeAllObservers()
//        }
//        chatListenerNewQuery = chatReference.queryOrdered(byChild: "timeStamp").queryStarting(atValue: lastTimeStamp, childKey: "timeStamp")
//
//        //listen to chat endpoints below
//        FirebaseMethods.keepListeningAddedChild(query: chatListenerNewQuery) { (dict) in
//            completion(self.getMessageFromDict(dict: dict)!)
//        }
//    }
//
//    func listenToRemovedMessagesOnly(firstTimeStamp: Double, completion: @escaping(Message) -> Void)
//    {
//        if(self.chatListenerRemovedQuery != nil)
//        {
//            chatListenerRemovedQuery.removeAllObservers()
//        }
//        chatListenerRemovedQuery = chatReference.queryOrdered(byChild: "timeStamp").queryStarting(atValue: firstTimeStamp, childKey: "timeStamp")
//
//        FirebaseMethods.keepListeningRemovedChild(query: chatListenerRemovedQuery) { (dict) in
//            completion(self.getMessageFromDict(dict: dict)!)
//        }
//    }
//
//    //MARK:- OLD LISTENERS MESSAGES
////    func keepListeningToChat(completion: @escaping(Message) -> Void)
////    {
////        chatReference = Database.database().reference().child("chats").child(chatRoomId).child("messages")
////        chatQuery = chatReference.queryOrdered(byChild: "timeStamp").queryLimited(toLast: UInt(currentPaginationIndex))
////
////        removeListenersFromChat()
////
////        //listen to chat endpoints below
////        FirebaseMethods.keepListeningAddedChild(query: chatQuery) { (dict) in
////            print(dict)
////
////            completion(self.getMessageFromDict(dict: dict)!)
////        }
////    }
////
////    func listenToRemovedMsg(completion: @escaping(Message) -> Void)
////    {
////        FirebaseMethods.keepListeningRemovedChild(query: chatQuery) { (dict) in
////            print(dict)
////
////            completion(self.getMessageFromDict(dict: dict)!)
////        }
////
////    }
//    //OLD LISTENERS END
//
//    func removeListenersFromChat()
//    {
//        chatReference.removeAllObservers()
//        chatOneTimeQuery.removeAllObservers()
//        chatListenerNewQuery.removeAllObservers()
//        chatListenerRemovedQuery.removeAllObservers()
//    }
//
//
//    //MARK:- CHATITEM FROM DICTIONARY
//    func getChatItemFromDict(dict: [String: AnyObject]) -> ChatInboxItem?
//    {
//        let chatItem = ChatInboxItem.init()
//        chatItem.fillData(dict: dict)
//        return chatItem
//    }
//
//    // MARK:- MESSAGE FROM DICTIONARY
//    func getMessageFromDict(dict: [String: AnyObject]) -> Message?
//    {
//        if let kind = dict["type"] as? String
//        {
//            let timeStamp = Date.init(timeIntervalSince1970: TimeInterval.init((dict["timeStamp"] as! Double)/1000))
//            if(kind == MessageTypeEnum.text.rawValue || kind == MessageTypeEnum.url.rawValue)
//            {
//                var attributedText = NSAttributedString.init(string: "")
//                let senderOfMsg = Sender.init(id: dict["senderId"] as? String ?? "", displayName: "")
//                if(senderOfMsg.id == self.ownSender.id)
//                {
//                    attributedText = NSAttributedString.init(string: dict["text"] as? String ?? "", attributes: [NSAttributedString.Key.font: Theme.getDefaultFont().withSize(ChatTheme.chatTextSize()), NSAttributedString.Key.foregroundColor: UIColor.white])
//                }
//                else
//                {
//                    attributedText = NSAttributedString.init(string: dict["text"] as? String ?? "", attributes: [NSAttributedString.Key.font: Theme.getDefaultFont().withSize(ChatTheme.chatTextSize()), NSAttributedString.Key.foregroundColor: UIColor.black])
//                }
//                return Message.init(attributedText: attributedText, sender: Sender.init(id: dict["senderId"] as? String ?? "", displayName: ""), messageId: dict["msgId"] as? String ?? "", date: timeStamp)
////                return Message.init(text: dict["text"] as? String ?? "", sender: Sender.init(id: dict["senderId"] as? String ?? "", displayName: ""), messageId: dict["msgId"] as? String ?? "", date: timeStamp)
//            }
////            else if(kind == MessageTypeEnum.url.rawValue)
////            {
////                return Message.init(text: dict["text"] as? String ?? "", sender: Sender.init(id: dict["senderId"] as? String ?? "", displayName: ""), messageId: dict["msgId"] as? String ?? "", date: timeStamp)
////            }
//            else if(kind == MessageTypeEnum.image.rawValue)
//            {
//                return Message.init(url: dict["url"] as? String ?? "", sender: Sender.init(id: dict["senderId"] as? String ?? "", displayName: ""), messageId: dict["msgId"] as? String ?? "", date: timeStamp)
//            }
//            else if(kind == MessageTypeEnum.system.rawValue)
//            {
//                return Message(custom: dict["text"] as? String ?? "", sender: Sender.init(id: dict["senderId"] as? String ?? "", displayName: ""), messageId: dict["msgId"] as? String ?? "", date: timeStamp)
//            }
//        }
//
//        return nil
//    }
//
//    //MARK:- URL METHODS
//    func writeNewURL(url: String)
//    {
//        let newURLkey = urlsReference.childByAutoId().key!
//        let writeDict = ["url": url, "timeStamp":[".sv":"timestamp"], "id": newURLkey, "sentBy": ownSender.id] as [String : Any]
//        FirebaseMethods.writeData(reference: urlsReference.child(newURLkey), value: writeDict)
//    }
//
//    func readURLs(completion: @escaping([TrackingURL]) -> Void)
//    {
//        FirebaseMethods.readValueOnce(query: urlsReference) { (snapshotDict) in
//            var urlsArray: [TrackingURL] = []
//            for url in snapshotDict
//            {
//                let urlDict = url.value as! [String: AnyObject]
//                let trackedURL = TrackingURL.init()
//                trackedURL.fillData(dict: urlDict)
//                urlsArray.append(trackedURL)
//            }
//            completion(urlsArray)
//        }
//    }
//
//    //MARK:- UPLOAD IMAGES WORK
//
//    func uploadImage(image: UIImage, chatRoomId: String, completion: @escaping(String?) -> Void)
//    {
//        //UPLOAD IMAGE TO FIREBASE
//        uploadToFirebase(image: image) { (url) in
//            if(url != nil)
//            {
//                let msg = Message.init(url: url!, sender: self.ownSender, messageId: "", date: Date.init())
//                self.writeNewMessage(newChatRoom: false, msg: msg, chatRoomId: chatRoomId, completion: { (msg) in
//                    completion("success")
//                })
//            }
//
//            completion("failure")
//        }
//    }
//
//    //MARK:- UPLOAD TO FIREBASE
//    func uploadToFirebase(image: UIImage, completion: @escaping(String?) -> Void)
//    {
//        if let imageData = image.jpegData(compressionQuality: 0.5){
//            let fileName = createFileName(withExtension: ".jpeg")
//            let fileReference = Storage.storage().reference().child("chatImages").child(fileName)
//
//            fileReference.putData(imageData, metadata: nil) { (metadata, error) in
//                // You can also access to download URL after upload.
//                fileReference.downloadURL { (url, error) in
//                    guard let downloadURL = url else {
//                        // Uh-oh, an error occurred!
//                        completion(nil)
//                        return
//                    }
//                    completion(downloadURL.absoluteString)
//                }
//            }
//        }
//    }
//
//    //CRREATE IMAGE FILE NAME
//    func createFileName(withExtension: String) -> String
//    {
//        var fileName = ""
//
//        let epochTime: Double = Date().timeIntervalSince1970
//
//        fileName.append(chatRoomId)
//        fileName.append("_\(UUID.init().uuidString)")
//        fileName.append("_\(String(epochTime))")
//        fileName.append(withExtension)
//
//        return fileName
//    }
//
//    //MARK:- WRITE METHODS
//    func writeNewMessage(newChatRoom: Bool, msg: Message, chatRoomId: String, completion: @escaping(Message) -> Void)
//    {
//        var newMsg = msg
//        chatReference = Database.database().reference().child("chats").child(chatRoomId).child("messages")
//        newMsg.messageId = FirebaseMethods.createNewKey(reference: chatReference)
//        FirebaseMethods.writeData(reference: chatReference.child(newMsg.messageId), value: newMsg.createDict())
//        if(!newChatRoom)
//        {
//            self.updateChatItemAfterMessage(msg: newMsg, chatRoomId: chatRoomId)
//        }
//        completion(newMsg)
//    }
}
