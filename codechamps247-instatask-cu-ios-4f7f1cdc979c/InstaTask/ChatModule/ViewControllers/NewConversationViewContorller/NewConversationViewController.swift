//
//  NewConversationViewController.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/29/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import UIKit
import MessageKit
//import MessageInputBar
import Kingfisher
import IQKeyboardManagerSwift
//import BSImagePicker
import Photos

final class NewConversationViewController: ChatViewController {
    
    let outgoingAvatarOverlap: CGFloat = 17.5
    
    var mySenderId = ""
    
    //FIREBASE VARIABLES
    var chatRoomId = "chat1"
    var ownSender: SenderType! = Sender(id: "000001", displayName: "")
    var otherSender: SenderType! = Sender(id: "000002", displayName: "")
    var urlArray: [String] = []
    
    var otherAvatarURL: String! = "https://d3icht40s6fxmd.cloudfront.net/sites/default/files/test-product-test.png"
    
    override func viewDidLoad() {
        updateDataStore()
        
        messagesCollectionView = MessagesCollectionView(frame: .zero, collectionViewLayout: CustomMessagesFlowLayout())
        messagesCollectionView.register(CustomCell.self)
        messagesCollectionView.backgroundColor = .white
        super.viewDidLoad()
        
        self.title = "Contact Provider"
//        configureTopBar()
        
        self.loadDummyMessages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.shared.enable = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
//        DataStore.shared.markSeenChatRoom(chatRoomId: self.chatRoomId)
    }
    
    func loadDummyMessages()
    {
        let uniqueID = NSUUID().uuidString
        
        var message = Message.init(text: "On way reaching in 5 mins", sender: otherSender as! Sender, messageId: uniqueID, date: Date())
        self.messageList.append(message)
        
        message = Message.init(text: "Okay I am waiting", sender: ownSender as! Sender, messageId: uniqueID, date: Date())
        self.messageList.append(message)
        
        self.reloadCollectionView()
    }
    
    func updateDataStore()
    {
//        DataStore.shared.chatRoomId = chatRoomId
        DataStore.shared.ownSender = (ownSender as! Sender)

//        DataStore.shared.senders = [ownSender, otherSender]
    }
    
    override func loadFirstMessages() {
//        DataStore.shared.getMessagesOnce { (messages) in
//            DispatchQueue.main.async {
//                self.messageList = messages
//                self.reloadCollectionView()
//                self.configureOtherListeners(paginate: false)
//                DataStore.shared.markSeenChatRoom(chatRoomId: self.chatRoomId)
//            }
//        }
    }
    
    override func loadMoreMessages() {
        paginate()
    }
    
    //MARK:- PAGINATION WORK
    func paginate()
    {
//        DataStore.shared.currentPaginationIndex = self.messageList.count + DataStore.shared.pageLimit
//
//        DataStore.shared.getMessagesOnce { (messages) in
//            DispatchQueue.main.async {
//                self.messageList = messages
//                self.messagesCollectionView.reloadDataAndKeepOffset()
//                self.refreshControl.endRefreshing()
//                self.configureOtherListeners(paginate: true)
//                DataStore.shared.markSeenChatRoom(chatRoomId: self.chatRoomId)
//            }
//        }
    }
    
    //MARK:- CONFIGURE LISTENERS
    func configureOtherListeners(paginate: Bool)
    {
        var lastTimeStamp: Double = 0
        var firstTime: Double = 0
        if(messageList.count>0)
        {
            lastTimeStamp = (self.messageList.last?.sentDate.timeIntervalSince1970)!
            firstTime = (self.messageList.first?.sentDate.timeIntervalSince1970)!
        }
        
        configRemovedListener(firstTimeStamp: firstTime*1000)
        
        if(!paginate)
        {
            configNewListener(lastTimeStamp: lastTimeStamp*1000)
        }
    }
    
    //CONFIGURE NEW AND REMOVE LISTENERS
    func configNewListener(lastTimeStamp: Double)
    {
//        DataStore.shared.listenToNewMessages(lastTimeStamp: lastTimeStamp) { (msg) in
//            //append and insert new message
//            if let index = self.messageList.lastIndex(where: {$0.messageId == msg.messageId})
//            {
//                return
//            }
//            DispatchQueue.main.async {
//                self.insertMessage(msg)
//                self.messagesCollectionView.scrollToBottom(animated: true)
//            }
//        }
    }
    
    func configRemovedListener(firstTimeStamp: Double)
    {
//        DataStore.shared.listenToRemovedMessagesOnly(firstTimeStamp: firstTimeStamp) { (msg) in
//            DispatchQueue.main.async {
//                if let index = self.messageList.lastIndex(where: {$0.messageId == msg.messageId})
//                {
//                    self.messageList.remove(at: index)
//                }
//                self.messagesCollectionView.reloadData()
//            }
//        }
    }
    
    func reloadCollectionView()
    {
        DispatchQueue.main.async {
            self.messagesCollectionView.reloadData()
            self.messagesCollectionView.scrollToBottom()
        }
    }
    
    override func configureMessageCollectionView() {
        super.configureMessageCollectionView()
        
        let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout
        layout?.sectionInset = UIEdgeInsets(top: 1, left: 8, bottom: 1, right: 8)
        
        // Hide the outgoing avatar and adjust the label alignment to line up with the messages
        layout?.setMessageOutgoingAvatarSize(.zero)
        layout?.setMessageIncomingAvatarSize(.zero)
        
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
    }
    
    override func configureMessageInputBar() {
        super.configureMessageInputBar()
        
        //messageInputBar.delegate = self
        messageInputBar.isTranslucent = true
        messageInputBar.separatorLine.isHidden = true
//        messageInputBar.inputTextView.tintColor = .de
//        messageInputBar.inputTextView.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        messageInputBar.inputTextView.placeholderTextColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
//        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 36)
//        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 36)
        messageInputBar.inputTextView.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1).cgColor
        messageInputBar.inputTextView.layer.borderWidth = 1.0
        messageInputBar.inputTextView.layer.cornerRadius = 16.0
        messageInputBar.inputTextView.layer.masksToBounds = true
        messageInputBar.inputTextView.scrollIndicatorInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        configureInputBarItems()
    }
    
    private func configureInputBarItems() {
        messageInputBar.separatorLine.alpha = 1.0
        
        messageInputBar.sendButton.contentEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        messageInputBar.sendButton.setSize(CGSize(width: 36, height: 36), animated: false)
        messageInputBar.sendButton.image = UIImage(named: "chat_sendButton")?.withRenderingMode(.alwaysTemplate)
        messageInputBar.sendButton.title = nil
        messageInputBar.sendButton.tintColor = ChatTheme.sendButtonTint()
        messageInputBar.inputTextView.layer.borderColor = UIColor.clear.cgColor
        messageInputBar.inputTextView.layer.borderWidth = 0.0
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        messageInputBar.setRightStackViewWidthConstant(to: 38, animated: false)
        
        messageInputBar.setStackViewItems([messageInputBar.sendButton], forStack: .right, animated: true)
    }
    
    // MARK: - Helpers
    
    func isTimeLabelVisible(at indexPath: IndexPath) -> Bool {
        return indexPath.section % 3 == 0 && !isPreviousMessageSameSender(at: indexPath)
    }
    
    func isPreviousMessageSameSender(at indexPath: IndexPath) -> Bool {
        guard indexPath.section - 1 >= 0 else { return false }
        return true//messageList[indexPath.section].sender == messageList[indexPath.section - 1].sender
    }
    
    func isNextMessageSameSender(at indexPath: IndexPath) -> Bool {
        guard indexPath.section + 1 < messageList.count else { return false }
        return true//messageList[indexPath.section].sender == messageList[indexPath.section + 1].sender
    }
    
    func setTypingIndicatorHidden(_ isHidden: Bool, performUpdates updates: (() -> Void)? = nil) {
        updateTitleView(title: "MessageKit", subtitle: isHidden ? "2 Online" : "Typing...")
    }
    
//    private func makeButton(named: String) -> InputBarButtonItem {
//        return InputBarButtonItem()
//            .configure {
//                $0.spacing = .fixed(10)
//                $0.image = UIImage(named: named)?.withRenderingMode(.alwaysTemplate)
//                $0.setSize(CGSize(width: 25, height: 25), animated: false)
//                $0.tintColor = UIColor(white: 0.8, alpha: 1)
//            }.onSelected {
//                $0.tintColor = .primaryColor
//            }.onDeselected {
//                $0.tintColor = UIColor(white: 0.8, alpha: 1)
//            }.onTouchUpInside { _ in
//                print("Item Tapped")
//        }
//    }
    
    // MARK: - UICollectionViewDataSource
    
    public override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let message = self.messageList[indexPath.section]
        
        if case .custom = message.kind {
            let cell = messagesCollectionView.dequeueReusableCell(CustomCell.self, for: indexPath)
            cell.configure(with: message, at: indexPath, and: messagesCollectionView)
            return cell
        }
        
        let cell = messagesCollectionView.dequeueReusableCell(CustomCell.self, for: indexPath)
        cell.configure(with: message, at: indexPath, and: messagesCollectionView)
        
        if(isFromCurrentSender(message: message))
        {
            cell.backgroundColor = ChatTheme.chatBubbleOutbox()
            cell.label.textColor = ChatTheme.chatOutgoingTextColor()
//            cell.removeShadow()
        }
        else
        {
            cell.backgroundColor = ChatTheme.chatBubbleInbox()
            cell.label.textColor = ChatTheme.chatIncomingTextColor()
//            cell.addShadow()
        }
//        let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView)
//        if case .custom = message.kind {
//            let cell = messagesCollectionView.dequeueReusableCell(CustomCell.self, for: indexPath)
//            cell.configure(with: message, at: indexPath, and: messagesCollectionView)
//            return cell
//        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    // MARK: - MessagesDataSource
    
    override func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if isTimeLabelVisible(at: indexPath) {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }
    
    override func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//        if !isPreviousMessageSameSender(at: indexPath) {
//            let name = message.sender.displayName
//            return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
//        }
        return nil
    }
    
    override func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
//        if !isNextMessageSameSender(at: indexPath) && isFromCurrentSender(message: message) {
//            return NSAttributedString(string: "Delivered", attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
//        }
        return nil
    }
    
    //MARK:- DATA METHODS
    func sendMessage(msg: Message)
    {
//        DataStore.shared.writeNewMessage(newChatRoom: false, msg: msg, chatRoomId: self.chatRoomId) { (msg) in
//            self.reloadCollectionView()
//        }
    }
    
}

// MARK: - MessagesDisplayDelegate

extension NewConversationViewController: MessagesDisplayDelegate {
    
    // MARK: - Text Messages
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : mainBlue
    }
    
    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
        return MessageLabel.defaultAttributes
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date, .transitInformation]
    }
    
    // MARK: - All Messages
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? ChatTheme.chatBubbleOutbox() : ChatTheme.chatBubbleInbox()
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        
        var corners: UIRectCorner = []
        
        if isFromCurrentSender(message: message) {
            corners.formUnion(.topLeft)
            corners.formUnion(.bottomLeft)
            if !isPreviousMessageSameSender(at: indexPath) {
                corners.formUnion(.topRight)
            }
            if !isNextMessageSameSender(at: indexPath) {
                corners.formUnion(.bottomRight)
            }
        } else {
            corners.formUnion(.topRight)
            corners.formUnion(.bottomRight)
            if !isPreviousMessageSameSender(at: indexPath) {
                corners.formUnion(.topLeft)
            }
            if !isNextMessageSameSender(at: indexPath) {
                corners.formUnion(.bottomLeft)
            }
        }
        
        switch message.kind {
        case .photo(let data):
            let configurationClosure = { (containerView: UIImageView) in
                let imageMask = UIImageView()
                imageMask.image = MessageStyle.bubble.image
                imageMask.frame = containerView.bounds
                containerView.mask = imageMask
                containerView.contentMode = .scaleToFill
                containerView.kf.indicatorType = .activity
                
                guard let url: URL = data.url
                    
                    else {
                        print("Could not convert message into a readable Message format")
                        return
                }
                
                print("Setting image to \(url.absoluteString)")
                
                containerView.kf.setImage(with: url)
                //                containerView.frame = cell.imageView.frame
            }
            return .custom(configurationClosure)
        default:
            break
        }
        
        return .custom { view in
            let radius: CGFloat = 8
            let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            view.layer.mask = mask
        }
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        avatarView.isHidden = isNextMessageSameSender(at: indexPath)
        avatarView.layer.borderWidth = 2
        avatarView.layer.borderColor = UIColor.white.cgColor
        avatarView.kf.indicatorType = .activity
        avatarView.contentMode = .scaleToFill
        avatarView.kf.setImage(with: URL.init(string: otherAvatarURL))
    }
    
}

// MARK: - MessagesLayoutDelegate

extension NewConversationViewController: MessagesLayoutDelegate {
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        if isTimeLabelVisible(at: indexPath) {
            return 18
        }
        return 0
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        if isFromCurrentSender(message: message) {
            return !isPreviousMessageSameSender(at: indexPath) ? 20 : 0
        } else {
            return !isPreviousMessageSameSender(at: indexPath) ? (20 + outgoingAvatarOverlap) : 0
        }
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return (!isNextMessageSameSender(at: indexPath) && isFromCurrentSender(message: message)) ? 16 : 0
    }
    
}

// MARK: - MessageInputBarDelegate

//extension NewConversationViewController: MessageInputBarDelegate {
//
//    private func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
//
//        for component in inputBar.inputTextView.components {
//
//            if let str = component as? String {
//
//                let message = Message(attributedText: NSAttributedString.init(string: str), sender: currentSender() as! Sender, messageId: UUID().uuidString, date: Date())
//                self.sendMessage(msg: message)
//            } else if let img = component as? UIImage {
//                let message = Message(image: img, sender: currentSender() as! Sender, messageId: UUID().uuidString, date: Date())
//                self.sendMessage(msg: message)
//            }
//        }
//        inputBar.inputTextView.text = String()
//        messagesCollectionView.scrollToBottom(animated: true)
//    }
//
//}
