//
//  ChatTheme.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/26/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import UIKit

class ChatTheme
{
    static func sendButtonTint() -> UIColor {
        return UIColor.white
//        return UIColor.init(rgb: 0xec405d)
    }
    
    static func chatTextSize() -> CGFloat {
        return 15.0
    }
    
    static func chatBubbleOutbox () -> UIColor {
        return mainBlue
    }
    
    static func chatBubbleInbox () -> UIColor  {
        return UIColor.lightGray
    }
    
    static func chatIncomingTextColor () -> UIColor {
        return mainBlue
    }
    
    static func chatOutgoingTextColor () -> UIColor {
        return UIColor.white
    }
    
    static func middleTextColor()-> UIColor {
        return UIColor.black.withAlphaComponent(0.36)
    }
}
