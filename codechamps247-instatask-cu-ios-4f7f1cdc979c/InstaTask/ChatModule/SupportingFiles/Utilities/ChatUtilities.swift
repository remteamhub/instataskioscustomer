//
//  ChatUtilities.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 12/3/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import UIKit

class ChatUtilities
{
//    static var hud = MBProgressHUD()
    
    //MARK: - Progress loader code block
    
//    class func showProgressLoader(view : UIView) {
//        DispatchQueue.main.async {
//            hud = MBProgressHUD.showAdded(to: view, animated: true)
//            hud.show(animated: true)
//        }
//    }
//
//    class func hideProgressLoader(view : UIView) {
//        DispatchQueue.main.async{
//            hud.hide(animated: true)
//            MBProgressHUD.hide(for: view, animated: true)
//        }
//
//    }
    
    class func showLoaderToStatusBar() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    class func hideLoaderFromStatusBar() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK:- Verification methods
    class func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    class func showError(title: String, message: String) -> UIAlertController
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(okAction)
        
        return alertController
    }
}
