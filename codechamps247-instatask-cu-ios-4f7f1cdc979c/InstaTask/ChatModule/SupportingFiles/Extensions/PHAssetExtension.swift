//
//  PHAssetExtension.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/29/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Photos
import UIKit

extension PHAsset {
    
    func returnImage(completion: @escaping(UIImage?) -> Void)
    {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.resizeMode = .exact
        
        manager.requestImage(for: self, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: option, resultHandler: {(result, info) -> Void in
            // do your stuff
            completion(result)
        })
    }
}
