//
//  RequestDetailViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/29/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit

class RequestDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var jobDetails = PreviousJobModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI()
    {
        self.whiteTopTitle(title: Utilities.UnixToCurrentTime(unixTime: jobDetails.job_schedual_time))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 356
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "RequestDetailCell", bundle: nil), forCellReuseIdentifier: "RequestDetailCell")
        
        tableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RequestDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //change for history and scheduled
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 356
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestDetailCell") as! RequestDetailCell
        
        cell.updateMarker(lat: jobDetails.lat, long: jobDetails.lng)
        cell.setImages(customer: jobDetails.provider.avatar)
        cell.cardLbl.text = jobDetails.user.stripe_unique_id
        cell.timeLbl.text = Utilities.UnixToTime(unixTime: jobDetails.job_schedual_time)
        cell.providerName.text = "\(jobDetails.provider.first_name) \(jobDetails.provider.last_name)"
        cell.priceAndLane.text = "$\(jobDetails.service_price) / \(jobDetails.service_name)"
        cell.rating.value = CGFloat(jobDetails.driver_rating)
        
        return cell
    }
    
}
