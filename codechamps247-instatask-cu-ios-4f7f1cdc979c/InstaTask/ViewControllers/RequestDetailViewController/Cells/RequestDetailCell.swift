//
//  RequestDetailCell.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/29/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import GoogleMaps
import HCSStarRatingView

class RequestDetailCell: UITableViewCell {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var cardLbl: UILabel!
    @IBOutlet weak var priceAndLane: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var rating: HCSStarRatingView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImgView.layer.cornerRadius = profileImgView.frame.size.width / 2
        profileImgView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateMarker(lat: Double, long: Double)
    {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15.0)
        mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = UIImage.init(named: "locationMarker")
        marker.map = mapView
        
        mapView.isUserInteractionEnabled = false
    }
    
    func setImages(customer avatar: String){
        self.profileImgView.kf.indicatorType = .activity
        self.profileImgView.kf.setImage(with: URL(string: avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
    }
    
    @IBAction func onclick_submitIssue(_ sender: Any) {
        NotificationCenter.default.post(.init(name: .openHelp))
    }
    
}
