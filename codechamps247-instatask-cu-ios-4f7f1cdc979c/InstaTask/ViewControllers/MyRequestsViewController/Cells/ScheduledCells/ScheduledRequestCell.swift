//
//  ScheduledRequestCell.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/24/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//



import UIKit
import GoogleMaps

class ScheduledRequestCell: UITableViewCell {
   
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var timeValue: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var trackButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateMarker(lat: Double, long: Double)
    {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15.0)
        mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = UIImage.init(named: "locationMarker")
        marker.map = mapView
        
        mapView.isUserInteractionEnabled = false
    }
    
}
