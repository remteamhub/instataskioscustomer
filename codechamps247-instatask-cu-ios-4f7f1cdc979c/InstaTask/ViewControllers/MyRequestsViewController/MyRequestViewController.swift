//
//  MyRequestViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import HMSegmentedControl

class MyRequestViewController: UIViewController {
    
    @IBOutlet weak var topSegmentControl: HMSegmentedControl!
    @IBOutlet weak var emptyLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var mode = 0 //0 for scheduled and 1 for history
    
    var history: [String] = ["","",""]
    var selectedIndex: Int = 0
    var data = PreviousJobModel()
    var pendingData = PendingJobModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        if User.shared.isUserSkipLogin == false {
            NetworkManager.checkPendingJobsApi(params: ["type" : "0", "user_id":"\(User.shared.id)"], delegate: self, showHUD: true)
        }
        setupUI()
    }
    
    func setupUI()
    {
        whiteTopTitle(title: "My Requests")
        emptyLbl.isHidden = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        topSegmentControl.sectionTitles = ["Scheduled", "History"]
        topSegmentControl.selectionIndicatorColor = mainBlue
        topSegmentControl.selectionStyle = .fullWidthStripe
        topSegmentControl.selectionIndicatorLocation = .down
        topSegmentControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: mainBlue]
        
        topSegmentControl.addTarget(self, action: #selector(segmentedControlChangedValue), for: UIControl.Event.valueChanged)
        
        if User.shared.isUserSkipLogin == false {
            emptyLbl.isHidden = true
            emptyLbl.text = ""
        }else{
            emptyLbl.isHidden = false
            emptyLbl.text = "SignIn/Signup to use this feature."
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 605
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "ScheduledRequestCell", bundle: nil), forCellReuseIdentifier: "ScheduledRequestCell")
        tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        
        //tableView.reloadData()
    }
    

    //MARK:- SEGMENT CONTROL CHANGED
    @objc func segmentedControlChangedValue(segment : HMSegmentedControl) {
        if User.shared.isUserSkipLogin == false {
            emptyLbl.isHidden = true
            emptyLbl.text = ""
            self.mode = segment.selectedSegmentIndex
            if segment.selectedSegmentIndex == 0 {
                NetworkManager.checkPendingJobsApi(params: ["type" : "0", "user_id":"\(User.shared.id)"], delegate: self, showHUD: true)
            }else{
                NetworkManager.checkPreviousJobsApi(params: ["type" : "0", "user_id":"\(User.shared.id)"], delegate: self, showHUD: true)
            }
        }else{
            emptyLbl.isHidden = false
            emptyLbl.text = "SignIn/Signup to use this feature."
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "openRequestDetail")
        {
            let destination = segue.destination as! RequestDetailViewController
            //set history item here
            destination.jobDetails = self.data
        }
        if (segue.identifier == "showChatFromHistory") {
            let destination = segue.destination as! ProviderChatController
            //set history item here
            destination.jobModel = self.pendingData
        }
    }
    
    @objc func contactProviderPressed()
    {
        self.performSegue(withIdentifier: "showChatFromHistory", sender: self)
    }

}


extension MyRequestViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //change for history and scheduled
        if(mode == 0)
        {
            return PendingJobModel.shared.pendingData.count
        }
        else if(mode == 1)
        {
            return PreviousJobModel.shared.previousData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(mode == 0)
        {
            return 605
        }
        else if(mode == 1)
        {
            return 201
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(mode == 0) // scheduled
        {
            return getCellForScheduled(indexPath: indexPath)
        }
        else if(mode == 1) // history
        {
            return getCellForHistory(indexPath: indexPath)
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(mode == 1) //only allow selection of history
        {
            selectedIndex = indexPath.row
            let data = PreviousJobModel.init(dic: PreviousJobModel.shared.previousData.reversed()[indexPath.row] as! [String:Any])
            self.data = data
            self.performSegue(withIdentifier: "openRequestDetail", sender: self)
        }
    }
    
    //MARK:- TABLEVIEW UTILITY METHODS
    
    func getCellForScheduled(indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduledRequestCell") as! ScheduledRequestCell
        
        let data = PendingJobModel.init(dic: PendingJobModel.shared.pendingData[indexPath.row] as! [String:Any])
        cell.updateMarker(lat: data.lat, long: data.lng)
        cell.serviceTypeLabel.text! = data.service_name
        cell.locationLabel.text! = data.location_address
        cell.providerName.text! = "\(data.provider.first_name) \(data.provider.last_name)"
        cell.paymentLabel.text! = "\(data.user.stripe_unique_id)"
        cell.timeValue.text = "\(Utilities.UnixToCurrentTime(unixTime: data.job_schedual_time))"
        cell.selectionStyle = .none
        cell.contactButton.addTarget(self, action: #selector(contactProviderPressed), for: .touchUpInside)
        self.pendingData = data
        cell.selectionStyle = .none
        return cell
    }
    
    func getCellForHistory(indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        
        let data = PreviousJobModel.init(dic: PreviousJobModel.shared.previousData.reversed()[indexPath.row] as! [String:Any])
        cell.updateMarker(lat: data.lat, long: data.lng)
        cell.costLabel.text = "$\(data.service_price)"
        cell.dateTimeLabel.text = "\(Utilities.UnixToCurrentTime(unixTime: data.job_schedual_time))"
        cell.userTypeLabel.text = "\(data.provider.first_name) \(data.provider.last_name)"
        cell.rating.value = CGFloat(data.customer_rating)
        
        cell.selectionStyle = .none
        return cell
    }
    
}

extension MyRequestViewController: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiCheckPreviousJobs{
            
            let data = dataArray as! [String:Any]
            if let dataArr = data["data"] as? NSArray {
                PreviousJobModel.shared.previousData.removeAll()
                PreviousJobModel.shared.previousData.append(contentsOf: dataArr)
            }
            if PreviousJobModel.shared.previousData.count == 0 {
                emptyLbl.isHidden = false
                emptyLbl.text = "No History"
            }else{
                emptyLbl.isHidden = true
            }
            self.tableView.reloadData()
            
        }else if api == .eApiCheckPendingJobs {
            
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            let data = dataArray as! [String:Any]
            if let dataArr = data["data"] as? NSDictionary {
                PendingJobModel.shared.pendingData.removeAll()
                let jobData = dataArr["job"] as! NSArray
                PendingJobModel.shared.pendingData.append(contentsOf: jobData)
            }
            if PendingJobModel.shared.pendingData.count == 0 {
                emptyLbl.isHidden = false
                emptyLbl.text = "No Job"
            }else{
                emptyLbl.isHidden = true
            }
            tableView.reloadData()
            
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiCheckPreviousJobs {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
}
