//
//  EditJobVC.swift
//  InstaTask
//
//  Created by Arqam Butt on 7/23/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

class EditJobVC: ViewController {

    @IBOutlet weak var confirm: UIButton!
    @IBOutlet weak var dropOffBtn: UIButton!
    @IBOutlet weak var laneOne: UIButton!
    @IBOutlet weak var laneTwo: UIButton!
    @IBOutlet weak var laneThree: UIButton!
    @IBOutlet weak var backView: UIView!
    
    var editjob = CurrentJobModel()
    var params = [String:Any]()
    
    override func viewDidLoad() {
        NetworkManager.servicesApi(params: ["fcm_token" : "\(APP_Delegate.deviceTokenString)"], delegate: self, showHUD: true)
        super.viewDidLoad()
        self.addCornorRadius(buttons: [confirm,dropOffBtn,backView])
        self.defaultLanes()
        
        print(editjob.location_address)
        dropOffBtn.setTitle(editjob.location_address, for: .normal)
        params["id"] = Extra.shared.tempJobId
        params["customer_approval"] = editjob.customer_approval
        params["job_status"] = editjob.job_status
        params["lat"] = editjob.lat
        params["lng"] = editjob.lng
        params["location_address"] = editjob.location_address
    }
    
    func addCornorRadius(buttons: [UIView]){
        for i in buttons {
            i.layer.cornerRadius = 10
            i.clipsToBounds = true
        }
    }

    func defaultLanes(){
        laneOne.setImage(UIImage.init(named: "1lane-notSelected"), for: .normal)
        laneTwo.setImage(UIImage.init(named: "2lane-notSelected"), for: .normal)
        laneThree.setImage(UIImage.init(named: "2pluslane-notSelected"), for: .normal)
    }
    
    @IBAction func confirm_Action(_ sender: Any) {
        
        params["customer_id"] = User.shared.id
        print(params)
        if Extra.shared.tempJobId != 0 && params["service_price"] != nil && params["title"] != nil && params["lat"] != nil && params["lng"] != nil && params["location_address"] != nil{
            NetworkManager.createJobsApi(params: params, delegate: self, showHUD: true)
        }else {
            Utilities.showAlert(title: "Error", message: "Something went wrong.")
        }
        
        
    }

    @IBAction func dropOff_Action(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func laneOne_Action(_ sender: Any) {
        laneOne.setImage(UIImage.init(named: "1lane"), for: .normal)
        laneTwo.setImage(UIImage.init(named: "2lane-notSelected"), for: .normal)
        laneThree.setImage(UIImage.init(named: "2pluslane-notSelected"), for: .normal)
        let dic = ServicesModel.init(dic: ServicesModel.shared.serviceArray[0] as! [String: Any])
        print(dic.title)
        params["service_price"] = dic.price
        params["title"] = dic.title
    }
    
    @IBAction func laneTwo_Action(_ sender: Any) {
        laneOne.setImage(UIImage.init(named: "1lane-notSelected"), for: .normal)
        laneTwo.setImage(UIImage.init(named: "2lane"), for: .normal)
        laneThree.setImage(UIImage.init(named: "2pluslane-notSelected"), for: .normal)
        let dic = ServicesModel.init(dic: ServicesModel.shared.serviceArray[1] as! [String: Any])
        print(dic.title)
        params["service_price"] = dic.price
        params["title"] = dic.title
    }
    
    @IBAction func laneThree_Action(_ sender: Any) {
        laneOne.setImage(UIImage.init(named: "1lane-notSelected"), for: .normal)
        laneTwo.setImage(UIImage.init(named: "2lane-notSelected"), for: .normal)
        laneThree.setImage(UIImage.init(named: "2pluslane"), for: .normal)
        let dic = ServicesModel.init(dic: ServicesModel.shared.serviceArray[2] as! [String: Any])
        print(dic.title)
        params["service_price"] = dic.price
        params["title"] = dic.title
    }
}

//MARK:- PLACES AUTOCOMPLETE DELEGATE
extension EditJobVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
       
        dropOffBtn.setTitle(place.formattedAddress, for: .normal)
        params["lat"] = place.coordinate.latitude
        params["lng"] = place.coordinate.longitude
        params["location_address"] = place.formattedAddress
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func getLocationAddress(lat: Double,long: Double, result:@escaping(([String])->()))  {
        let geoCoder = GMSGeocoder()
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        geoCoder.reverseGeocodeCoordinate(coordinates) { (placeMarkers, error) in
            if let placeMarker = placeMarkers {
                if let address = placeMarker.firstResult()?.lines {
                    result(address)
                }
            }
        }
    }
    
}

extension EditJobVC: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiServices {
            let data = dataArray as! NSDictionary
            let dataArr = data["data"] as! NSArray
            print("Received Data: ", dataArr)
            ServicesModel.shared.serviceArray.append(contentsOf: dataArr)
        }else if api == .eApiCreateJobs {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
//            let resp = dataArray as! NSDictionary
//            let data = resp["data"] as! NSDictionary
//            if let jobId = data["last_insert_id"] as? Int {
//                Extra.shared.tempJobId = jobId
//            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiServices {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
}
