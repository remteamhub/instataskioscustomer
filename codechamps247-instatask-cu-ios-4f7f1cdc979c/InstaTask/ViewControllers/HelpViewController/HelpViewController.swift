//
//  HelpViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/27/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    @IBOutlet weak var kindIssueTF: UITextField!
    @IBOutlet weak var typeIssueTF: UITextField!
    @IBOutlet weak var descriptionView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    func setupUI()
    {
        whiteTopTitle(title: "Help")
    }
    
    func checkFields() -> Bool
    {
        if(self.kindIssueTF.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter title for your issue.")
            return false
        }
        else if(self.typeIssueTF.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter your issue type.")
            return false
        }else if(self.descriptionView.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Description can't be empty.")
            return false
        }
        
        return true
    }
    
    @IBAction func onclick_submit(_ sender: Any) {
        if User.shared.isUserSkipLogin == false {
            if checkFields() {
                let params = [
                    "user_id":"\(User.shared.id)",
                    "user_type": "0",
                    "issue_name":"\(kindIssueTF.text!)",
                    "issue_type":"\(typeIssueTF.text!)",
                    "issue_description":"\(descriptionView.text!)",
                    "status":"Satisfied"
                ]
                
                NetworkManager.helpSupportApi(params: params, delegate: self, showHUD: true)
            }
        }else{
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HelpViewController: RedirectDelegate {
    func redirectTo() {
        User.shared.deleteUser {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}


extension HelpViewController: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiCreatIssue {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            let alertController = UIAlertController(title: "Successful!", message: "Your issue has been received. Our support center will contact you shortly.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default) { (alert) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiCreatIssue {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
