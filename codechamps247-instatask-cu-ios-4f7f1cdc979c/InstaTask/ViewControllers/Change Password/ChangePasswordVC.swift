//
//  ChangePasswordVC.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 24/10/2019.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var repeatPassTF: UITextField!
    @IBOutlet weak var newPassTF: UITextField!
       
    var delegate: ChangePasswordDelegate?

    override func viewDidLoad() {
       super.viewDidLoad()
       cancelBtn.layer.borderColor = mainBlue.cgColor
       cancelBtn.layer.borderWidth = 1.0
       cancelBtn.layer.cornerRadius = 6
       confirmBtn.layer.cornerRadius = 6
       // Do any additional setup after loading the view.
    }


    func checkFields() -> Bool
    {
       if((self.newPassTF.text?.count)! < 6)
       {
           self.displayAlertWithOk(title: "Error", message: "Password cannot be of less than 6 characters.")
           return false
       }
       
       if(self.newPassTF.text == "" || self.repeatPassTF.text == "")
       {
           self.displayAlertWithOk(title: "Error", message: "Please check fields. All fields are mandatory")
           return false
       }
       
       return true
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       // Get the new view controller using segue.destination.
       // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onclick_confirm(_ sender: Any) {
       if checkFields() {
           print("EveryThings work.")
           let params = [
               "new_password":"\(repeatPassTF.text!)"
           ]
           self.dismiss(animated: true, completion: nil)
           delegate?.didUpdateData(params: params)
       }
    }
    @IBAction func onclick_cancel(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }

}
