//
//  ProfileViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Kingfisher
import Photos

class ProfileViewController: UIViewController, ChangePasswordDelegate {
    
    @IBOutlet weak var profileImgTopView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var nameTF: UITextField!
    
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var requestsLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var milesLbl: UILabel!
    @IBOutlet weak var updateBtn: UIButton!
    
    var fileURL:URL?
    var check = false
    var params = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if User.shared.social_id != "" {
            editBtn.isHidden = false
        }else{
            editBtn.isHidden = true
        }
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI()
    {
        whiteTopTitle(title: "Profile")
        profileImgView.layer.cornerRadius = profileImgView.frame.size.width / 2
        profileImgView.clipsToBounds = true
        profileImgView.kf.indicatorType = .activity
        profileImgView.kf.setImage(with: URL(string: imageBackURL+User.shared.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
        
        cameraButton.layer.cornerRadius = cameraButton.frame.size.width / 2
        cameraButton.clipsToBounds = true
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "pencil-black-64") , style: .plain, target: self, action: #selector(buttonTapped))
        
        profileImgView.backgroundColor = .lightGray
        
        self.disableEditing()
        updateBtn.layer.cornerRadius = 6
        
        radiusSlider.value = Float(Extra.shared.tempMiles)
        milesLbl.text = "\(Extra.shared.tempMiles) Miles"
        
        nameTF.text = "\(User.shared.first_name) \(User.shared.last_name)"
        contactLabel.text = User.shared.phone
        emailLabel.text = User.shared.email
        ratingView.allowsHalfStars = true
        ratingView.value = CGFloat(round(100*User.shared.rating)/100)
        ratingLabel.text = "\(round(100*User.shared.rating)/100)/5.0"
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func disableEditing(){
        nameTF.isEnabled = false
        updateBtn.isHidden = true
    }
    
    func enableEditing(){
        nameTF.isEnabled = true
        updateBtn.isHidden = false
    }
    
    func checkEditingState(){
        if check == false {
            check = true
            self.enableEditing()
        }else{
            check = false
            self.disableEditing()
        }
    }
    
    @objc func buttonTapped(){
        print("Edit button Tapped")
        
        if User.shared.isUserSkipLogin == false {
            self.checkEditingState()
        }else {
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
        
    }
    
    func didUpdateData(params: [String : Any]) {
        self.params = params
    }
    
    @IBAction func onClickUpdateBtn(_ sender: Any) {
        
        let names = nameTF.text!.split(separator: " ")
        if !nameTF.text!.contains(" ") || names.last! == names.first! {
            self.displayAlert(title: "Error", message: "Please enter your full name.")
            return
        }
        if nameTF.text! == "" {
            self.displayAlert(title: "Error", message: "Name filed empty.")
            return
        }
//        if (User.shared.radius != Int(radiusSlider.value)) {
//            params["radius"] = "\(Int(radiusSlider.value))"
//        }else{
//            params["radius"] = "\(User.shared.radius)"
//        }
        params["user_id"] = "\(User.shared.id)"
        params["fcm_token"] = "\(APP_Delegate.deviceTokenString)"
        params["first_name"] = names.first!
        params["last_name"] = names.last!
        print(params)
        if let file = fileURL {
            NetworkManager.updateLicenseApi(params: params, file: file, delegate: self, showHUD: true)
        }else{
            NetworkManager.updateLicenseApi(params: params, file: nil, delegate: self, showHUD: true)
        }
        
        
    }
    
    //MARK:- BUTTON ACTIONS
    @IBAction func camerBtnPressed(_ sender: Any) {
        
        if User.shared.isUserSkipLogin == false {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            
            let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
            
            actionSheet.view.subviews.flatMap({$0.constraints}).filter{ (one: NSLayoutConstraint)-> (Bool)  in
                return (one.constant < 0) && (one.secondItem == nil) &&  (one.firstAttribute == .width)
                }.first?.isActive = false
            
        }else {
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
        
    }
    
    @IBAction func editContactPressed(_ sender: Any) {
    }
    
    @IBAction func editPasswordPressed(_ sender: Any) {
        self.enableEditing()
        check = true
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditPasswordVC") as! EditPasswordVC
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
        
        let currentValue = Int(sender.value)
        milesLbl.text = "\(currentValue) Miles"
        
    }
    
}

extension ProfileViewController: RedirectDelegate {
    func redirectTo() {
        User.shared.deleteUser {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}


extension ProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        
        if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
        }else if let editImage = info[.editedImage] as? UIImage {
            selectedImage = editImage
        }
        
        picker.dismiss(animated: true, completion: nil)
        
        if let data = selectedImage!.jpegData(compressionQuality: 0.5){
            
            self.fileURL = Utilities.getDocumentsDirectory()
            self.fileURL!.appendPathComponent("avatar_image.png")
            
            
            if FileManager.default.fileExists(atPath: self.fileURL!.path){
                do{
                    try FileManager.default.removeItem(at: fileURL!)
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            
            do{
                try data.write(to: self.fileURL!, options: .atomic)
            }catch{
                print(error.localizedDescription)
            }
            
            self.enableEditing()
            check = true
            
            let provider = LocalFileImageDataProvider(fileURL: self.fileURL!)
            self.profileImgView.kf.setImage(with: provider, options: [.forceRefresh])
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}


extension ProfileViewController: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiUpdateLicense {
            
            let data = dataArray as! [String:Any]
            let resp = data["data"] as! [String:Any]
            let userProfile = resp["userDetails"] as! [String:Any]
            Utilities.printPrettyJSONFromDic(params: userProfile)
            let user = User.init(dic: userProfile)
            User.shared.saveUser(user: user)
            NotificationCenter.default.post(.init(name: .displayNameUpdated))
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiUpdateLicense {
            self.displayAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
