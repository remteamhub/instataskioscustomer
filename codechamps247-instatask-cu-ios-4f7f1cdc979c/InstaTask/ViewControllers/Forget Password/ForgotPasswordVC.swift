//
//  ForgotPasswordVC.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 23/10/2019.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var showMessage: UILabel!
    var check = false
    var tempEmail = String()
    var params = [String : Any]()
    var userID = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.whiteTopTitle(title: "Forget Password")
        forgotPasswordBtn.layer.cornerRadius = 8
        textfield.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
        resetView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func resetView(){
        forgotPasswordBtn.setTitle("FORGOT PASSWORD", for: .normal)
        textfield.placeholder = "Email"
        showMessage.isHidden = true
    }
    
    func checkFields() -> Bool
    {
        if(self.textfield.text == "" && check == false)
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter email")
            return false
        }
        
        if(self.textfield.text == "" && check == true)
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter security code")
            return false
        }
        
        return true
    }
    
    
    @IBAction func forgetTapped(_ sender: Any) {
        if checkFields() {
            if check == false {
                NetworkManager.forgetPasswordApi(params: ["email":"\(textfield.text!)"], delegate: self, showHUD: true)
            }else{
                NetworkManager.getForgetCodeApi(params: ["email":"\(tempEmail)", "code":"\(textfield.text!)"], delegate: self, showHUD: true)
            }
        }
    }

}


extension ForgotPasswordVC: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiForgetPassword {
            UIView.animate(withDuration: 1.0) {
                self.check = true
                self.forgotPasswordBtn.setTitle("CONFIRM", for: .normal)
                self.textfield.placeholder = "Enter Code"
                self.tempEmail = self.textfield.text!
                self.textfield.text = ""
                self.showMessage.isHidden = false
            }
        }
        if api == .eApiGetFpCode {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            if let dataArr = dataArray as? [String:Any] {
                if let data = dataArr["data"] as? [String:Any] {
                    if let validity = data["validity"] as? Bool {
                        if validity == true {
                            self.userID = data["user_id"] as! Int
                            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                            vc.delegate = self
                            self.present(vc, animated: true, completion: nil)
                        }else{
                            self.displayAlertWithOk(title: "Error", message: "The entered code is not correct.")
                        }
                    }
                }
            }
            
        }
        if api == .eApiUpdatePassword {
            let alertController = UIAlertController(title: "Successful!", message: "Your password is succesfully updated. Kindly login again to use our services.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default) { (alert) in
                self.check = false
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiForgetPassword {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }
        if api == .eApiGetFpCode {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }
        if api == .eApiUpdatePassword {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }
    }
}

extension ForgotPasswordVC: ChangePasswordDelegate {
    func didUpdateData(params: [String : Any]) {
        print(params)
        self.params = params
        self.params["user_id"] = "\(userID)"
        self.params["fcm_token"] = "\(APP_Delegate.deviceTokenString)"
        self.params["no_old"] = "1"
        //Update Password on server
        NetworkManager.updatePasswordApi(params: self.params, delegate: self, showHUD: true)
    }
    
    
}
