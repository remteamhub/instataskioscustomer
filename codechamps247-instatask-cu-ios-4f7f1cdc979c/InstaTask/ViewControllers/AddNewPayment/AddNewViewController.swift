//
//  AddNewViewController.swift
//  InstaTask
//
//  Created by Arqam Butt on 8/22/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import UIKit
import Stripe
import CreditCardForm

class AddNewViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var cardHolderTF: UITextField!
    @IBOutlet weak var CardView: CreditCardFormView!
    let paymentTextField = STPPaymentCardTextField()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        whiteTopTitle(title: "Add Payments")
        createTextField()
        cardHolderTF.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        CardView.cardHolderString = textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        CardView.cardHolderString = textField.text!
        cardHolderTF.resignFirstResponder()
        return true
    }
    
    
    func createTextField() {
        paymentTextField.frame = CGRect(x: 15, y: 199, width: self.view.frame.size.width - 30, height: 44)
        paymentTextField.delegate = self
        paymentTextField.translatesAutoresizingMaskIntoConstraints = false
        paymentTextField.borderWidth = 0
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: paymentTextField.frame.size.height - width, width:  paymentTextField.frame.size.width, height: paymentTextField.frame.size.height)
        border.borderWidth = width
        paymentTextField.layer.addSublayer(border)
        paymentTextField.layer.masksToBounds = true
        
        view.addSubview(paymentTextField)
        
        NSLayoutConstraint.activate([
            paymentTextField.topAnchor.constraint(equalTo: CardView.bottomAnchor, constant: 20),
            paymentTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            paymentTextField.widthAnchor.constraint(equalToConstant: self.view.frame.size.width-20),
            paymentTextField.heightAnchor.constraint(equalToConstant: 44)
            ])
    }

    @IBAction func TappedButton(_ sender: Any){
        
        if cardHolderTF.text != "" {
            CardView.cardHolderString = cardHolderTF.text!
            let param = STPCardParams()
            param.name = cardHolderTF.text!
            param.number = paymentTextField.cardParams.number
            param.cvc = paymentTextField.cardParams.cvc
            param.expMonth = paymentTextField.cardParams.expMonth as! UInt
            param.expYear = paymentTextField.cardParams.expYear as! UInt
            
            STPAPIClient.shared().createToken(withCard: param) { (token: STPToken?, error: Error?) in
                guard let token = token, error == nil else {
                    Utilities.showAlert(title: "Error", message: error!.localizedDescription)
                    return
                }
                if token.tokenId != "" {
                    let params = [
                        "user_id":"\(User.shared.id)",
                        "token":"\(token.tokenId)"
                    ]
                    print(params)
                    NetworkManager.addCardApi(params: params, delegate: self, showHUD: true)
                }
            }
        }else{
            Utilities.showAlert(title: "Error", message: "Card Holder Name can't be empty.")
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AddNewViewController: STPPaymentCardTextFieldDelegate {
    
    // Mark: - Credeit Card Delegates
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        CardView.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: textField.expirationYear, expirationMonth: textField.expirationMonth, cvc: textField.cvc)
        
    }
    
    func paymentCardTextFieldDidEndEditingExpiration(_ textField: STPPaymentCardTextField) {
        CardView.paymentCardTextFieldDidEndEditingExpiration(expirationYear: textField.expirationYear)
        
    }
    
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
        CardView.paymentCardTextFieldDidBeginEditingCVC()
        
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        CardView.paymentCardTextFieldDidEndEditingCVC()
        
        print("Card Number---:--- \(textField.cardNumber!)")
    }
    
}

extension AddNewViewController: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiAddCard {
            
            if let data = dataArray as? [String:Any] {
               if let sources = data["sources"] as? NSDictionary {
                    UserStripeDetails.shared.deleteStripeData()
                    guard let defaultSource = data["default_source"] as? String else{return}
                    UserStripeDetails.shared.tempDefaultSource = defaultSource
                    let objData = sources["data"] as! NSArray
                    UserStripeDetails.shared.dataArr.append(contentsOf: objData)
                    UserStripeDetails.shared.savaStripeData(arr: objData)
                }else{
                    UserStripeDetails.shared.deleteStripeData()
                    UserStripeDetails.shared.dataArr.append(dataArray as! [String:Any])
                    UserStripeDetails.shared.savaStripeData(arr: UserStripeDetails.shared.dataArr as NSArray)
                }
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiAddCard {
            self.navigationController?.popViewController(animated: true)
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
