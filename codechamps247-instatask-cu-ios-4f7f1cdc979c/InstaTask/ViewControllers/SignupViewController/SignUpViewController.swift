//
//  SignUpViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
import FBSDKLoginKit
import TwitterKit

class SignUpViewController: UIViewController {
    
//    CustomerLoginCell
    @IBOutlet weak var tableView: UITableView!

    var params = [String:Any]()
    var user:User!
    var check = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 325
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "CustomerLoginCell", bundle: nil), forCellReuseIdentifier: "CustomerLoginCell")
        
        tableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "signUpSuccess")
        {
            self.navigationController?.isNavigationBarHidden = false
        }
        if segue.identifier == "verificationPhone" {
            let destination = segue.destination as! VerifyPhoneVC
            destination.delegate = self
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
//    func signUpTapped(){
//
//        self.performSegue(withIdentifier: "signUpSuccess", sender: self)
//    }

}

extension SignUpViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //change in case of influencer and business owner
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 710
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerLoginCell") as! CustomerLoginCell
        
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
}

extension SignUpViewController: SignUpDelegate
{
    func selectedSignIn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func signUpPressed(params: [String:Any]) {
        self.params = params
        check = false
//        loginWithPhone()
        self.performSegue(withIdentifier: "verificationPhone", sender: self)
    }
    
    func facebookPressed() {

        let readPermissions = ["email","public_profile"]
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: readPermissions, from: self)  { (result, error) -> Void in
            if (error == nil){
                if (result?.isCancelled)!{
                    // if user cancel the login
                }
                else
                {
                    if (FBSDKAccessToken.current() != nil) {
                        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id,name,email, picture.type(large)"]).start(completionHandler: { (icon, data, err) in
                            if err != nil {
                                print("Graph Request Error............................\(err!)")
                                return
                            }else{
                                
                                let params = data as! [String:Any]
                                let id = params["id"] as! String
                                let name = params["name"] as! String
                                guard let email = params["email"] as? String else{
                                    Utilities.showAlert(title: "Error", message: "You are not registed with email on facebook. Use facebook account with a registered email or register manually.")
                                    return
                                }
                                let pic = params["picture"] as! [String:Any]
                                let picData = pic["data"] as! [String:Any]
                                let picURL = picData["url"] as! String
                                
                                
                                let names = name.split(separator: " ")
                                let userParams = [
                                    "first_name":"\(names.first!)",
                                    "last_name":"\(names.last!)",
                                    "username": "\(names.first!)\(names.last!)\(id)",
                                    "email":"\(email)",
                                    "address": "empty",
                                    "avatar_file":"\(picURL)",
                                    "social_id":"\(id)",
                                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                                    "lat":"\(LocationCoordinator.shared.lat)",
                                    "lng":"\(LocationCoordinator.shared.lng)"
                                ]
                                
                                fbLoginManager.logOut()
                                self.callSignUpAPI(json: userParams)
                            }
                        })
                    }
                }
            } else {
                print(error?.localizedDescription ?? "Sign up View Controller -> fbLoginPressed")
            }
        }
    }
    
    func twitterPressed() {

        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                let currentUser = TWTRAPIClient.withCurrentUser()
                
                var userParams: [String:Any] = [
                    "username": "\(session!.userName)\(session!.userID)",
                    "address": "empty",
                    "social_id":"\(session!.userID)",
                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                    "lat":"\(LocationCoordinator.shared.lat)",
                    "lng":"\(LocationCoordinator.shared.lng)"
                ]
                
                currentUser.requestEmail { (email, error) in
                    if let ema = email {
                        userParams["email"] = ema
                    }
                    if let err = error {
                        Utilities.showAlert(title: "Error", message: "\(err)")
                        return
                    }
                }
                currentUser.loadUser(withID: session!.userID, completion: { (user, error) in
                    let names = user!.name.split(separator: " ")
                    userParams["first_name"] = names.first
                    userParams["last_name"] = names.last
                    userParams["avatar_file"] = user!.profileImageLargeURL
                    
                })
                
                self.callSignUpAPI(json: userParams)
                
            } else {
                print(error!.localizedDescription)
            }
        })
        
    }
    
    func showAlert(title: String, message: String) {
        self.displayAlertWithOk(title: title, message: message)
    }
    
    //MARK:- NETWORK CALL
    func callSignUpAPI(json: [String:Any])
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            NetworkManager.socialLoginApi(params: json, delegate: self, showHUD: true)
        })

    }
    
}

extension SignUpViewController: VerifyPhoneDelegate {
    
    func sendPhoneVerifyResponse(_ phone: String?, completion: (() -> ())) {
        
        self.params["phone"] = "\(phone!)"
        print("Params with phones", self.params)
        if self.check == true {
            let params = [
                "user_id":"\(User.shared.id)",
                "fcm_token":"\(APP_Delegate.deviceTokenString)",
                "phone":"\(phone!)"
            ]
            NetworkManager.updatePhoneApi(params: params, delegate: self, showHUD: true)
        }else{
            self.params["lat"] = "\(LocationCoordinator.shared.lat)"
            self.params["lng"] = "\(LocationCoordinator.shared.lng)"
            NetworkManager.signUpApi(params: self.params, delegate: self, showHUD: true)
        }
        
        completion()
    }
    
}


extension SignUpViewController: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiSignUp {
            let resp = dataArray as! [String:Any]
            let data = resp["data"] as! [String:Any]
            let userProfile = data["userDetails"] as! [String:Any]
            Utilities.printPrettyJSONFromDic(params: userProfile)
            let user = User.init(dic: userProfile)
            User.shared.saveUser(user: user)
            SocketManagerSingeleton.shared.connectToSocket()
            self.performSegue(withIdentifier: "signUpSuccess", sender: self)
        }else if api == .eApiSocialLogin {
            let resp = dataArray as! [String:Any]
            if let userProfile = resp["userProfile"] as? [String:Any] {
                Utilities.printPrettyJSONFromDic(params: userProfile)
                let user = User.init(dic: userProfile)
                if user.phone == "" {
                    self.user = user
                    check = true
//                    loginWithPhone()
                    self.performSegue(withIdentifier: "verificationPhone", sender: self)
                }else{
                    check = false
                    User.shared.saveUser(user: user)
                    SocketManagerSingeleton.shared.connectToSocket()
                    self.performSegue(withIdentifier: "signUpSuccess", sender: self)
                }
            }else {
                self.displayAlert(title: "Alert!", message: resp["status"] as! String)
            }
            
        }else if api == .eApiUpdatePhone {
            User.shared.saveUser(user: user)
            SocketManagerSingeleton.shared.connectToSocket()
            self.performSegue(withIdentifier: "signUpSuccess", sender: self)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiSignUp {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }else if api == .eApiSocialLogin {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }else if api == .eApiUpdatePhone {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }
    }
    
}
