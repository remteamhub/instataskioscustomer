//
//  CustomerLoginCell.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit

protocol SignUpDelegate{
    func signUpPressed(params: [String:Any])
    
    func facebookPressed()
    
    func twitterPressed()
    
    func showAlert(title: String, message: String)
    
    func selectedSignIn()
}

class CustomerLoginCell: UITableViewCell {

    @IBOutlet weak var termsLabel: UILabel!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    var delegate: SignUpDelegate!
    
//    var user: User!
    
    //OUTLETS
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordAgainField: UITextField!
    @IBOutlet weak var checkBox: CheckboxButton!
    
    @IBOutlet weak var signInButton: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    func setupUI()
    {
        let font = Theme.semiBoldFontWithSize(size: 15.0)
        // Colored Underline Label
        let termsStr1 = NSMutableAttributedString.init(string: "I have read the ", attributes: [NSAttributedString.Key.font: font])
        let termsStr2 = NSMutableAttributedString.init(string: " and I agree", attributes: [NSAttributedString.Key.font: font])
        let labelString = "Terms and Conditions"
        let textColor: UIColor = UIColor.init(rgb: 0xFEB401)
        let underLineColor: UIColor = UIColor.init(rgb: 0xFEB401)
        let underLineStyle = NSUnderlineStyle.single.rawValue
        
        let labelAtributes:[NSAttributedString.Key : Any]  = [
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.underlineStyle: underLineStyle,
            NSAttributedString.Key.underlineColor: underLineColor,
            NSAttributedString.Key.font: font
        ]
        
        let underlineAttributedString = NSAttributedString(string: labelString, attributes: labelAtributes)
        termsStr1.append(underlineAttributedString)
        termsStr1.append(termsStr2)
        
        termsLabel.attributedText = termsStr1
        
        emailField.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
        passwordField.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
        name.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
        passwordAgainField.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
        
//        emailField.textContentType = .emailAddress
//        passwordField.textContentType = .password
        
        let haveAccount1 = NSMutableAttributedString.init(string: "Have an account? ", attributes: [NSAttributedString.Key.font: font])
        
        let haveAccount2 = "Sign in"
        let haveAccountColor: UIColor = UIColor.white
        
        let labelAtributesHaveAccount:[NSAttributedString.Key : Any]  = [
            NSAttributedString.Key.foregroundColor: haveAccountColor,
            NSAttributedString.Key.underlineStyle: underLineStyle,
            NSAttributedString.Key.underlineColor: haveAccountColor,
            NSAttributedString.Key.font: font
        ]
        
        let haveAccountUnderlinedText = NSAttributedString(string: haveAccount2, attributes: labelAtributesHaveAccount)
        haveAccount1.append(haveAccountUnderlinedText)
        
        signInButton.attributedText = haveAccount1
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(signInPressed))
        signInButton.addGestureRecognizer(tapGesture)
        signInButton.isUserInteractionEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- VALIDATOR
    func checkFields() -> Bool
    {
        let names = name.text!.split(separator: " ")
        if !name.text!.contains(" ") || names.last! == names.first! {
            delegate.showAlert(title: "Error", message: "Please enter your full name.")
            return false
        }
        if(self.emailField.text == "" || self.passwordField.text == "" || name.text == "" || passwordAgainField.text == "")
        {
            delegate.showAlert(title: "Error", message: "Please check fields. All fields are mandatory")
            return false
        }
        else if(self.passwordField.text! != self.passwordAgainField.text!)
        {
            delegate.showAlert(title: "Error", message: "Password and Confirm Password must be same.")
            return false
        }
        else if((self.passwordField.text?.count)! < 8)
        {
            delegate.showAlert(title: "Error", message: "Password cannot be of less than 8 characters.")
            return false
        }
        else if(Utilities.isValidEmail(testStr: self.emailField.text!) == false)
        {
            delegate.showAlert(title: "Error", message: "Check Email format.")
            return false
        }
        else if(!checkBox.on)
        {
            delegate.showAlert(title: "Error", message: "You must agree to the Terms and Conditions.")
            return false
        }
        
        return true
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        if(checkFields())
        {

            let number = Int.random(in: 0 ..< 1000000)
            let names = name.text!.split(separator: " ")
            let params = [
                "first_name":"\(names.first!)",
                "last_name":"\(names.last!)",
                "username": "\(names.first!)\(names.last!)\(number)",
                "email":"\(emailField.text!)",
                "password":"\(passwordAgainField.text!)",
                "address": "empty",
                "fcm_token":"\(APP_Delegate.deviceTokenString)"
            ]
            UserDefaults.standard.set(passwordField.text!, forKey: My_Pass)
            delegate.signUpPressed(params: params)
        }
    }
    
    @IBAction func fbBtnPressed(_ sender: Any) {
        delegate.facebookPressed()
    }
    
    @IBAction func twitterSignUpPressed(_ sender: Any) {
        delegate.twitterPressed()
    }
    
    @objc func signInPressed()
    {
        delegate.selectedSignIn()
    }
    
}
