//
//  PaymentCell.swift
//  InstaTask
//
//  Created by Arqam Butt on 5/4/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import UIKit

protocol PaymentCellDelegate: class {
    func didTap(_ cell: PaymentCell)
}

class PaymentCell: UITableViewCell {

    @IBOutlet weak var timeLBl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var defaultBtn: UIButton!
    @IBOutlet weak var defaultImage: UIImageView!
    @IBOutlet weak var paymentDetailsTitle: UILabel!
    
    weak var delegate: PaymentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.defaultBtn.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        
    }

    @objc func buttonTapped(){
        delegate?.didTap(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func showCardData(last4: String, expDate: String, expYear: String, id: String, row: Int){
        timeLBl.text = "Expiry: \(expDate)/\(expYear)"
        cardNumber.text = "xxxx-xxxx-xxxx-\(last4)"
        paymentDetailsTitle.text = "Payment Details \(row + 1)"
        if row == 0 {
            defaultImage.image = UIImage(named: "default1")
        }else{
            defaultImage.image = UIImage(named: "default-no1")
        }
    }
    
}
