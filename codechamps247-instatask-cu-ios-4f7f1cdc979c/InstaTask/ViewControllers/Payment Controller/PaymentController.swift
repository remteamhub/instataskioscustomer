//
//  PaymentController.swift
//  InstaTask
//
//  Created by Arqam Butt on 5/4/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import UIKit
import Stripe

class PaymentController: ViewController {

    @IBOutlet weak var tableVC: UITableView!
    let date = Date()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UserStripeDetails.shared.deleteStripeData()
        // Do any additional setup after loading the view.
        if User.shared.isUserSkipLogin == false {
            if UserStripeDetails.shared.dataArr.count == 0 {
                NetworkManager.getUserStripeDetailsApi(params: ["user_id" : "\(User.shared.id)"], delegate: self, showHUD: true)
            }
        }
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableVC.reloadData()
    }
    
    func setUpUI() {
        self.navigationController?.isNavigationBarHidden = false
        whiteTopTitle(title: "My Payments")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableVC.tableFooterView = UIView()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func addTime() -> String{
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let time = formatter.string(from: date)
        return "Added on \(time)"
    }
    
    func addDate() -> NSMutableAttributedString{
        
        let mFormatter = formatter
        mFormatter.dateFormat = "dd"
        let date_day = Int(mFormatter.string(from: date))
        let suffix_string = "|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st"
        var suffixes = suffix_string.components(separatedBy: "|")
        let suffix = suffixes[date_day!]
        let dateString = String(date_day!) + (suffix)
        
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: "\(dateString)", attributes: [.font:UIFont.systemFont(ofSize: 17)])
        attString.setAttributes([.font: UIFont.systemFont(ofSize: 10) ,.baselineOffset:10], range: NSRange(location:String(date_day!).count,length:2))
        
        let dayFormat = formatter
        dayFormat.dateFormat = "MMMM yyyy"
        let day = dayFormat.string(from: date)
        
        let concate = NSMutableAttributedString(attributedString: attString)
        concate.append(NSAttributedString(string: " "))
        concate.append(NSAttributedString(string: day))
        concate.append(NSAttributedString(string: "."))
        
        return concate
        
    }
    
    //MARK: Stripe Payement Method
    
    @IBAction func onclick_addNewPayment(_ sender: Any) {
        if User.shared.isUserSkipLogin == false {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewViewController") as! AddNewViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
//        handleAddPaymentOptionButtonTapped()
    }
    
//    func handleAddPaymentOptionButtonTapped() {
//        // Setup add card view controller
//        let addCardViewController = STPAddCardViewController()
//        addCardViewController.delegate = self
//
//        // Present add card view controller
//        let navigationController = UINavigationController(rootViewController: addCardViewController)
//        present(navigationController, animated: true)
//    }
}

extension PaymentController: RedirectDelegate {
    func redirectTo() {
        User.shared.deleteUser {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension PaymentController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserStripeDetails.shared.dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentCell
        let data = DataModel.init(dic: UserStripeDetails.shared.dataArr[indexPath.row] as! [String : Any])
        //let data = DataModel.init(data: UserStripeDetails.shared.dataArr[indexPath.row] as! DataModel)
        cell.showCardData(last4: "\(data.last4)", expDate: "\(data.exp_month)", expYear: "\(data.exp_year)", id: data.id, row: indexPath.row)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let data = DataModel.init(dic: UserStripeDetails.shared.dataArr[indexPath.row] as! [String:Any])
        if editingStyle == .delete {
            UserStripeDetails.shared.dataArr.remove(at: indexPath.row)
            UserStripeDetails.shared.savaStripeData(arr: UserStripeDetails.shared.dataArr as NSArray)
            tableView.deleteRows(at: [indexPath], with: .fade)
            let params = [
                "user_id":"\(User.shared.id)",
                "card_id":"\(data.id)"
            ]
            print(params)
            NetworkManager.removeStripCardApi(params: params, delegate: self, showHUD: true)
        }
    }
    
}

//extension PaymentController: STPAddCardViewControllerDelegate {
//
//    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        // Dismiss add card view controller
//        print("\n *** addCardViewControllerDidCancel *** \n")
//        dismiss(animated: true)
//    }
//
//    private func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
//        print("\n *** addCardViewController *** \n")
//        let params = [
//            "user_id":"\(User.shared.id)",
//            "token":"\(token.tokenId)"
//        ]
//        print(params)
//        NetworkManager.addCardApi(params: params, delegate: self, showHUD: true)
//
//    }
//
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateSource source: STPSource, completion: @escaping STPErrorBlock) {
//        print("\n *** didCreateSource *** \n")
//        print(source.label)
//    }
//
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock) {
//        print("\n *** didCreatePaymentMethod *** \n")
////        submitPaymentMethodToBackend(paymentMethod, completion: { (error: Error?) in
////            if let error = error {
////                // Show error in add card view controller
////                completion(error)
////            }
////            else {
////                // Notify add card view controller that PaymentMethod creation was handled successfully
////                completion(nil)
////
////                // Dismiss add card view controller
////                dismiss(animated: true)
////            }
////        })
//    }
//
//}

extension PaymentController: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiGetUserStripeDetails {
            UserStripeDetails.shared.dataArr.removeAll()
            UserStripeDetails.shared.deleteStripeData()
            if let data = dataArray as? NSDictionary {
                if let sources = data["sources"] as? NSDictionary {
                    guard let defaultSource = data["default_source"] as? String else{return}
                    UserStripeDetails.shared.tempDefaultSource = defaultSource
                    let objData = sources["data"] as! NSArray
                    UserStripeDetails.shared.dataArr.append(contentsOf: objData)
                    UserStripeDetails.shared.savaStripeData(arr: objData)
                    tableVC.reloadData()
                }else{
                    print("\n *** No Stripe Card *** \n")
                }
            }
        }else if api == .eApiSetDefaultStripCard {
            UserStripeDetails.shared.dataArr.removeAll()
            UserStripeDetails.shared.deleteStripeData()
            let data = dataArray as! NSDictionary
            let sources = data["sources"] as! NSDictionary
            UserStripeDetails.shared.tempDefaultSource = data["default_source"] as! String
            let objData = sources["data"] as! NSArray
            UserStripeDetails.shared.dataArr.append(contentsOf: objData)
            UserStripeDetails.shared.savaStripeData(arr: objData)
            tableVC.reloadData()
        }else if api == .eApiRemoveStripCard{
            Extra.shared.dataSource.removeAll()
            Extra.shared.dataSourseString.removeAll()
            tableVC.reloadData()
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiGetUserStripeDetails {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiSetDefaultStripCard {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}

extension PaymentController: PaymentCellDelegate {
    func didTap(_ cell: PaymentCell) {
        let indexPath = self.tableVC.indexPath(for: cell)
//        cell.defaultImage.image = UIImage(named: "default1")
        let data = DataModel.init(dic: UserStripeDetails.shared.dataArr[indexPath!.row] as! [String : Any])
        let params = [
            "user_id":"\(User.shared.id)",
            "card_id":"\(data.id)"
        ]
        NetworkManager.setDefaultStripCardApi(params: params, delegate: self, showHUD: true)
    }
}
