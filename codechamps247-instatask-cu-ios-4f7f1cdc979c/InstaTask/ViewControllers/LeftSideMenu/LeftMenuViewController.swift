//
//  LeftMenuViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
class LeftMenuViewController: UIViewController {
    
    //MARK:- Outlets and properties
    @IBOutlet weak var tableView: UITableView!
    var menuItems: [LeftMenuItem] = []
    
    var selectedIndexPath : IndexPath!
    
    //MARK:- View controleller life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        populateLeftMenuData()
        
        tableView.delegate = self
        tableView.dataSource = self
        view.backgroundColor = mainBlue
        
        tableView.frame = view.frame
        
        let footerView = UIView.init()
        footerView.backgroundColor = mainBlue
        tableView.tableFooterView = footerView
        
        tableView.isUserInteractionEnabled = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.tableView.reloadData()
        }
        
        print(view.frame.height)
        print(tableView.frame.height)
        observerNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK:- Default Initialization
    
    func observerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(displayNameUpdated(notification:)), name: .displayNameUpdated , object: nil)
    }
    
    @objc func inboxUnreadCountUpdated(notification : Notification)
    {
        self.tableView.reloadData()
    }
    
    @objc func displayNameUpdated(notification : Notification){
        //TODO
        self.tableView.reloadData()
    }
    
    //MARK:- Utility functions
    func registerCustomNibs() {
//        tableView.register(UINib(nibName: "MailListFooterView", bundle: nil), forCellReuseIdentifier: "MailListFooterView")
    }
    
    //MARK:- UI Setup
    
    func populateLeftMenuData() {
        var leftMenuItem =  LeftMenuItem(imageName: "sideMenu-home", cellLabel : "Home" , method : "openHome")
        menuItems.append(leftMenuItem)
        leftMenuItem =  LeftMenuItem(imageName: "sideMenu-profile", cellLabel : "Profile" , method : "openProfile")
        menuItems.append(leftMenuItem)
        leftMenuItem =  LeftMenuItem(imageName: "sideMenu-requests", cellLabel : "My Requests" , method : "openMyRequests")
        menuItems.append(leftMenuItem)
        leftMenuItem =  LeftMenuItem(imageName: "sideMenu-payment", cellLabel : "Payment" , method : "openPayment")
        menuItems.append(leftMenuItem)
        leftMenuItem =  LeftMenuItem(imageName: "sideMenu-help", cellLabel : "Help" , method : "openHelp")
        menuItems.append(leftMenuItem)
        leftMenuItem =  LeftMenuItem(imageName: "sideMenu-logout", cellLabel : "Logout" , method : "logout")
        menuItems.append(leftMenuItem)
    }
    
    //MARK: - Action methods
    
    @objc func openHome() {
        
        SlideNavigationController.sharedInstance().closeMenu {
        }
        //NotificationCenter.default.post(.init(name: .openHome))
    }
    
    @objc func openProfile() {
        SlideNavigationController.sharedInstance().closeMenu {
        }
        NotificationCenter.default.post(.init(name: .openProfile))
    }
    
    @objc func openMyRequests () {
        SlideNavigationController.sharedInstance().closeMenu {
        }
        NotificationCenter.default.post(.init(name: .openMyRequest))
    }
    
    @objc func openPayment () {
        SlideNavigationController.sharedInstance().closeMenu {
        }
        NotificationCenter.default.post(.init(name: .openPayment))
    }
    @objc func openHelp () {
        SlideNavigationController.sharedInstance().closeMenu {
        }
        NotificationCenter.default.post(.init(name: .openHelp))
    }
    
    @objc func logout () {
        SlideNavigationController.sharedInstance().closeMenu {
        }
        NotificationCenter.default.post(.init(name: .logout))
    }
    
}

extension LeftMenuViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if(section == 1)
//        {
//            return view.frame.height - 55.0 - CGFloat(menuItems.count*141)
//        }
//        return 0
//    }
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footerView = UIView.init()
//        footerView.backgroundColor = mainBlue
//        return footerView
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if(section == 0)
//        {
//            return 5
//        }
//        return 0
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let footerView = UIView.init()
//        footerView.backgroundColor = mainBlue
//        return footerView
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return 1
        }
        else if(section == 2)
        {
            return 1
        }
        
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0)
        {
            return 141
        }
        else if(indexPath.section == 2)
        {
            let heightCell = view.frame.height - 141 - CGFloat(menuItems.count*55)
            if(heightCell < 0)
            {
                return 0
            }
            return heightCell
        }
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            //LeftMenuHeaderCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuHeaderCell", for: indexPath) as! LeftMenuHeaderCell
           
            cell.nameLabel.text = "\(User.shared.first_name) \(User.shared.last_name)"
            cell.starsView.value = CGFloat(round(100*User.shared.rating)/100)
            cell.ratingLavel.text = "\(round(100*User.shared.rating)/100)/5.0"
            cell.profileImgView.kf.setImage(with: URL(string: imageBackURL+User.shared.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
            
            print(Double(round(100*User.shared.rating)/100).rounded(toPlaces: 2))
            
            cell.nameLabel.textColor = mainBlue
            
            cell.backgroundColor = UIColor.init(red: 248/255, green: 246/255, blue: 247/255, alpha: 1.0)
            cell.starsView.backgroundColor = UIColor.init(red: 248/255, green: 246/255, blue: 247/255, alpha: 1.0)
            
            cell.selectionStyle = .none
            return cell
        }
        else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuCell", for: indexPath) as! LeftMenuCell
            
            cell.txtLabel.text = menuItems[indexPath.row].cellLabel
            cell.imgView.image = UIImage(named: menuItems[indexPath.row].imageName!)
            
            cell.backgroundColor = UIColor.init(red: 1/255, green: 72/255, blue: 126/255, alpha: 1.0)
            cell.txtLabel.textColor = .white
            
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuCell", for: indexPath)
            cell.backgroundColor = mainBlue
            cell.selectionStyle = .none
            return cell
        }
    }
    
//    func getAccessoryView(count : String, frame: CGRect) -> UILabel {
//        let label = UILabel(frame: CGRect(x: frame.width*0.80 - 40, y: frame.center.y-15, width: 24, height: 24))
//        label.font = Theme.defaultSmallFont()
//        label.backgroundColor = Theme.unreadCountBGColor()
//        label.layer.cornerRadius = 12
//        label.clipsToBounds = true
//        label.textColor         = UIColor.white
//        label.text              = String(count)
//        label.textAlignment     = .center
//        label.tag = 100
//        return label
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section != 0)
        {
            selectedIndexPath = indexPath
            perform(Selector(menuItems[indexPath.row].method!))
            
        }
    }
}


class LeftMenuCell: UITableViewCell {
    
    @IBOutlet weak var txtLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
}
