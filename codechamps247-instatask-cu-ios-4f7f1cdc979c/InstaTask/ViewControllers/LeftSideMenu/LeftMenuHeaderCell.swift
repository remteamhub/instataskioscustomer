//
//  LeftMenuHeaderCell.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import HCSStarRatingView

class LeftMenuHeaderCell: UITableViewCell {
    
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLavel: UILabel!
    @IBOutlet weak var starsView: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    func setupUI()
    {
        self.profileImgView.layer.cornerRadius = self.profileImgView.frame.size.width / 2
        self.profileImgView.clipsToBounds = true
        self.profileImgView.kf.indicatorType = .activity
        self.profileImgView.kf.setImage(with: URL(string: imageBackURL+User.shared.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
        self.starsView.allowsHalfStars = true
        self.profileImgView.backgroundColor = .lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
