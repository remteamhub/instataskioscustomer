//
//  HomeOrderStatusCell.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/9/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import GoogleMaps
import HCSStarRatingView

class HomeOrderStatusCell: UITableViewCell {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var spStatus: UILabel!
    @IBOutlet weak var statusBackView: UIView!
    
    var marker: GMSMarker! = GMSMarker()
    var markers:[GMSMarker] = []
    
    @IBOutlet weak var minsAwayLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var providerNameLabel: UILabel!
    @IBOutlet weak var ratingLabelView: UILabel!
    @IBOutlet weak var starRatingView: HCSStarRatingView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var beforeImageView: UIImageView!
    @IBOutlet weak var afterImageView: UIImageView!
    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var disapproveButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.clipsToBounds = true
        avatarImageView.image = UIImage.init(named: "placeholder-profile")
        
        starRatingView.allowsHalfStars = true
        
        approveButton.layer.cornerRadius = 8
        contactButton.layer.cornerRadius = 8
        shareButton.layer.cornerRadius = 8
        disapproveButton.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func updateMarker(lat: Double, long: Double)
//    {
//        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 12.5)
//        mapView.camera = camera
//
//        // Creates a marker in the center of the map.
//        marker.map = nil
//        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
//        marker.icon = UIImage.init(named: "locationMarker")
//        marker.map = mapView
//
//        mapView.isUserInteractionEnabled = true
//    }
    
    func updateMarker(lat: Double, long: Double, desLat: Double, desLong: Double, bool: Bool)
    {
        self.markers.removeAll()
        mapView.clear()
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15.5)
        mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = UIImage.init(named: "locationMarker")
        marker.map = mapView
        self.markers.append(marker)
        mapView.isUserInteractionEnabled = true
        
        if bool == true {
            let mapPinIcon = GMSMarker()
            mapPinIcon.icon = UIImage(named: "map-pin")!.resizedImageWithinRect(rectSize: CGSize(width: 40, height: 40))
            mapPinIcon.position = CLLocationCoordinate2D(latitude:desLat, longitude:desLong)
            mapPinIcon.map = mapView
            self.markers.append(mapPinIcon)
            self.setUpCameraBounds()
        }
    }
    
    func setUpCameraBounds(){
        let myLocation: CLLocationCoordinate2D = self.markers.first!.position
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
        for m in self.markers{
            bounds = bounds.includingCoordinate(m.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(80.0))
        self.mapView.animate(with: update)
    }
    
    func acceptSpData(name: String, status: String, timeViewHide: Bool, hideBackView: Bool, jobAddress location: String, ratingValue: CGFloat){
        spStatus.text = status
        providerNameLabel.text = name
        minsAwayLabel.isHidden = timeViewHide
        statusBackView.isHidden = hideBackView
        addressLabel.text = location
        starRatingView.value = CGFloat(round(100*ratingValue)/100)
        ratingLabelView.text = "\(Double(round(100*ratingValue)/100))/5.0"
        
    }
    
    func setImages(current situationImage: String, after situation:String, driver avatar: String){
        self.beforeImageView.kf.indicatorType = .activity
        self.beforeImageView.kf.setImage(with: URL(string: imageBackURL+situationImage), placeholder: UIImage.init(named: "home-noImageAdded"), options: [.transition(.fade(0.2))])
        self.afterImageView.kf.indicatorType = .activity
        self.afterImageView.kf.setImage(with: URL(string: imageBackURL+situation), placeholder: UIImage.init(named: "home-noImageAdded"), options: [.transition(.fade(0.2))])
        self.avatarImageView.kf.indicatorType = .activity
        self.avatarImageView.kf.setImage(with: URL(string: avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
    }
    
    func contactProvider(_ PhoneNumber: String){
//        if let url = URL(string: "tel://\(PhoneNumber)"), UIApplication.shared.canOpenURL(url) {
//            if #available(iOS 10, *) {
//                UIApplication.shared.open(url)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
    }
    
    
}
