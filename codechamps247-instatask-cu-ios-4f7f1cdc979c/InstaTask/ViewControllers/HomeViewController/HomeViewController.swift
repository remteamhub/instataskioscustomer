//
//  HomeViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
import GoogleMaps
import GooglePlaces
import HCSStarRatingView
import SwiftLocation
import DropDown
import SocketIO

class HomeViewController: UIViewController, SlideNavigationControllerDelegate, SocketStatusDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var mapView: UIView!
    //@IBOutlet weak var vi_Map: GMSMapView!
    //let marker = GMSMarker()
    
    @IBOutlet weak var locationButton: UIButton!
    
    @IBOutlet weak var topAddressCross: UIButton!
    @IBOutlet weak var topAddressLabel: UILabel!
    @IBOutlet weak var topAddressView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var topStatusView: UIView!
    @IBOutlet weak var topStatusLabel: UILabel!
    
    @IBOutlet weak var orderByButton: UIButton!
    
    //BOTTOM SERVICE SELECT VIEW ELEMENTS
    @IBOutlet weak var selectPaymentView: UIView!
    @IBOutlet weak var oneLaneButton: UIButton!
    @IBOutlet weak var twoLaneButton: UIButton!
    @IBOutlet weak var twoPlusLaneButton: UIButton!
    @IBOutlet weak var selectPaymentButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    //ORDER BOTTOM VIEW
    @IBOutlet weak var bottomStartOrderView: UIView!
    @IBOutlet weak var bottomAddressBar: UILabel!
    @IBOutlet weak var bottomAvatarView: UIImageView!
    @IBOutlet weak var bottomProviderName: UILabel!
    @IBOutlet weak var bottomRatingLabel: UILabel!
    @IBOutlet weak var bottomStarsView: HCSStarRatingView!
    @IBOutlet weak var bottomCardTypeImgView: UIImageView!
    @IBOutlet weak var bottomCardLabel: UILabel!
    @IBOutlet weak var estimatedRateLabel: UILabel!
    @IBOutlet weak var estimatedRateValueLabel: UILabel!
    @IBOutlet weak var enterPromolabel: UILabel!
    @IBOutlet weak var enterPromoCodeTextField: UITextField!
    @IBOutlet weak var bottomGoButton: UIButton!
    @IBOutlet weak var bottomLane: UILabel!
    
    //AFTER ORDER REVIEW BOTTOM VIEW
    @IBOutlet weak var bottomReviewView: UIView!
    @IBOutlet weak var bottomReviewImgView: UIImageView!
    
    @IBOutlet weak var bottomReviewPriceLabel: UILabel!
    @IBOutlet weak var bottomReviewNameLabel: UILabel!
    @IBOutlet weak var bottomRatingInput: HCSStarRatingView!
    @IBOutlet weak var bottomSendReviewButton: UIButton!
    
    var buttonImage : UIButton!
    var pencilImageView : UIImageView!
    var imageView : UIView!
    //var button1 = UIBarButtonItem()
    var params = [String:Any]()
    var cancelButtonCheck = false
    let zoom : Float = 16.5
    //var seconds = Int()
    //var timer : Timer?
    let dropDown = DropDown()
    var editCreateJob = CurrentJobModel()
    var pendingData = PendingJobModel()
    var geoFenceCircle = GMSCircle()
    //var markers = [GMSMarker]()
    var req: LocationRequest?
    var rating = CGFloat()
    
    
    //payment stuff
//    var customerContext: STPCustomerContext!
//    var paymentContext: STPPaymentContext!
    
    //Mine Stripe Code
    //let paymentCardTextField = STPPaymentCardTextField()
    //MARK: BackgroundTask
//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }
    
//    let manager = SocketManager(socketURL: URL(string: socketURL)!, config: [.log(true), .compress, .connectParams(["userID" : "\(User.shared.id)"])])
//    var socket: SocketIOClient!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//
//        MARK: BackgroundTask
//
//        SocketManagerSingeleton.shared.setupSocketManager(delegate: self)
        
        setupUI()
        if User.shared.isUserSkipLogin == false {
            dispatchGroup.enter()
            NetworkManager.checkCurrentJobsApi(params: ["user_id":"\(User.shared.id)", "type":"0"], delegate: self, showHUD: true)
        }
        
        
        
//        // Setup payment card text field
//        paymentCardTextField.delegate = self
//
//        // Add payment card text field to view
//        view.addSubview(paymentCardTextField)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topAddressView.layer.cornerRadius = 8
        topAddressView.addShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        if User.shared.isUserSkipLogin == false {
            dispatchGroup.enter()
            NetworkManager.updateLatLongApi(params: ["lat" : "\(LocationCoordinator.shared.lat)", "lng" : "\(LocationCoordinator.shared.lng)", "fcm_token" : "\(APP_Delegate.deviceTokenString)"], delegate: self, showHUD: true)
        }
        if UserDefaults.standard.value(forKey: "findingDriver") != nil {
            showDriver()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if User.shared.isUserSkipLogin == false {
            DispatchQueue.main.async {
                self.setupStripData()
            }
        }
        
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        check = true
//        vi_Map.clear()
//        vi_Map.stopRendering()
//        vi_Map.removeFromSuperview()
//        vi_Map = nil
//    }

    
    func setupUI()
    {
        whiteTopTitle(title: "Home")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        setupLeftButton()
        observeNotifications()
        dropDown.anchorView = selectPaymentButton
        //LocationManager.shared.startUpdatingLocation()
        //setUpMap()
        
        locationButton.setImage(UIImage.init(named: "home-locationButton"), for: .normal)
        locationButton.imageView?.contentMode = .scaleAspectFill
        locationButton.backgroundColor = .clear
        locationButton.setTitle("", for: .normal)
        locationButton.alpha = 1.0
        
        buttonImage = UIButton.init(frame: CGRect.init(x: 5, y: 5, width: 20, height: 20))
        buttonImage.setImage(UIImage(named: "pencil-64"), for: .normal)
        buttonImage.addTarget(self, action: #selector(displayEditScreen), for: .touchUpInside)
        imageView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
        imageView.addSubview(buttonImage)
        
        sendButton.imageView?.contentMode = .scaleAspectFill
        sendButton.backgroundColor = .clear
        sendButton.setTitle("", for: .normal)
        
        orderByButton.layer.cornerRadius = 15
        orderByButton.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 0)
        
        bottomStarsView.allowsHalfStars = true
        
        topAddressCross.tintColor = UIColor.init(rgb: 0xAEAEAE)
        
        confirmButton.layer.cornerRadius = 8
        bottomGoButton.layer.cornerRadius = 8
        bottomSendReviewButton.layer.cornerRadius = 8
        
        bottomAvatarView.layer.cornerRadius = bottomAvatarView.frame.size.width / 2
        bottomAvatarView.clipsToBounds = true
        
        bottomStartOrderView.alpha = 0.0
        
        
        hideStatusView()
        resetAddressBar()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 1042
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "HomeOrderStatusCell", bundle: nil), forCellReuseIdentifier: "HomeOrderStatusCell")
        
        self.view.bringSubviewToFront(tableView)
        tableView.alpha = 0.0
        
        
        enterPromoCodeTextField.delegate = self
        enterPromoCodeTextField.returnKeyType = .done
        
        //bottom incoming order view settings
        bottomAvatarView.layer.cornerRadius = bottomAvatarView.frame.size.width / 2
        bottomAvatarView.clipsToBounds = true
        bottomAvatarView.image = UIImage.init(named: "placeholder-profile")
        
        //bottom review view settings
        bottomReviewImgView.layer.cornerRadius = bottomReviewImgView.frame.size.width / 2
        bottomReviewImgView.clipsToBounds = true
        bottomReviewImgView.image = UIImage.init(named: "placeholder-profile")
        bottomSendReviewButton.setTitle("Submit", for: .normal)
        
        defaultLanes()
        loadLocation()
    }

    func defaultLanes(){
        bottomGoButton.setTitle("GO", for: .normal)
        bottomGoButton.isEnabled = true
        cancelButtonCheck = false
        oneLaneButton.setImage(UIImage.init(named: "1lane-notSelected"), for: .normal)
        twoLaneButton.setImage(UIImage.init(named: "2lane-notSelected"), for: .normal)
        twoPlusLaneButton.setImage(UIImage.init(named: "2pluslane-notSelected"), for: .normal)
    }
    
    func setupLeftButton()
    {
        
        let sideMenuButton = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        sideMenuButton.image = UIImage.init(named: "menu_icon")
        sideMenuButton.contentMode = .scaleAspectFill

        let myView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.openMenu))
        myView.addGestureRecognizer(tapGesture)
        myView.isUserInteractionEnabled = true
        myView.addSubview(sideMenuButton)
        let item2 = UIBarButtonItem(customView: myView)

        SlideNavigationController.sharedInstance()?.leftBarButtonItem = item2
   
    }
    
    func observeNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(openProfile), name: .openProfile , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openMyRequests), name: .openMyRequest , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPayment), name: .openPayment , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openHelp), name: .openHelp , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(logOut), name: .logout , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openRemoteNotification), name: .openRemoteNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openJobEditEnable), name: .openJobEditEnable , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openJobEditCancel), name: .openJobEditCancel , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openNewMessage), name: .openNewMessage, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    
    func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: .openNewMessage, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openProfile, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openMyRequest, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openPayment, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openHelp, object: nil)
        NotificationCenter.default.removeObserver(self, name: .logout, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openJobEditEnable, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openRemoteNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openJobEditCancel, object: nil)
        NotificationCenter.default.removeObserver(LeftMenuViewController(), name: .displayNameUpdated, object: nil)
    }
    
    
    @objc func openRemoteNotification(){
        if Extra.shared.checkNotification == "checkCurrentJob" {
            dispatchGroup.enter()
            NetworkManager.checkCurrentJobsApi(params: ["user_id":"\(User.shared.id)", "type":"0"], delegate: self, showHUD: false)
        }
    }
    
    @objc func openMenu()
    {
       SlideNavigationController.sharedInstance()?.toggleLeftMenu()
    }
    
    @objc func openNewMessage() {
        if Extra.shared.alreadyOnChat != true && Extra.shared.userClickedNotification == true{
            Extra.shared.alreadyOnChat = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.performSegue(withIdentifier: "showChatFromHome", sender: self)
            }
        }
    }
    
    @objc func openJobEditEnable(){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditJobVC") as! EditJobVC
        vc.editjob = editCreateJob
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func openJobEditCancel(){
        UserDefaults.standard.removeObject(forKey: "requestEdit")
        Utilities.showAlert(title: "Edit Job Alert!", message: "Driver disapproved your request to edit job.")
    }
    
    @IBAction func didChangeValue(_ sender: HCSStarRatingView) {
        print(String(format: "Changed rating to %.1f", sender.value))
        rating = sender.value
    }
    
    @IBAction func onclick_currentLocation(_ sender: Any) {
        
        print("Current Location Button")
        NearByProviderModel.shared.serviceArray.removeAll()
        self.updateMarker(lat: LocationCoordinator.shared.lat, long: LocationCoordinator.shared.lng, bool: false)

    }
    
    
    @IBAction func onclick_sendBtn(_ sender: Any) {
        print("Butto nPressed")
        params["lat"] = LocationCoordinator.shared.lat
        params["lng"] = LocationCoordinator.shared.lng
        if User.shared.isUserSkipLogin == false {
            Utilities.shared.showHUD()
        }
        print("=>", LocationCoordinator.shared.lat,LocationCoordinator.shared.lng)
        DispatchQueue.main.async {
            self.getLocationAddress(lat: LocationCoordinator.shared.lat, long: LocationCoordinator.shared.lng, result: { (address) in
                self.params["location_address"] = "\(address[0])"
                self.topAddressLabel.text = "\(address[0])"
                if User.shared.isUserSkipLogin == false {
                    NetworkManager.servicesApi(params: ["fcm_token" : "\(APP_Delegate.deviceTokenString)"], delegate: self, showHUD: true)
                }else{
                    self.showAddressBar()
                    self.defaultLanes()
                }
            }) { (error) in
                Utilities.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    //MARK:- LOCATION WORK
    func loadLocation()
    {
        // Manually request given authorization
        //LocationManager.shared.requireUserAuthorization(.whenInUse)//Locator.requestAuthorizationIfNeeded(.whenInUse)
        getLocation()
        getContinuousLocation()
        let res = LocationManager.shared.onAuthorizationChange.add { newStatus in
            print("Authorization status changed to \(newStatus)")
            switch newStatus {
            case .available:
                self.getLocation()
            case .undetermined:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> undetermined <********")
            case .denied:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> denied <********")
            case .restricted:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> restricted <********")
            case .disabled:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> disabled <********")
            }
        }
        print("Locator Result: \n", res.magnitude)
    }

    
    func setupTruckOnMap(){
        NearByProviderModel.shared.loadArr {
            print("\n Near By Prividers \n", NearByProviderModel.shared.serviceArray.count, "\n")
//            if LocationCoordinator.shared.myMarkersDictionary.first?.key == User.shared.stringID() {
//                LocationCoordinator.shared.marker.position = CLLocationCoordinate2D(latitude: LocationCoordinator.shared.lat, longitude: LocationCoordinator.shared.lng)
//                LocationCoordinator.shared.marker.icon = UIImage.init(named: "locationMarker")
//                LocationCoordinator.shared.marker.map = LocationCoordinator.shared.vi_Map
//                LocationCoordinator.shared.myMarkersDictionary[User.shared.stringID()] = LocationCoordinator.shared.marker
//            }
            if NearByProviderModel.shared.serviceArray.count > 0 {
                for (_, value) in NearByProviderModel.shared.serviceArray.enumerated(){
                    let val = value as! [String:Any]
                    let near = NearByProviderModel.init(dic: val)
                    let homeTruckIcon = GMSMarker()
                    homeTruckIcon.icon = UIImage(named: "home-truckIcon")!.resizedImageWithinRect(rectSize: CGSize(width: 40, height: 40))
                    homeTruckIcon.position = CLLocationCoordinate2D(latitude:near.lat, longitude:near.lng)
                    homeTruckIcon.map = LocationCoordinator.shared.vi_Map
                    LocationCoordinator.shared.myMarkersDictionary["\(near.usr_id)"] = homeTruckIcon
                    //print("Fuck It.", LocationCoordinator.shared.myMarkersDictionary)
                    //self.markers.append(homeTruckIcon)
                }
            }
        }
    }
    
    func setupStripData(){
        UserStripeDetails.shared.loadStripeArr() {
            if UserStripeDetails.shared.dataArr.count > 0 {
                Extra.shared.dataSource.removeAll()
                Extra.shared.dataSourseString.removeAll()
                for (_, value) in UserStripeDetails.shared.dataArr.enumerated(){
                    let val = value as! [String:Any]
                    let stripe = DataModel.init(dic: val)
                    let params = [
                        "last4":"\(stripe.last4)",
                        "id":"\(stripe.id)"
                    ]
                    Extra.shared.dataSource.append(params)
                    Extra.shared.dataSourseString.append("xxxx-xxxx-xxxx-\(stripe.last4)")
                }
                self.selectPaymentButton.setTitle(Extra.shared.dataSourseString[0], for: .normal)
//                let dataS = Extra.shared.dataSource[0] as! NSDictionary
//                self.params["payment_card_token"] = dataS["id"]!
                self.dropDown.dataSource = [Extra.shared.dataSourseString[0]]
            }else{
                self.selectPaymentButton.setTitle("Choose payment method", for: .normal)
                self.selectPaymentButton.setTitleColor(.lightGray, for: .normal)
                self.dropDown.dataSource = []
                self.params["payment_card_token"] = nil
            }
        }
    }
    
//    func setUpCameraBounds(){
//
//        let myLocation: CLLocationCoordinate2D = self.markers.first!.position
//        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
//        for m in self.markers{
//            bounds = bounds.includingCoordinate(m.position)
//        }
//        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(80.0))
//        vi_Map?.animate(with: update)
//    }

//    func myLocation() {
//        let myLocIcon = GMSMarker()
//        myLocIcon.icon = UIImage(named:"locationMarker")
//        myLocIcon.position = CLLocationCoordinate2D(latitude:LocationManager.shared.location.coordinate.latitude, longitude: LocationManager.shared.location.coordinate.longitude)
//        myLocIcon.map = LocationManager.shared.mapView
//        //self.setupCameraZoom()
//        LocationManager.shared.markers.append(myLocIcon)
//    }
    
    func setupMapCamera(){
//        LocationManager.shared.markers.removeAll()
//        let camera = GMSCameraPosition.camera(withLatitude: LocationManager.shared.location.coordinate.latitude, longitude: LocationManager.shared.location.coordinate.longitude, zoom: zoom)
//        vi_Map.camera = camera
//        LocationManager.shared.mapView = vi_Map
    }
    
    func setupCameraZoom(){
//        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(LocationManager.shared.lat)!, longitude: CLLocationDegrees(LocationManager.shared.long)!)
//
//        let camera = GMSCameraPosition.camera(withTarget: coordinates , zoom: zoom)
//        LocationManager.shared.mapView?.animate(to: camera)
    }
    
    func getLocation() {
        LocationManager.shared.locateFromGPS(.oneShot, accuracy: .block, timeout: Timeout.Mode.absolute(3000)) { (result) in
            switch result {
            case .success(let location):
                debugPrint("Get Location received: \(location.coordinate.latitude), \(location.coordinate.longitude)")
                Extra.shared.myLatitude = location.coordinate.latitude; Extra.shared.myLongitude = location.coordinate.longitude
                let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: self.zoom)
                LocationCoordinator.shared.vi_Map = GMSMapView.map(withFrame: self.mapView.frame, camera: camera)
                self.mapView.addSubview(LocationCoordinator.shared.vi_Map!)
                self.updateMarker(lat: location.coordinate.latitude, long: location.coordinate.longitude, bool: false)
            case .failure(let error):
                debugPrint("Received error: \(error)")
            }
        }
    }
    
    func getContinuousLocation() {
        
//        req = LocationManager.shared.locateFromGPS(.continous, accuracy: .block, timeout: Timeout.Mode.absolute(3000)) { (result) in
        req = LocationManager.shared.locateFromGPS(.continous, accuracy: .block, distance: .some(CLLocationDistance(2.0)), activity: .automotiveNavigation, timeout: Timeout.Mode.absolute(10.0)) { (result) in
            switch result {
            case .success(let location):
                print("Get Continuous Location received: \(location.coordinate.latitude), \(location.coordinate.longitude)")
                LocationCoordinator.shared.lat = location.coordinate.latitude
                LocationCoordinator.shared.lng = location.coordinate.longitude
                
                let coords = [
                    "lat":"\(LocationCoordinator.shared.lat)",
                    "lng":"\(LocationCoordinator.shared.lng)"
                ]
                SocketManagerSingeleton.shared.getSocket()?.emit("getDriversData", coords)
                
//                self.updateMarker(lat: location.coordinate.latitude, long: location.coordinate.longitude, bool: false)
                
                
            case .failure(let error):
                print("Received error: \(error)")
            }
        }
        //req?.stop()
    }
    
    func getCurrentLocationAddress(){

//        let options = GeocoderRequest.GoogleOptions(APIKey: MAPS_KEY)
//        let coordinates = CLLocationCoordinate2DMake(LocationCoordinator.shared.lat, LocationCoordinator.shared.lng)
//
//        LocationManager.shared.locateFromCoordinates(coordinates, service: .google(options)) { result in
//            switch result {
//            case .failure(let error):
//                debugPrint("An error has occurred: \(error)")
//            case .success(let places):
//                debugPrint("Found \(places.count) places!")
//                debugPrint((places[0].city))
//                debugPrint((places[0].country))
//                debugPrint((places[0].county))
//                debugPrint((places[0].formattedAddress))
//                debugPrint((places[0].name))
//                debugPrint((places[0].isoCountryCode))
//                debugPrint((places[0].neighborhood))
//                debugPrint((places[0].placemark?.subThoroughfare))
//                debugPrint((places[0].placemark?.administrativeArea))
//                var location = String()
////                print("subThoroughfare: " ,places[0].subThoroughfare)
////                if places[0].description != "" {
////                    location += "\(places[0].description), "
////                }
////                if let city = places[0].city, let province = places[0].administrativeArea, let country = places[0].country {
////                    location += "\(city), \(province), \(country)"
////                }
//                self.params["location_address"] = "\(location)"
//                self.topAddressLabel.text = "\(location)"
//            }
//        }
//        let homeTruckIcon = GMSMarker()
//        homeTruckIcon.icon = UIImage(named:"home-truckIcon")
//        homeTruckIcon.position = CLLocationCoordinate2D(latitude:Double(31.583322848682153), longitude: Double(74.49379526853013))
//        homeTruckIcon.map = LocationManager.shared.mapView
//
//        LocationManager.shared.markers.append(homeTruckIcon)
    
//        Locator.location(fromCoordinates: coordinates, onSuccess: { (places) -> (Void) in
//            var location = String()
//            print("subThoroughfare: " ,places[0].subThoroughfare)
//            if places[0].description != "" {
//                location += "\(places[0].description), "
//            }
//            if let city = places[0].city, let province = places[0].administrativeArea, let country = places[0].country {
//                location += "\(city), \(province), \(country)"
//            }
//            self.params["location_address"] = "\(location)"
//            self.topAddressLabel.text = "\(location)"
//        }, onFail: { (error) -> (Void) in
//            print("User Current Location Error =======> \(error.localizedDescription)")
//        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "showChatFromHome") {
            let destination = segue.destination as! ProviderChatController
            //set history item here
            destination.jobModel = self.pendingData
        }
        
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    //MARK:- LISTENER FUNCTIONS
    @objc func logOut()
    {
        //NonEscaping Clousure
        if  User.shared.isUserSkipLogin == false {
            NetworkManager.logoutApi(params: ["fcm_token":"\(APP_Delegate.deviceTokenString)"], delegate: self, showHUD: true)
        }else{
            User.shared.deleteUser {
                NearByProviderModel.shared.deleteNearBy()
                UserStripeDetails.shared.deleteStripeData()
                removeObservers()
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
        //self.navigationController?.popViewController(animated: true)
        //self.performSegue(withIdentifier: "logOutUser", sender: self)
    }
    
    @objc func openProfile()
    {
        self.performSegue(withIdentifier: "showProfile", sender: self)
    }
    
    @objc func openMyRequests()
    {
        self.performSegue(withIdentifier: "showMyRequests", sender: self)
    }
    
    @objc func openPayment()
    {
//        STPTheme.default().accentColor = .white
        //STPTheme.default().primaryForegroundColor = mainBlue
        self.performSegue(withIdentifier: "showPayment", sender: self)
        //self.paymentContext.pushPaymentMethodsViewController()
//        self.performSegue(withIdentifier: "showProfile", sender: self)
    }

    @objc func openHelp()
    {
        self.performSegue(withIdentifier: "showHelpPage", sender: self)
    }
    
    //MARK:- PLACES UTILITY METHODS
    @IBAction func orderByTapped(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //MARK:- MAP UTILITY METHODS
    func updateMarker(lat: Double, long: Double, desLat: Double = 0.0, desLong: Double = 0.0, bool: Bool)
    {
        // Creates a marker in the center of the map.
        LocationCoordinator.shared.myMarkersDictionary.removeAll()
        //self.markers.removeAll()
        LocationCoordinator.shared.vi_Map?.clear()
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: zoom)
        LocationCoordinator.shared.vi_Map?.camera = camera
        
        // Creates a marker in the center of the map.
        
        LocationCoordinator.shared.marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        LocationCoordinator.shared.marker.icon = UIImage.init(named: "locationMarker")
        LocationCoordinator.shared.marker.map = LocationCoordinator.shared.vi_Map
        LocationCoordinator.shared.myMarkersDictionary[User.shared.stringID()] = LocationCoordinator.shared.marker
       // self.markers.append(marker)
        LocationCoordinator.shared.vi_Map?.isUserInteractionEnabled = true
        if bool == false {
            self.setupTruckOnMap()
        }
    }
    
    //MARK:- SOCKET DELEGATES
    func socketEventStatus(event: SocketClientEvent, data: [Any], ack: SocketAckEmitter) {
            if event == .connect {
                print("\n \n connect: \n \n \(data) \(ack) \n \n")
            }
            if event == .disconnect {
                print("\n \n disconnect: \n \n \(data) \(ack) \n \n")
            }
            if event == .error {
                print("\n \n error: \n \n \(data) \(ack) \n \n")
            }
        }
        
        func didReceiveResponseOfApi(api: SocketName, dataArray: Any) {
            if api == .status {
                let coords = [
                    "lat":"\(LocationCoordinator.shared.lat)",
                    "lng":"\(LocationCoordinator.shared.lng)"
                ]
                Utilities.printPrettyJSONFromDic(params: coords as [String:Any])
                SocketManagerSingeleton.shared.getSocket()?.emit("getDriversData", coords)
                //print("==>" ,SocketManagerSingeleton.shared.getSocket())
    //            SocketManagerSingeleton.shared.getSocket().emit("getDriversData", with: coords as! [Any])
            }
            if api == .getDriversData {
                
                
                if let data = dataArray as? NSArray {
                    if let datArr = data[0] as? NSArray {
                        if let dic = datArr[0] as? [String:Any] {
                            if let arrTwo = dic["resolve"] as? NSArray {
                                for j in arrTwo {
                                    let dataDic = j as! [String:Any]
                                    let id = dataDic["usr_id"] as! Int
                                    print(LocationCoordinator.shared.myMarkersDictionary.count)
                                    for mark in LocationCoordinator.shared.myMarkersDictionary {
                                       
                                        if "\(id)" == mark.key || mark.key == User.shared.stringID() {
                                            print("Found Marker")
                                            print(mark.key)
                                            if let lat = dataDic["lat"] as? Double, let lng = dataDic["lng"] as? Double {
                                                CATransaction.begin()
                                                CATransaction.setAnimationDuration(1.0)
                                                print(lat, lng)
                                                mark.value.position = CLLocationCoordinate2D.init(latitude: lat, longitude: lng)
                                                
                                                print(LocationCoordinator.shared.vi_Map)
                                                mark.value.map = LocationCoordinator.shared.vi_Map
                                                CATransaction.commit()
                                            }
                                        }else{
                                            print("Did Not Found Marker")
                                            print(mark.key)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
                
            }
        }
    
    
    //MARK:- ADDRESS BAR FUNCTIONS
    @IBAction func addressCrossTapped(_ sender: Any) {
        resetAddressBar()
        defaultLanes()
    }
    
    func showAddressBar()
    {
        hideAllViews()
        orderByButton.alpha = 0.0
        sendButton.alpha = 0.0
        
        topAddressView.alpha = 1.0
        showSelectPaymentMethod()
    }
    
    func resetAddressBar()
    {
        hideAllViews()
        
        orderByButton.alpha = 1.0
        sendButton.alpha = 1.0
        
        topAddressView.alpha = 0.0
        selectPaymentView.alpha = 0.0
    }
    
    func showSelectPaymentMethod()
    {
        selectPaymentView.alpha = 1.0
    }
    
    func showBottomGoView(hidden: Bool)
    {
        if(hidden == false)
        {
            orderByButton.alpha = 0.0
            sendButton.alpha = 0.0
            topAddressView.alpha = 0.0
            selectPaymentView.alpha = 0.0
            tableView.alpha = 0.0
            bottomStartOrderView.alpha = 1.0
        }
        else
        {
            orderByButton.alpha = 1.0
            sendButton.alpha = 1.0
            topAddressView.alpha = 1.0
            selectPaymentView.alpha = 1.0
            tableView.alpha = 0.0
            bottomStartOrderView.alpha = 0.0
        }
    }
    
    func hideAllViews()
    {
        topAddressView.alpha = 0.0
        bottomStartOrderView.alpha = 0.0
        bottomReviewView.alpha = 0.0
        selectPaymentView.alpha = 0.0
        tableView.alpha = 0.0
        hideStatusView()
    }
    
    func hideStatusView() {
        topStatusView.isHidden = true
    }
    
    func showStatusView() {
        self.topStatusView.isHidden = false
    }
    
    func showRatingView(show: Bool)
    {
        hideAllViews()
        if show
        {
            bottomReviewView.alpha = 1.0
        }
    }

    func showProviderInfo()
    {
        
    }
    //MARK:- HUD FUNCTION
    func showDriver(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.showStatusView()
            self.topStatusLabel.text = labelText
        }
        
    }
    
  
    
    
    //MARK:- BOTTOM SERVICE SELECT VIEW METHODS
    
    @IBAction func oneLanePressed(_ sender: Any) {
        oneLaneButton.setImage(UIImage.init(named: "1lane"), for: .normal)
        twoLaneButton.setImage(UIImage.init(named: "2lane-notSelected"), for: .normal)
        twoPlusLaneButton.setImage(UIImage.init(named: "2pluslane-notSelected"), for: .normal)
        if User.shared.isUserSkipLogin == false {
            let dic = ServicesModel.init(dic: ServicesModel.shared.serviceArray[0] as! [String: Any])
            print(dic.title)
            params["service_price"] = dic.price
            params["title"] = dic.title
        }
    }
    @IBAction func twoLanePressed(_ sender: Any) {
        oneLaneButton.setImage(UIImage.init(named: "1lane-notSelected"), for: .normal)
        twoLaneButton.setImage(UIImage.init(named: "2lane"), for: .normal)
        twoPlusLaneButton.setImage(UIImage.init(named: "2pluslane-notSelected"), for: .normal)
        if User.shared.isUserSkipLogin == false {
            let dic = ServicesModel.init(dic: ServicesModel.shared.serviceArray[1] as! [String: Any])
            print(dic.title)
            params["service_price"] = dic.price
            params["title"] = dic.title
        }
    }
    @IBAction func twoPlusPressed(_ sender: Any) {
        oneLaneButton.setImage(UIImage.init(named: "1lane-notSelected"), for: .normal)
        twoLaneButton.setImage(UIImage.init(named: "2lane-notSelected"), for: .normal)
        twoPlusLaneButton.setImage(UIImage.init(named: "2pluslane"), for: .normal)
        if User.shared.isUserSkipLogin == false {
            let dic = ServicesModel.init(dic: ServicesModel.shared.serviceArray[2] as! [String: Any])
            print(dic.title)
            params["service_price"] = dic.price
            params["title"] = dic.title
        }
    }
    
    @IBAction func paymentSelectPressed(_ sender: Any) {
        print("Payment pressed. \(Extra.shared.dataSourseString.count)")
        
        if User.shared.isUserSkipLogin == false {
            if Extra.shared.dataSourseString.count == 0 {
                NotificationCenter.default.post(.init(name: .openPayment))
            }
        }
        dropDown.show()
        //NotificationCenter.default.post(.init(name: .openPayment))
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectPaymentButton.setTitle(item, for: .normal)
//            let dataS = Extra.shared.dataSource[index] as! NSDictionary
//            self.params["payment_card_token"] = "\(dataS["id"]!)"
        }
    }
    
    @IBAction func confirmPaymentPressed(_ sender: Any) {
        //take to page where provider's details and accept is shown
        //showBottomGoView(hidden: false)
        if User.shared.isUserSkipLogin == false {
            if (params["title"]) == nil {
                Utilities.showAlert(title: "Alert", message: "Lane not Selected")
                return
            }
            if Extra.shared.dataSourseString.count == 0 {
                Utilities.showAlert(title: "Alert", message: "Choose payment method to proceed.")
                return
            }
        
        
//        if (params["payment_card_token"]) == nil {
//            Utilities.showAlert(title: "Alert", message: "Choose payment method to proceed.")
//            return
//        }
        
            params["customer_id"] = User.shared.id
            print(params)
            bottomLane.text = params["title"] as? String
            bottomProviderName.text = "\(User.shared.first_name) \(User.shared.last_name)"
            bottomStarsView.value = CGFloat(round(100*User.shared.rating)/100)
            bottomRatingLabel.text = "\(round(100*User.shared.rating)/100)/5.0"
            bottomAddressBar.text = topAddressLabel.text
            if Extra.shared.dataSourseString.count != 0 {
                bottomCardLabel.text = Extra.shared.dataSourseString[0] == "" ? "No Card Selected" : Extra.shared.dataSourseString[0]
            }else{
                bottomCardLabel.text = "No Card Selected"
            }
            estimatedRateValueLabel.text = "$\(String(describing: params["service_price"]!))"
        }
        self.showBottomGoView(hidden: false)
        
    }
    
    @IBAction func goButtonPressed(_ sender: Any) {
        // hide everything and show the scrolling view
        if User.shared.isUserSkipLogin == false {
            if cancelButtonCheck == true {
                //Hit cancel api
                NetworkManager.cancelJobApi(params: ["job_id":"\(Extra.shared.tempJobId)"], delegate: self, showHUD: true)
                print("Cancel Job Api Hit")
            }else {
                NetworkManager.createJobsApi(params: params, delegate: self, showHUD: true)
            }
        }else{
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
    }
    
    //TABLE VIEW BUTTON FUNCTIONS
    
    @objc func approveOrder()
    {
        //show bill view in bottom view
        bottomReviewNameLabel.text = Extra.shared.tempSpName
        bottomReviewPriceLabel.text = "$\(Extra.shared.tempServicePrice)"
        bottomReviewImgView.kf.setImage(with: URL(string: Extra.shared.tempDriverImg), placeholder: UIImage.init(named: "home-noImageAdded"), options: [.transition(.fade(0.2))])
        showRatingView(show: true)
    }
    
    @objc func disapproveOrder(){
        let params = [
            "job_id":"\(Extra.shared.tempJobId)",
            "approve":"0"
        ]
        NetworkManager.customerApproveApi(params: params, delegate: self, showHUD: true)
    }
    
    
    @IBAction func submitReviewPressed(_ sender: Any) {
        let params = [
            "job_id":"\(Extra.shared.tempJobId)",
            "approve":"1",
            "driver_rating": "\(rating)"
        ]
        NetworkManager.customerApproveApi(params: params, delegate: self, showHUD: true)
    }
    
    func checkStatus(status: JobStatus) -> Int {
        switch status {
        case .complete:
            return 0
        case .leave_for_job:
            return 1
        case .pending:
            return 2
        case .cancel:
            return 3
        case .accept:
            return 4
        case .working:
            return 5
        case .timeout:
            return 6
        case .confirmArrival:
            return 7
        case .requestApproval:
            return 8
        }
    }
    
    func showTableView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.view.bringSubviewToFront(self.tableView)
            self.tableView.alpha = 1.0
            Utilities.hideHUD()
        }
    }
}

extension HomeViewController: RedirectDelegate {
    func redirectTo() {
        User.shared.deleteUser {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}


//MARK:- PLACES AUTOCOMPLETE DELEGATE
extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print("Place name: \(place.name)")
//        print("Place address: \(place.formattedAddress)")
//        print("Place attributions: \(place.attributions)")
//        LocationManager.shared.lat = "\(place.coordinate.latitude)"
//        LocationManager.shared.long = "\(place.coordinate.longitude)"
       // let  position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        
        //LocationManager.shared.updateMarker(camPosition: position, map: LocationManager.shared.mapView, markerImage: "locationMarker")
        self.updateMarker(lat: place.coordinate.latitude, long: place.coordinate.longitude, bool: false)
        self.topAddressLabel.text = place.formattedAddress
        params["lat"] = place.coordinate.latitude
        params["lng"] = place.coordinate.longitude
        params["location_address"] = place.formattedAddress
        dismiss(animated: true, completion: nil)
        if User.shared.isUserSkipLogin == false {
            NetworkManager.servicesApi(params: ["fcm_token" : "\(APP_Delegate.deviceTokenString)"], delegate: self, showHUD: true)
        }else{
            self.showAddressBar()
            self.defaultLanes()
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func getLocationAddress(lat: Double,long: Double, result:@escaping([String])->(), errorRes: @escaping(Error)->())  {
        DispatchQueue.main.async {
            let geoCoder = GMSGeocoder()
            let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
            geoCoder.reverseGeocodeCoordinate(coordinates) { (placeMarkers, error) in
                if let placeMarker = placeMarkers {
                    if let address = placeMarker.firstResult()?.lines {
                        result(address)
                    }
                }else{
                    errorRes(error!)
                }
            }
        }
    }
    
//    func getTimer() {
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(HomeViewController.updateTimer)), userInfo: nil, repeats: true)
//        BackgroundTask.shared.registerBackgroundTask()
//    }
    
//    @objc func updateTimer() {
//        print(Extra.shared.seconds)
//        if Extra.shared.seconds < 1 {
//            Extra.shared.seconds = 15
//            bottomGoButton.isEnabled = false
//            bottomGoButton.setTitle("GO", for: .normal)
//            cancelButtonCheck = false
//            timer?.invalidate()
//            timer = nil
//            if BackgroundTask.shared.backgroundTask != .invalid {
//                BackgroundTask.shared.endBackgroundTask()
//            }
//        }else{
//            BackgroundTask.shared.backgroundStateCheck()
//            Extra.shared.seconds -= 1
//            bottomGoButton.setTitle("CANCEL", for: .normal)
//            cancelButtonCheck = true
//            bottomGoButton.isEnabled = true
//        }
//    }
    //MARK: BackgroundTask
//    @objc func reinstateBackgroundTask() {
//        if timer != nil && BackgroundTask.shared.backgroundTask == .invalid {
//            BackgroundTask.shared.registerBackgroundTask()
//        }
//    }
    
    @objc func displayEditScreen(){
        NetworkManager.jobEditRequestApi(params: ["job_id" : "\(Extra.shared.tempJobId)"], delegate: self, showHUD: true)
    }
    
    func cancelStatusHit(){
        hideStatusView()
        hideAllViews()
        tableView.alpha = 0.0
        orderByButton.alpha = 1.0
        sendButton.alpha = 1.0
        UserDefaults.standard.removeObject(forKey: "findingDriver")
        //print(LocationManager.shared.lat, LocationManager.shared.long)
        NetworkManager.nearByProvidersApi(params: [ "lat":"\(LocationCoordinator.shared.lat)", "lng":"\(LocationCoordinator.shared.lng)"], delegate: self, showHUD: false)
    }
    
}

//MARK: - TextField Delegate
extension HomeViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        enterPromoCodeTextField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Text Field End Editing.")
        if User.shared.isUserSkipLogin == false {
            NetworkManager.validateCouponsApi(params: ["code" : "\(enterPromoCodeTextField.text!)"], delegate: self, showHUD: true)
        }
    }
}


//TABLEVIEW WORK
extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //change in case of influencer and business owner
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 1079
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeOrderStatusCell") as! HomeOrderStatusCell
        
        cell.approveButton.addTarget(self, action: #selector(approveOrder), for: .touchUpInside)
        cell.disapproveButton.addTarget(self, action: #selector(disapproveOrder), for: .touchUpInside)
        
        cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.size.width / 2
        cell.avatarImageView.clipsToBounds = true
        cell.avatarImageView.image = UIImage.init(named: "placeholder-profile")
        cell.contactButton.addTarget(self, action: #selector(contactProvider), for: .touchUpInside)
        if Extra.shared.afterWorkImage != "" {
            print("Ineraction Enable")
            cell.approveButton.isUserInteractionEnabled = true
            cell.disapproveButton.isUserInteractionEnabled = true
        }else{
            print("Ineraction Disable")
            cell.approveButton.isUserInteractionEnabled = false
            cell.disapproveButton.isUserInteractionEnabled = false
        }
        
        cell.setImages(current: Extra.shared.currentSituationImage, after: Extra.shared.afterWorkImage, driver: Extra.shared.tempDriverImg)
        ///UIImage.init(named: "home-beforeWorkImage")
        ///UIImage.init(named: "home-noImageAdded")
        
        cell.updateMarker(lat: Extra.shared.tempDriverLat, long: Extra.shared.tempDriverLng, desLat: Extra.shared.tempJobLat, desLong: Extra.shared.tempJobLong, bool: true)
        cell.acceptSpData(name: Extra.shared.tempSpName, status: Extra.shared.tempStatus, timeViewHide: Extra.shared.tempTimeViewHide, hideBackView: Extra.shared.tempHideStatusBackView, jobAddress: Extra.shared.tempJobLocation, ratingValue: CGFloat(Extra.shared.driverRating))
        cell.contactProvider(Extra.shared.tempContact)
        cell.selectionStyle = .none
        return cell
    }
    
    @objc func contactProvider(_ sender:Any) {
        print("///////////////////////// Contacting Provider //////////////////////////")
         self.performSegue(withIdentifier: "showChatFromHome", sender: self)
    }
    
    
}

//STRIPE DELEGATE
//extension HomeViewController: STPPaymentContextDelegate
//{
//    //STRIPE DELEGATE METHODS
//    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
//        //        print(paymentContext.selectedPaymentMethod)
//    }
//
//    func paymentContext(_ paymentContext: STPPaymentContext,
//                        didCreatePaymentResult paymentResult: STPPaymentResult,
//                        completion: @escaping STPErrorBlock) {
//
//    }
//
//    func paymentContext(_ paymentContext: STPPaymentContext,
//                        didFinishWith status: STPPaymentStatus,
//                        error: Error?) {
//
//        switch status {
//        case .error:
//            print("Error Here!")
//            self.dismiss(animated: true, completion: nil)
//        case .success:
//            print("Success Here!")
//            self.dismiss(animated: true, completion: nil)
//        case .userCancellation:
//            return // Do nothing
//        }
//    }
//
//    func paymentContext(_ paymentContext: STPPaymentContext,
//                        didFailToLoadWithError error: Error) {
//        self.navigationController?.popViewController(animated: true)
//        // Show the error to your user, etc.
//    }
//
//    func paymentContext(_ paymentContext: STPPaymentContext, didUpdateShippingAddress address: STPAddress, completion: @escaping STPShippingMethodsCompletionBlock) {
//    }
//}



extension HomeViewController: NetworkManagerDelegate {
    
    
    func populateData(with data: CurrentJobModel){
        Extra.shared.tempJobLat = data.lat
        Extra.shared.tempJobLong = data.lng
        Extra.shared.tempJobLocation = data.location_address
        Extra.shared.tempDriverLat = data.provider.lat
        Extra.shared.tempDriverLng = data.provider.lng
        Extra.shared.tempContact = data.provider.phone
        Extra.shared.tempDriverImg = "\(imageBackURL)"+"\(data.provider.avatar)"
        Extra.shared.driverRating = data.provider.rating
        self.hideStatusBar(isHidden: false)
        self.showTableView()
        tableView.reloadData()
    }
    
    func timeView(isHidden bool: Bool){
        if bool {
            Extra.shared.tempTimeViewHide = bool
        }else{
            Extra.shared.tempTimeViewHide = bool
        }
    }
    
    func hideStatusBar(isHidden bool: Bool){
        if bool {
            Extra.shared.tempHideStatusBackView = bool
        }else{
            Extra.shared.tempHideStatusBackView = bool
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiLogout {
            User.shared.deleteUser {
                NearByProviderModel.shared.deleteNearBy()
                UserStripeDetails.shared.deleteStripeData()
                removeObservers()
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.popToRootViewController(animated: true)
            }
        }else if api == .eApiServices {
            let data = dataArray as! NSDictionary
            let dataArr = data["data"] as! NSArray
            print("Received Data: ", dataArr)
            ServicesModel.shared.serviceArray.append(contentsOf: dataArr)
            self.showAddressBar()
            self.defaultLanes()
            
        }else if api == .eApiCreateJobs {
            
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            
            let resp = dataArray as! NSDictionary
            let data = resp["data"] as! NSDictionary
            let status = data["job_status"] as! Int
            if let jobId = data["last_insert_id"] as? Int {
                Extra.shared.tempJobId = jobId
            }
            if status == checkStatus(status: .pending) {
                UserDefaults.standard.set(labelText, forKey: "findingDriver")
                showDriver()
            }
            bottomGoButton.setTitle("CANCEL", for: .normal)
            cancelButtonCheck = true
            //getTimer()
        }
        
        dispatchGroup.notify(queue: .main) {
            if api == .eApiCheckCurrentJobs {
                
                //LocationManager.shared.mapView?.clear()
                let resp = dataArray as! NSDictionary
                if let dataArr = resp["data"] as? NSDictionary {
                    
                    print("\n ***> Check Current Job <*** \n")
                    Utilities.printPrettyJSONFromDic(params: resp as! [String:Any])
                    
                    var data = CurrentJobModel()
                    if let jobs = dataArr["job"] as? [String:Any] {
                        data = CurrentJobModel.init(dic: jobs)
                        self.pendingData = PendingJobModel.init(dic: jobs)
                    }else{
                        data = CurrentJobModel.init(dic: dataArr as! [String : Any])
                    }
                    self.editCreateJob = data
                    
                    Extra.shared.tempJobId = data.id
                    Extra.shared.tempSpName = "\(data.provider.first_name) \(data.provider.last_name)"
                    Extra.shared.tempSpPhone = data.provider.phone
                    Extra.shared.tempServicePrice = data.service_price
                    
                    if data.job_status == self.checkStatus(status: .pending) {
                        
                        UserDefaults.standard.set(labelText, forKey: "findingDriver")
                        //                    bottomGoButton.isEnabled = false
                        self.bottomGoButton.setTitle("CANCEL", for: .normal)
                        self.navigationItem.setRightBarButton(nil, animated: true)
                        self.cancelButtonCheck = true
                        self.showDriver()
                        self.showBottomGoView(hidden: false)
                        self.updateMarker(lat: data.lat, long: data.lng, bool: false)
                        self.bottomProviderName.text = "\(data.user.first_name) \(data.user.last_name)"
                        self.bottomAddressBar.text = data.location_address
                        self.bottomStarsView.value = CGFloat(round(100*data.user.rating)/100)
                        self.bottomRatingLabel.text = "\(round(100*data.user.rating)/100)/5.0"
                        
                        self.estimatedRateValueLabel.text = "$\(data.service_price)"
                    }else if data.job_status == self.checkStatus(status: .cancel) {
                        
                        self.cancelStatusHit()
                        self.navigationItem.setRightBarButton(nil, animated: true)
                        self.dismiss(animated: true, completion: nil)
                        if data.user.fcm_token == "" || data.user.fcm_token != APP_Delegate.deviceTokenString  {
                            User.shared.deleteUser {
                                NearByProviderModel.shared.deleteNearBy()
                                UserStripeDetails.shared.deleteStripeData()
                                self.removeObservers()
                                self.navigationController?.isNavigationBarHidden = true
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                        
                    }else if data.job_status == self.checkStatus(status: .accept) {
                        print("\n\n start")
                        Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
                        print("\n\n end")
                        self.hideStatusView()
                        self.navigationItem.setRightBarButton(UIBarButtonItem.init(customView: self.imageView), animated: true)
                        Extra.shared.tempStatus = "Serivce Provicer accept the job."
                        self.timeView(isHidden: true)
                        self.tableView.isScrollEnabled = false
                        self.tableView.scroll(to: .top, animated: true)
                        
//                        let origin = "\(data.user.lat),\(data.user.lng)"
//                        let destination = "\(data.lat),\(data.lng)"
//                        let params = [
//                            "origin":"\(origin)",
//                            "destination":"\(destination)",
//                            "mode":"driving",
//                            "key":"\(MAPS_KEY)"
//                        ]
//                        NetworkManager.polylineApi(params: params, delegate: self, showHUD: false)
                        self.populateData(with: data)
                        
                    }else if data.job_status == self.checkStatus(status: .leave_for_job) {
                        
                        self.hideStatusView()
                        self.navigationItem.setRightBarButton(UIBarButtonItem.init(customView: self.imageView), animated: true)
                        Extra.shared.tempStatus = "Serivce Provicer is on the way."
                        self.timeView(isHidden: false)
                        self.tableView.isScrollEnabled = false
                        self.tableView.scroll(to: .top, animated: true)
                        self.populateData(with: data)
                        
                    }else if data.job_status == self.checkStatus(status: .working) {
                        
                        self.navigationItem.setRightBarButton(nil, animated: true)
                        Extra.shared.currentSituationImage = "\(data.current_situation_img)"
                        Extra.shared.afterWorkImage = "\(data.after_work_img)"
                        self.tableView.isScrollEnabled = true
                        self.populateData(with: data)
                        self.hideStatusBar(isHidden: true)
                        
                    }else if data.job_status == self.checkStatus(status: .confirmArrival) {
                        
                        self.navigationItem.setRightBarButton(nil, animated: true)
                        Extra.shared.tempStatus = "Serivce Provicer arrived on destination."
                        self.timeView(isHidden: true)
                        self.populateData(with: data)
                        
                    }else if data.job_status == self.checkStatus(status: .requestApproval) {
                        
                        self.navigationItem.setRightBarButton(nil, animated: true)
                        Extra.shared.tempStatus = "Serivce Provicer requested for approval."
                        self.timeView(isHidden: true)
                        Extra.shared.currentSituationImage = "\(data.current_situation_img)"
                        Extra.shared.afterWorkImage = "\(data.after_work_img)"
                        self.tableView.scroll(to: .bottom, animated: true)
                        self.populateData(with: data)
                        
                    }
                    //                Timeout will be handled on server and on timeout we will get job status 3.
                    //                else if data.job_status == checkStatus(status: .timeout) {
                    //                    print(dataArray)
                    //                    self.cancelStatusHit()
                    //                    self.defaultLanes()
                    //                }
                    
                }
//                if self.chatCheck == true  {
//                    self.chatCheck = false
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                        self.performSegue(withIdentifier: "showChatFromHome", sender: self)
//                    }
//                }
            }else if api == .eApiUpdateLatLong {
                print("\n ***> Lat Long Updated <*** \n")
            }
        }
        
        if api == .eApiCustomerApprove {
            print("Approved Data: ",dataArray)
            let resp = dataArray as! NSDictionary
            let data = resp["data"] as! NSDictionary
            let status = data["status"] as! String
            if status == "approve" {
                Extra.shared.afterWorkImage = ""
                Extra.shared.currentSituationImage = ""
                UserDefaults.standard.removeObject(forKey: "findingDriver")
                self.resetAddressBar()
                params["title"] = nil
            }else{
                Extra.shared.afterWorkImage = ""
                self.tableView.reloadData()
            }
        }else if api == .eApiPolylines {
            let data = dataArray as! [String:Any]
//
//            print("\n\n\n Polyline Start")
//            print(data)
//            print("\n\n\n Polyline End")
            
//            let json = JSON(data: response.data!)
            let routes = data["routes"] as! NSArray
//
            for route in routes
            {
//                print(route)
                let rte = route as! [String:Any]
                guard let routeOverviewPolyline = rte["overview_polyline"] as? [String:Any] else { print("Error on : Polyline overview_polyline"); return}
                guard let points = routeOverviewPolyline["points"] as? String else { print("Error on : Polyline point"); return }
                let path = GMSPath.init(fromEncodedPath: points)

                let polyline = GMSPolyline(path: path)
                polyline.strokeColor = .black
                polyline.strokeWidth = 10.0
                polyline.map = LocationCoordinator.shared.vi_Map

            }
            
            
        }
        else if api == .eApiNearByProviders {
            let resp = dataArray as! NSDictionary
            print("\n ***> Near By Providers <*** \n")
            Utilities.printPrettyJSONFromDic(params: resp as! [String:Any])
            if let nearBy = resp["data"] as? NSArray {
                NearByProviderModel.shared.deleteNearBy()
                NearByProviderModel.shared.saveArr(arr: nearBy)
                self.setupTruckOnMap()
            }
        }
        else if api == .eApiCancelJob{
            //timer?.invalidate()
            self.cancelStatusHit()
            bottomGoButton.setTitle("GO", for: .normal)
            cancelButtonCheck = false
        }else if api == .eApiValidateCoupons {
            let resp = dataArray as! NSDictionary
            print("\n ***> Validate Coupon <*** \n")
            //Utilities.printPrettyJSONFromDic(params: resp as! [String:Any])
            let couponData = resp["data"] as! NSDictionary
            let currentPrice = estimatedRateValueLabel.text!.replacingOccurrences(of: "$", with: "")
            let discountPrice = ((couponData["percentage"] as! Float)/100) * Float(currentPrice)!
            estimatedRateValueLabel.text = "$\(Int(Float(currentPrice)! - discountPrice))"
            params["service_price"] = "\(Int(Float(currentPrice)! - discountPrice))"
        }else if api == .eApiLimitations {
            
            let resp = dataArray as! NSDictionary
            let limitationData = resp["data"] as! NSDictionary
            Extra.shared.seconds = limitationData["max_job_cancellation_time"] as! Int
            print("\n ***> Limitation <*** \n")
            
        }else if api == .eApiJobEditRequest {
            UserDefaults.standard.set(editRequestText, forKey: "requestEdit")
                self.showStatusView()
                self.topStatusLabel.text = editRequestText
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiLogout {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiCreateJobs {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiCheckCurrentJobs {
            Utilities.hideHUD()
        }else if api ==  .eApiServices{
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api ==  .eApiCustomerApprove{
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api ==  .eApiPolylines{
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
        else if api == .eApiNearByProviders {
            NearByProviderModel.shared.deleteNearBy()
            self.updateMarker(lat: Extra.shared.myLatitude, long: Extra.shared.myLongitude, bool: false)
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
        else if api == .eApiCancelJob {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiValidateCoupons {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiJobEditRequest {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
}


//let origin = "\(37.778483),\(-122.513960)"
//let destination = "\(37.706753),\(-122.418677)"
//let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=[YOUR-API-KEY]"
//
//Alamofire.request(url).responseJSON { response in
//    let json = JSON(data: response.data!)
//    let routes = json["routes"].arrayValue
//
//    for route in routes
//    {
//        let routeOverviewPolyline = route["overview_polyline"].dictionary
//        let points = routeOverviewPolyline?["points"]?.stringValue
//        let path = GMSPath.init(fromEncodedPath: points!)
//
//        let polyline = GMSPolyline(path: path)
//        polyline.strokeColor = .black
//        polyline.strokeWidth = 10.0
//        polyline.map = mapViewX
//
//    }
//}



