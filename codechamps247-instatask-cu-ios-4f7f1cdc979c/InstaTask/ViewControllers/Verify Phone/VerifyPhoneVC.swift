//
//  VerifyPhoneVC.swift
//  InstaTask
//
//  Created by MacBook Pro on 09/04/2020.
//  Copyright © 2020 CodeChamps. All rights reserved.
//

protocol VerifyPhoneDelegate:class {
    func sendPhoneVerifyResponse(_ phone: String?, completion: (()->()))
}

import UIKit
import Firebase
import FirebaseAuth

class VerifyPhoneVC: UIViewController {

    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var vCodeTF: UITextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var msg: UILabel!
    @IBOutlet weak var vCodeBackView: UIView!
    @IBOutlet weak var confirnBtn: UIButton!
    
    var delegate: VerifyPhoneDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        whiteTopTitle(title: "Phone Verification")
        // Do any additional setup after loading the view.
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        
        if phoneTF.text != "" {
            PhoneAuthProvider.provider().verifyPhoneNumber("\(self.phoneTF.text!)", uiDelegate: nil) { [weak self] (verificationID, error) in
                if let err = error {
                    Utilities.showAlert(title: "Error", message: err.localizedDescription)
                    return
                }
                print("Sign in using the verificationID and the code sent to the user")
                
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID-Insta")
                self?.confirnBtn.isHidden = true
                self?.confirnBtn.isHidden = false
                self?.msg.isHidden = false
                self?.vCodeBackView.isHidden = false
            }
        }else{
            Utilities.showAlert(title: "Alert", message: "Phone number can't be empty.")
        }
    }
    
    @IBAction func confirmTapped(_ sender: Any) {
        
        if vCodeTF.text != "" {
            let verificationID = UserDefaults.standard.string(forKey: "authVerificationID-Insta")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: vCodeTF.text!)
            Auth.auth().signIn(with: credential) { [weak self] (authResult, error) in
                if let err = error {
                    Utilities.showAlert(title: "Error", message: err.localizedDescription)
                    return
                }
                let userID = authResult?.user.uid//?.user.uid
                let phone = authResult?.user.phoneNumber
                print("User Firebase ID : \(userID!)")
                print("User Phone # : \(phone!)")
                self?.delegate?.sendPhoneVerifyResponse(phone, completion: { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                })
            }
        }else{
            Utilities.showAlert(title: "Alert", message: "Code can't be empty.")
        }
        
    }
    
}
