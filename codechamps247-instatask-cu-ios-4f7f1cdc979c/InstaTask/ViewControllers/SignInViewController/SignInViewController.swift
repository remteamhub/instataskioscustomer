//
//  SignInViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/7/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
import FBSDKLoginKit
import TwitterKit
import Crashlytics
import SwiftLocation
import CoreLocation
import SocketIO

class SignInViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    
    var user: User!
    //var manager = LocationManager.shared.locationManager!
//    let manager = SocketManager(socketURL: URL(string: "http://134.209.115.28:3000/")!, config: [.log(true), .compress, .connectParams(["userID" : "208"])])
//    var socket: SocketIOClient!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        getLocation()
        setupUI()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if User.shared.isLoggedIn() {
            SocketManagerSingeleton.shared.connectToSocket()
            self.performSegue(withIdentifier: "signInSuccess", sender: self)
        }
    }
    
    func setupUI(){
        emailField.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
        passwordField.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
    }
    
    func getLocation() {
        
        LocationManager.shared.locateFromGPS(.oneShot, accuracy: .block, timeout: Timeout.Mode.absolute(3000)) { (result) in
            switch result {
            case .success(let location):
                debugPrint("Location received: \(location.coordinate.latitude), \(location.coordinate.longitude)")
                LocationCoordinator.shared.lat = location.coordinate.latitude
                LocationCoordinator.shared.lng = location.coordinate.longitude
            case .failure(let error):
                debugPrint("Received error: \(error)")
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "signInSuccess")
        {
            self.navigationController?.isNavigationBarHidden = false
        }
        if segue.identifier == "verifiyPhone" {
            let destination = segue.destination as! VerifyPhoneVC
            destination.delegate = self
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    //MARK:- VALIDATORS
    func checkFields() -> Bool
    {
        if(self.emailField.text == "" || self.passwordField.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please check fields. All fields are mandatory")
            return false
        }
        else if((self.passwordField.text?.count)! < 6)
        {
            self.displayAlertWithOk(title: "Error", message: "Password cannot be of less than 8 characters.")
            return false
        }else if(Utilities.isValidEmail(testStr: self.emailField.text!) == false)
        {
            self.displayAlertWithOk(title: "Error", message: "Check Email format.")
            return false
        }
        
        return true
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        
        User.shared.isUserSkipLogin = true
        let userObj = [
            "id": "0",
            "username": "",
            "email": "",
            "phone": "+13037898765",
            "address": "Appartment 9, South Avenue, NY, USA",
            "first_name": "",
            "last_name": "",
            "status": "",
            "type": "1",
            "rating": "0.0",
            "radius": "0",
            "user_id":"0",
            "job_status": "",
            "withdraw_status": "",
            "total_earning": "",
            "weekly_earning": "",
            "overall_rating": ""
        ]
        User.shared.saveUser(user: User.init(dic: userObj))
        NotificationCenter.default.post(.init(name: .displayNameUpdated))
        self.performSegue(withIdentifier: "signInSuccess", sender: self)
        
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        if(checkFields())
        {
            let json: [String:Any] = [
                "email":"\(emailField.text!)",
                "password":"\(passwordField.text!)",
                "fcm_token":"\(APP_Delegate.deviceTokenString)",
                "lat":"\(LocationCoordinator.shared.lat)",
                "lng":"\(LocationCoordinator.shared.lng)"
            ]
            self.callSignInAPI(json: json)
        }
    }
    
    @IBAction func forgotPassButtonPressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "goToForgetPassword", sender: self)
    }
    
    @IBAction func fbLoginPressed(_ sender: Any) {

        let readPermissions = ["email","public_profile"]
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: readPermissions, from: self)  { (result, error) -> Void in
            if (error == nil){
                if (result?.isCancelled)!{
                    // if user cancel the login
                }
                else
                {
                    if (FBSDKAccessToken.current() != nil) {
                        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id,name,email, picture.type(large)"]).start(completionHandler: { (icon, data, err) in
                            if err != nil {
                                print("Graph Request Error............................\(err!)")
                                return
                            }else{
                                
                                let params = data as! [String:Any]
                                let id = params["id"] as! String
                                let name = params["name"] as! String
                                guard let email = params["email"] as? String else{
                                    Utilities.showAlert(title: "Error", message: "You are not registed with email on facebook. Use facebook account with a registered email or register manually.")
                                    return
                                }
                                let pic = params["picture"] as! [String:Any]
                                let picData = pic["data"] as! [String:Any]
                                let picURL = picData["url"] as! String
                                
                                
                                let names = name.split(separator: " ")
                                let userParams = [
                                    "first_name":"\(names.first!)",
                                    "last_name":"\(names.last!)",
                                    "username": "\(names.first!)\(names.last!)\(id)",
                                    "email":"\(email)",
                                    "address": "empty",
                                    "avatar_file":"\(picURL)",
                                    "social_id":"\(id)",
                                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                                    "lat":"\(LocationCoordinator.shared.lat)",
                                    "lng":"\(LocationCoordinator.shared.lng)"
                                ]
                                
                                fbLoginManager.logOut()
                                NetworkManager.socialLoginApi(params: userParams, delegate: self, showHUD: true)
                            }
                        })
                    }
                }
            } else {
                print(error?.localizedDescription ?? "Sign up View Controller -> fbLoginPressed")
            }
        }
        
    }
    
    @IBAction func twitterLoginPressed(_ sender: Any) {
        
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                let currentUser = TWTRAPIClient.withCurrentUser()
                
                var userParams: [String:Any] = [
                    "username": "\(session!.userName)\(session!.userID)",
                    "address": "empty",
                    "social_id":"\(session!.userID)",
                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                    "lat":"\(LocationCoordinator.shared.lat)",
                    "lng":"\(LocationCoordinator.shared.lng)"
                ]
                
                currentUser.requestEmail { (email, error) in
                    if let ema = email {
                        userParams["email"] = ema
                    }
                    if let err = error {
                        Utilities.showAlert(title: "Error", message: "\(err)")
                        return
                    }
                }
                currentUser.loadUser(withID: session!.userID, completion: { (user, error) in
                    let names = user!.name.split(separator: " ")
                    userParams["first_name"] = names.first
                    userParams["last_name"] = names.last
                    userParams["avatar_file"] = user!.profileImageLargeURL
                    
                })
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                    NetworkManager.socialLoginApi(params: userParams, delegate: self, showHUD: true)
                })
            } else {
                print(error!.localizedDescription)
            }
        })
    }
    
    
    //MARK:- NETWORK CALL
    func callSignInAPI(json: [String:Any])
    {
        NetworkManager.loginApi(params: json, delegate: self, showHUD: true)
    }
    
}

extension SignInViewController: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiLogin{
            User.shared.isUserSkipLogin = false
            UserDefaults.standard.set(passwordField.text!, forKey: My_Pass)
            let data = dataArray as! [String:Any]
            let resp = data["data"] as! [String:Any]
            Utilities.printPrettyJSONFromDic(params: resp["userDetails"] as! [String : Any])
            let user = User.init(dic: resp["userDetails"] as! [String : Any])
//            if let nearBy = resp["nearbyProviders"] as? NSArray {
//                NearByProviderModel.shared.saveArr(arr: nearBy)
//            }
            User.shared.saveUser(user: user)
            SocketManagerSingeleton.shared.connectToSocket()
            NotificationCenter.default.post(.init(name: .displayNameUpdated))
            self.performSegue(withIdentifier: "signInSuccess", sender: self)
            
        }else if api == .eApiSocialLogin {
            User.shared.isUserSkipLogin = false
            let data = dataArray as! [String:Any]
            let resp = data["data"] as! [String:Any]
            //Utilities.printPrettyJSONFromDic(params: resp["userDetails"] as! [String : Any])
            if let userProfile = resp["userDetails"] as? [String:Any] {
                Utilities.printPrettyJSONFromDic(params: userProfile)
                let user = User.init(dic: userProfile)
//                if let nearBy = resp["nearbyProviders"] as? NSArray {
//                    NearByProviderModel.shared.saveArr(arr: nearBy)
//                }
                if user.phone == "" {
                    self.user = user
//                    loginWithPhone()
                    self.performSegue(withIdentifier: "verifiyPhone", sender: self)
                }else{
                    User.shared.saveUser(user: user)
                    SocketManagerSingeleton.shared.connectToSocket()
                    self.performSegue(withIdentifier: "signInSuccess", sender: self)
                }
            }else{
                 self.displayAlert(title: "Alert!", message: resp["status"] as! String)
            }
            
        
        }else if api == .eApiUpdatePhone {
            User.shared.isUserSkipLogin = false
            User.shared.saveUser(user: user)
            SocketManagerSingeleton.shared.connectToSocket()
            self.performSegue(withIdentifier: "signInSuccess", sender: self)
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiLogin{
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }else if api == .eApiSocialLogin {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }else if api == .eApiUpdatePhone {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }
    }
}


extension SignInViewController: VerifyPhoneDelegate {
    
    func sendPhoneVerifyResponse(_ phone: String?, completion: (() -> ())) {
        
        let params = [
            "user_id":"\(self.user.id)",
            "fcm_token":"\(APP_Delegate.deviceTokenString)",
            "phone":"\(phone!)"
        ]
        NetworkManager.updatePhoneApi(params: params, delegate: self, showHUD: true)
        
        completion()
    }
    
}


//
//extension SignInViewController: AKFViewControllerDelegate
//{
//    func prepareLoginViewController(loginViewController: AKFViewController) {
//        loginViewController.delegate = self
//        //UI Theming - Optional
//        loginViewController.uiManager = SkinManager(skinType: .classic, primaryColor: mainBlue)
//    }
//
//    func loginWithPhone(){
//        let inputState = UUID().uuidString
//        let vc = (accountKit?.viewControllerForPhoneLogin(with: nil, state: inputState))!
//        vc.isSendToFacebookEnabled = true
//        vc.delegate = self
//        self.prepareLoginViewController(loginViewController: vc)
//        self.present(vc as UIViewController, animated: true, completion: nil)
//    }
//
//
//    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AKFAccessToken, state: String) {
//        if(accountKit != nil)
//        {
//            accountKit.requestAccount {
//                (account, error) -> Void in
//                if let phoneNumber = account?.phoneNumber{
//                    DispatchQueue.main.async {
//                        let userPhone = "+" + phoneNumber.countryCode + phoneNumber.phoneNumber
//                        let params = [
//                            "user_id":"\(self.user.id)",
//                            "fcm_token":"\(APP_Delegate.deviceTokenString)",
//                            "phone":"\(userPhone)"
//                        ]
//                        NetworkManager.updatePhoneApi(params: params, delegate: self, showHUD: true)
//                    }
//                }
//                else
//                {
//                    print(error ?? "Sign up View Controller")
//                }
//            }
//        }
//    }
//
//    func viewController(_ viewController: (UIViewController & AKFViewController), didFailWithError error: Error) {
//        // ... implement appropriate error handling ...
//        print("\(String(describing: viewController)) did fail with error: \(error.localizedDescription)")
//    }
//
//    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)) {
//        // ... handle user cancellation of the login process ...
//    }
//}


//extension SignInViewController: CLLocationManagerDelegate {
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let loc = locations.first{
//            print("SIGN IN Lat: " + "\(loc.coordinate.latitude)")
//            print("SIGN IN Long: " + "\(loc.coordinate.longitude)")
//        }
//    }
//
//}
