//
//  CustomerCell.swift
//  InstaTask
//
//  Created by Arqam Butt on 10/17/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import UIKit

class CustomerCell: UITableViewCell {

    @IBOutlet weak var cBackView: UIView!
    @IBOutlet weak var cMsg: UILabel!
    @IBOutlet weak var cMsgTime: UILabel!
    @IBOutlet weak var cNAme: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


