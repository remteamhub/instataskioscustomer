//
//  ProviderCell.swift
//  InstaTask
//
//  Created by Arqam Butt on 10/17/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import UIKit

class ProviderCell: UITableViewCell {

    @IBOutlet weak var pBackView: UIView!
    @IBOutlet weak var pMsg: UILabel!
    @IBOutlet weak var pMsgTime: UILabel!
    @IBOutlet weak var pName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
