//
//  Chat View.swift
//  InstaTask
//
//  Created by Arqam Butt on 10/17/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import UIKit

class ViewForProvider: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        roundCorners(corners: [.bottomRight, .topLeft, .topRight], radius: 8.0)
    }
}

class ViewForCustomer: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        roundCorners(corners: [.bottomLeft, .topLeft, .topRight], radius: 8.0)
    }
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
