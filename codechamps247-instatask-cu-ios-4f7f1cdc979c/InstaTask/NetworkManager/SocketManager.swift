//
//  SocketManager.swift
//  InstaTask
//
//  Created by Arqam Butt on 16/11/2019.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import SocketIO

enum SocketName: Int {
    case status = 0
    case getDriversData = 1
}

protocol SocketStatusDelegate: class {
    func socketEventStatus(event: SocketClientEvent, data: [Any], ack: SocketAckEmitter)
    func didReceiveResponseOfApi(api: SocketName, dataArray:Any)
}

typealias success = (_ status: String, _ data: Any) -> Void
typealias failure = (_ error: NSError) -> Void

class SocketManagerSingeleton: NSObject {
    
    static let shared: SocketManagerSingeleton = {
        let instance = SocketManagerSingeleton()
        return instance
    }()
    
    weak var delegate: SocketStatusDelegate?
 
    fileprivate let manager = SocketManager(socketURL: URL(string: socketURL)!, config: [.log(true), .compress, .connectParams([
        "userID" : "\(User.shared.id)"
    ])])

    fileprivate var socket: SocketIOClient?

    func getSocket() -> SocketIOClient? {
        socket = self.manager.defaultSocket
        return socket
    }
 
    fileprivate func setupSocketManager(delegate: SocketStatusDelegate){
        
        socket = getSocket()
        
        //Events Start
        //-------------------------------------
        
        socket?.on(clientEvent: .connect) {data, ack in
            delegate.socketEventStatus(event: .connect, data: data, ack: ack)
        }
        
        socket?.on(clientEvent: .disconnect) {data, ack in
            delegate.socketEventStatus(event: .disconnect, data: data, ack: ack)
        }
        
        socket?.on(clientEvent: .error) { (data, ack) in
            //print("\n ================\n \(data) \n \(ack) \n=============\n")
            delegate.socketEventStatus(event: .error, data: data, ack: ack)
        }
        
        //Events Close
        //-------------------------------------
        
        //Status Start
        //-------------------------------------
        
        socket?.on("status") { (data, ack) in
            delegate.didReceiveResponseOfApi(api: SocketName.status, dataArray: data)
        }
        
        socket?.on("getDriversData", callback: { (data, ack) in
           delegate.didReceiveResponseOfApi(api: SocketName.getDriversData, dataArray: data)
        })
        
        //Status Close
        //-------------------------------------
        
        socket?.connect()
        
 
    }
    
    func connectToSocket() {
        self.setupSocketManager(delegate: HomeViewController())
    }
    
}
