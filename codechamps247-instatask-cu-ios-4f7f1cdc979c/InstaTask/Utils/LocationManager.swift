//
//  LocationManager.swift
//  ToEat
//
//  Created by Taha Muneeb on 06/10/2017.
//  Copyright © 2017 toEat. All rights reserved.
//


import CoreLocation
import GoogleMaps
import GooglePlaces

class LocationCoordinator:NSObject{

//    var location:CLLocation!
    var lat: Double = 0.0
    var lng: Double = 0.0
    var myMarkersDictionary = [String:GMSMarker]()
//    var lat_Float:CLLocationDegrees = 0.0
//    var long_Float:CLLocationDegrees = 0.0
//
//    var locationManager :CLLocationManager!
//
//    var markers:[GMSMarker] = []
    let marker = GMSMarker()
    var vi_Map:GMSMapView?
//    let marker = GMSMarker()
    static let shared: LocationCoordinator = {
        let instance = LocationCoordinator()

        return instance
    }()
    
    
//    // MARK: - Initialization Method
//
//    override init() {
//        super.init()
//        location = CLLocation()
//        locationManager = CLLocationManager()
//        self.locationManager.requestAlwaysAuthorization()
//        self.locationManager.requestWhenInUseAuthorization()
//        guard let pathURL = URL(string: "prefs:root=LOCATION_SERVICES"), !pathURL.absoluteString.isEmpty else {
//            return
//        }
//        if CLLocationManager.locationServicesEnabled() {
//            switch CLLocationManager.authorizationStatus() {
//            case .notDetermined, .restricted, .denied:
//                    UIApplication.shared.open(pathURL, options: [:], completionHandler: nil)
//            case .authorizedAlways, .authorizedWhenInUse:
//                print("Access")
//                locationManager.delegate = self
//                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters//kCLLocationAccuracyBestForNavigation
//            default:
//                fatalError(" Location Not Verified. ")
//            }
//        } else {
//                UIApplication.shared.open(pathURL, options: [:], completionHandler: nil)
//        }
//    }
//
//    func startUpdatingLocation() {
//        if (CLLocationManager.locationServicesEnabled())
//        {
//            locationManager = CLLocationManager()
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.requestAlwaysAuthorization()
//            locationManager.startUpdatingLocation()
//        }
//
//    }
//
//    func stopUpdatingLocation() {
//        locationManager.stopUpdatingLocation()
//    }
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let loc = locations.first{
//            location = loc
//            lat = "\(loc.coordinate.latitude)"
//            long = "\(loc.coordinate.longitude)"
//            lat_Float = loc.coordinate.latitude
//            long_Float = loc.coordinate.longitude
//            print(lat)
//            print(long)
//        }
//    }
    
    
//    func updateMarker(camPosition: CLLocationCoordinate2D, map: GMSMapView!, markerImage: String){
////        LocationManager.shared.markers.removeAll()
////        marker.map = nil
////        marker.icon = UIImage(named: markerImage)
////        marker.position = camPosition
////        marker.map = map
////        //print(LocationManager.shared.markers.count)
//////        let latitude : Double? = 37.3118
//////        let longitude : Double? = -122.0312
////            let bounds = GMSCoordinateBounds(coordinate: location.coordinate, coordinate: CLLocationCoordinate2D(latitude: lat_Float, longitude: long_Float))
////            let update = GMSCameraUpdate.fit(bounds, withPadding: 10.0)
////            map?.moveCamera(update)
////
//////            let bounds = GMSCoordinateBounds(coordinate: location.coordinate, coordinate: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!))
//////            let update = GMSCameraUpdate.fit(bounds, withPadding: 10.0)
//////            map?.moveCamera(update)
////
////
////        LocationManager.shared.markers.append(marker)
//    }
//
 
    
 
}
