//
//  Utilities.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/20/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import Foundation
import Alamofire
import PKHUD
import Photos
import Presentr

protocol RedirectDelegate: class{
    func redirectTo()
}

class Utilities: NSObject {
    
//    static var hud = MBProgressHUD()
    
    //MARK:- Reachability checks
    static let shared = Utilities()
    weak var delegate: RedirectDelegate?
    
    
    class func isNetworkReachable() -> Bool {
        let networkReachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        return (networkReachabilityManager?.isReachable)!
    }
    
    //MARK: - Progress loader code block
    
//    class func showProgressLoader(view : UIView) {
//        DispatchQueue.main.async {
//            hud = MBProgressHUD.showAdded(to: view, animated: true)
//            hud.show(animated: true)
//        }
//    }
//
//    class func hideProgressLoader(view : UIView) {
//        DispatchQueue.main.async{
//            hud.hide(animated: true)
//            MBProgressHUD.hide(for: view, animated: true)
//        }
//    }
    
    class func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func showHUD() {
        
        let view = PKHUDRotatingImageView.init(image: UIImage(named:"loading"), title: "", subtitle: "")
        view.frame = CGRect(x: 0, y: 0, width: 130, height: 130)
        view.backgroundColor = .clear
        PKHUD.sharedHUD.contentView = view
        PKHUD.sharedHUD.dimsBackground = true
        
        PKHUD.sharedHUD.show()
        
    }
    
    class func hideHUD() {
        PKHUD.sharedHUD.hide()
    }
    
    class func showTextHUD(text: String){
        let view = PKHUDTextView.init(text: text)
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 80)
        view.backgroundColor = .clear
        PKHUD.sharedHUD.contentView = view
        PKHUD.sharedHUD.dimsBackground = true
        
        PKHUD.sharedHUD.show()
        
//        UIView.animate(withDuration: 1.0, animations: {
//            view.alpha = 0.0
//        }) { (true) in
//            view.alpha = 1.0
//        }
        
    }
    
    class func UnixToCurrentTime(unixTime: Int) -> String {
        print(unixTime)
        print(TimeInterval(unixTime))
        let date = Date(timeIntervalSince1970: TimeInterval(unixTime))
        print(date)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = TimeZone.current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    class func UnixToTime(unixTime: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(unixTime))
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        //        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = TimeZone.current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    class func printPrettyJSONFromDic(params:[String:Any]) {
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print("Response: \(jsonString)")
    }
    
    class func generateError(message:String) -> NSError {
        let userInfo: [String : Any] =
            [
                NSLocalizedDescriptionKey :  NSLocalizedString("er", value: message, comment: "")
        ]
        return NSError(domain: "ShiploopHttpResponseErrorDomain", code: 401, userInfo: userInfo)
    }
    
    class func showAlert(title:String, message:String, delegate: RedirectDelegate? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (action) in
            if delegate != nil {
                delegate?.redirectTo()
            }
        }
        alert.addAction(okAction)
        let win = APP_Delegate.window
        win?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
//    class func isKeyPresentInUserDefaults(key: String) -> Bool {
//        return UserDefaults.standard.object(forKey: key) != nil
//    }
    
    //MARK:- UserDefaults Saving
//    class func saveToken(token: String)
//    {
//        UserDefaults.standard.set(token, forKey: "authToken")
//        UserDefaults.standard.synchronize()
//    }
//
//    class func getToken() -> String
//    {
//        return UserDefaults.standard.string(forKey: "authToken")!
//    }
    
//    class func saveCurrentUser(user: User)
//    {
//        let data = NSKeyedArchiver.archivedData(withRootObject: user)
//        UserDefaults.standard.set(data, forKey: "currentUser")
//        UserDefaults.standard.synchronize()
//    }
//
//    class func getCurrentUser() -> User
//    {
//        var user = User()
//        let usersData = UserDefaults.standard.object(forKey: "currentUser") as? NSData
//        if let loggedInUsersData = usersData {
//            if let  userData = NSKeyedUnarchiver.unarchiveObject(with: loggedInUsersData as Data) as? User {
//                user = userData
//            }
//        }
//        return user
//    }
//
//    class func removeCurrentUser()
//    {
//        UserDefaults.standard.removeObject(forKey: "currentUser")
//        UserDefaults.standard.synchronize()
//    }
    
    //MARK:- VALIDATORS
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}


extension Utilities {
    
    class func show(alertMessage msgDetails: String, alertTitle mainTitle: String, onWhichController ViewController: UIViewController, onSuccessHandler: ((Bool)->Void)?, onFailureHandler: ((Bool)->Void)?) {
        var alertController: AlertViewController {
            let alertController = AlertViewController(title: mainTitle, body: msgDetails)
            let cancelAction = AlertAction(title: "Cancel", style: .cancel) {
                print("CANCEL!!")
                onFailureHandler?(false)
            }
            let okAction = AlertAction(title: "OK", style: .destructive) {
                print("OK!!")
                onSuccessHandler?(true)
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            return alertController
        }
        
        let presenter: Presentr = {
            let presenter = Presentr(presentationType: .alert)
            presenter.transitionType = .crossDissolve
            presenter.dismissTransitionType = .crossDissolve
            return presenter
        }()
        
        presenter.viewControllerForContext = ViewController
        presenter.outsideContextTap = .passthrough
        ViewController.customPresentViewController(presenter, viewController: alertController, animated: true)
    }
    
}
extension Dictionary {
    
    static func += (left: inout [Key: Value], right: [Key: Value]) {
        for (key, value) in right {
            left[key] = value
        }
    }
}
