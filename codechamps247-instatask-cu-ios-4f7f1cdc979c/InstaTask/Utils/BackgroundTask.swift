//
//  BackgroundTask.swift
//  InstaTask
//
//  Created by Arqam Butt on 5/14/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import UIKit

let dispatchGroup = DispatchGroup()

class BackgroundTask: NSObject {
    
    static let shared = BackgroundTask()
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    var application = UIApplication.shared
    
    func registerBackgroundTask() {
        backgroundTask = application.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        application.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
    }
    
    func backgroundStateCheck() {
        //call endBackgroundTask() on completion..
        switch application.applicationState {
        case .active:
            print("App is active.")
        case .background:
            print("App is in background.")
            print("Background time remaining = \(application.backgroundTimeRemaining) seconds")
        case .inactive:
            print("App is In active.")
        @unknown default:
            fatalError()
        }
    }
}
