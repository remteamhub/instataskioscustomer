//
//  Extra.swift
//  InstaTask
//
//  Created by Arqam Butt on 4/29/19.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation

class Extra:NSObject {
    static let shared = Extra()
    
    var tempParams = [String:Any]()
    var myLatitude = Double()
    var myLongitude = Double()
    var tempLoginCheck = false
    var tempJobLat = Double()
    var tempJobLong = Double()
    var tempDriverLat = Double()
    var tempDriverLng = Double()
    var checkNotification = String()
    var tempDriverImg = String()
    var tempJobLocation = String()
    var tempContact = String()
    var dataSource = [Any]()
    var dataSourseString = [String]()
    var seconds = Int()
    var alreadyOnChat = false
    var userClickedNotification = false

    //chapi for tableView xib
    var tempStatus = String()
    var tempSpName = String()
    var tempSpPhone = String()
    var tempTimeViewHide = Bool()
    var tempHideStatusBackView = Bool()
    var tempJobId = Int()
    var tempServicePrice = Int()
    var tempMiles = Int()
    var currentSituationImage = String()
    var afterWorkImage = String()
    var driverRating = Float()
    
}

enum JobStatus:Int {
    case complete = 0
    case leave_for_job = 1
    case pending = 2
    case cancel = 3
    case accept = 4
    case working = 5
    case timeout = 6
    case confirmArrival = 7
    case requestApproval = 8
}

enum UserStatus:Int {
    case active = 0
    case inactive = 1
    case banned = 2
    case pending = 3
}

enum AdminApprovalStatus:Int {
    case approve = 0
    case unApprove = 1
}
