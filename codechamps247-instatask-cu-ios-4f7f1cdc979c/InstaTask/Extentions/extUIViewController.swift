//
//  extUIViewController.swift
//  QueFront
//
//  Created by Asfand Shabbir on 7/7/18.
//  Copyright © 2018 Code Champs. All rights reserved.
//

import Foundation
import UIKit

var alertController : UIAlertController!
extension UIViewController{
    
    //MARK:- DisplayAlertMessage
    
    func displayAlertWithOk(title : String , message : String) {
        // create the alert
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlert(title : String , message : String) {
        // create the alert
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func whiteTopTitle(title: String)
    {
        let titleView: UILabel?  = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        titleView?.font = Theme.primaryFontWithSize(size: 23.0)
        titleView?.textColor = .white
        titleView?.text = title
        titleView?.textAlignment = .center
        self.navigationItem.titleView = titleView
    }
    
}
