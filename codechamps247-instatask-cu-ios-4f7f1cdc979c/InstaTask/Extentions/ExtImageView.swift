//
//  ExtImageView.swift
//  SnikPic
//
//  Created by Aqsa Arshad on 13/11/2018.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIImageView {
    
    func setImage (urlString: String,placeHolderString : String = "placeholder") {
        let placeHolderImage = UIImage.init(named: placeHolderString)
        
        if(urlString.trim() == "") {
            self.image = placeHolderImage
            return
        }
        
        self.kf.setImage(with: URL(string: urlString)!, placeholder: placeHolderImage, options: nil, progressBlock: { (receivedSize, totalSize) -> () in
            
        }, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
                if(image != nil) {
                    self.image = image
                }
                
            }
        }
        )
    }
}

extension UIImage {
    
    /// Returns a image that fills in newSize
    func resizedImage(newSize: CGSize) -> UIImage {
        // Guard newSize is different
        guard self.size != newSize else { return self }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /// Returns a resized image that fits in rectSize, keeping it's aspect ratio
    /// Note that the new image size is not rectSize, but within it.
    func resizedImageWithinRect(rectSize: CGSize) -> UIImage {
        let widthFactor = size.width / rectSize.width
        let heightFactor = size.height / rectSize.height
        
        var resizeFactor = widthFactor
        if size.height > size.width {
            resizeFactor = heightFactor
        }
        
        let newSize = CGSize(width: size.width/resizeFactor, height: size.height/resizeFactor)
        let resized = resizedImage(newSize: newSize)
        return resized
    }
    
}

//import Accelerate
//extension UIImage{
//    func resizeImageUsingVImage(size:CGSize) -> UIImage? {
//        let cgImage = self.cgImage!
//        var format = vImage_CGImageFormat(bitsPerComponent: 8, bitsPerPixel: 32, colorSpace: nil, bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.first.rawValue), version: 0, decode: nil, renderingIntent: CGColorRenderingIntent.defaultIntent)
//        var sourceBuffer = vImage_Buffer()
//        defer {
//            free(sourceBuffer.data)
//        }
//        var error = vImageBuffer_InitWithCGImage(&sourceBuffer, &format, nil, cgImage, numericCast(kvImageNoFlags))
//        guard error == kvImageNoError else { return nil }
//        // create a destination buffer
//        let scale = self.scale
//        let destWidth = Int(size.width)
//        let destHeight = Int(size.height)
//        let bytesPerPixel = self.cgImage!.bitsPerPixel/8
//        let destBytesPerRow = destWidth * bytesPerPixel
//        //let destData = //UnsafeMutablePointer<UInt8>.allocate(capacity: destHeight * destBytesPerRow)
//        let destData = UnsafeMutableBufferPointer<UInt8>(start: UnsafeMutablePointer<UInt8>.allocate(capacity: destHeight * destBytesPerRow), count: destHeight * destBytesPerRow)
//        defer {
//            destData.baseAddress?.deallocate()
//        }
//        var destBuffer = vImage_Buffer(data: destData, height: vImagePixelCount(destHeight), width: vImagePixelCount(destWidth), rowBytes: destBytesPerRow)
//        // scale the image
//        error = vImageScale_ARGB8888(&sourceBuffer, &destBuffer, nil, numericCast(kvImageHighQualityResampling))
//        guard error == kvImageNoError else { return nil }
//        // create a CGImage from vImage_Buffer
//        var destCGImage = vImageCreateCGImageFromBuffer(&destBuffer, &format, nil, nil, numericCast(kvImageNoFlags), &error)?.takeRetainedValue()
//        guard error == kvImageNoError else { return nil }
//        // create a UIImage
//        let resizedImage = destCGImage.flatMap { UIImage(cgImage: $0, scale: 0.0, orientation: self.imageOrientation) }
//        destCGImage = nil
//        return resizedImage
//    }
//}
