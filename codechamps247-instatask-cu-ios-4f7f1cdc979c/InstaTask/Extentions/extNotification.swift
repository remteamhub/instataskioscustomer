//
//  extNotification.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let openHome = Notification.Name(kOpenHomeNotification)
    static let openProfile = Notification.Name(kOpenProfileNotification)
    static let openMyRequest = Notification.Name(kOpenMyRequestsNotification)
    static let openPayment = Notification.Name(kOpenPaymentNotification)
    static let openHelp = Notification.Name(kOpenHelpNotification)
    static let openSettings = Notification.Name(kOpenSettingsNotification)
    static let logout = Notification.Name(kLogoutNotification)
    static let openRemoteNotification = Notification.Name(kOpenRemoteNotification)
    static let openMessage = Notification.Name(kOpenMessage)
    static let openJobEditEnable = Notification.Name(kOpenJobEditEnable)
    static let openJobEditCancel = Notification.Name(kOpenJobEditCancel)
    static let displayNameUpdated = Notification.Name(kDisplayNameUpdated)
    static let openEditJobNotification = Notification.Name(kOpenEditJobNotification)
    static let refreshChat = Notification.Name(kRefreshChat)
    static let openNewMessage = Notification.Name(kOpenNewMessage)
}
