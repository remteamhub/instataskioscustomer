//
//  ExtString.swift
//  SnikPic
//
//  Created by Aqsa Arshad on 31/10/2018.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation

extension String {
    
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    func convertToDictionary() -> Any? {
        
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as Any
            } catch {
            }
        }
        return nil
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
